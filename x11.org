* PACKAGES
  #+begin_src shell :tangle ~/x11-packages
  picom-ibhagwan-git
  dunst
  stalonetray
  polybar
  #+end_src
* Picom
  #+begin_src conf :tangle ~/.config/picom/picom.conf :mkdirp yes
  corner-radius = 10.0;
  inactive-opacity = 0.9;
  # Rules
  opacity-rule = [
  "97:class_g = 'obsidian'",
  "95:class_g = 'nuclear'"
  ];
  rounded-corners-exclude = [
  "class_g = 'Polybar'",
  "class_g = 'Dunst'",
  "class_g = 'bemenu'",
  "class_g = 'Rofi'"
  ];
  opacity-exclude = [
  "class_g = 'Dunst'",
  "class_g = 'bemenu'",
  "class_g = 'Rofi'",
  "class_g = 'Zathura'"
  ];
  focus-exclude = [
  "class_g = 'Inkscape'",
  "class_g = 'Gimp-2.99'",
  "class_g = 'Dunst'",
  "class_g = 'bemenu'",
  "class_g = 'Zathura'",
  "class_g = 'Rofi'",
  ];
  #+end_src
* Dunst
  #+begin_src conf :tangle ~/.config/dunst/dunstrc :mkdirp yes
  [global]
  follow = mouse
  geometry = 300x5
  offset = 540x35
  padding = 10
  horizontal_padding = 10
  frame_width = 2
  frame_color = "#B5BBC9"
  separator_color = frame
  sort = yes
  idle_threshold = 10
  corner_radius = 15
  font = Noto Sans Regular 10
  line_height = 0
  markup = full
  format = "<b>%s</b>\n%b"
  alignment = center
  vertical_alignment = center
  show_age_threshold = -1
  word_wrap = yes
  ellipsize = middle
  ignore_newline = no
  stack_duplicates = true
  hide_duplicate_count = false
  show_indicators = yes
  icon_position = left
  min_icon_size = 0
  max_icon_size = 32
  icon_path = /usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/
  sticky_history = no
  history_length = 0
  title = Dunst
  class = Dunst
  [urgency_low]
  background = "#252931"
  foreground = "#ffffff"
  timeout = 5
  [urgency_normal]
  background = "#252931"
  foreground = "#ffffff"
  timeout = 5
  [urgency_critical]
  background = "#252931"
  foreground = "#ffffff"
  frame_color = "#ff9505"
  timeout = 5
  #+end_src
* Stalonetray
  #+begin_src conf :tangle ~/.config/stalonetray/stalonetrayrc :mkdirp yes
  icon_size 16
  slot_size 24
  decorations none
  scrollbars none
  vertical false
  no_shrink false
  kludges force_icons_size
  geometry 1x1+1272+4
  background "#24262E"
  grow_gravity E
  #+end_src
* Polybar
  #+begin_src conf :tangle ~/.config/polybar/config :mkdirp yes
  [colors]
  background = #24262E
  background-alt = #444
  foreground = #B5BBC9
  foreground-alt = #555
  primary = #ffb52a
  secondary = #e60053
  alert = #bd2c40

  [bar/main]
  override-redirect = true
  locale = pt_BR.UTF-8
  radius = 10
  width = 92.2%
  height = 24
  offset-x = 45
  offset-y = 4.0
  fixed-center = true

  background = ${colors.background}
  foreground = ${colors.foreground}

  line-size = 1
  line-color = ${colors.foreground}

  border-size = 0
  border-color = ${colors.foreground}

  padding-left = 2
  padding-right = 3

  module-margin-left = 1
  module-margin-right = 0

  font-0 = Ubuntu:weight=bold:pixelsize=9:antialias=true:hinting=true;1

  modules-left = ewmh xwindow
  modules-center = date
  modules-right = pulseaudio

  tray-background = #414856
  tray-position = right
  tray-padding = 1

  cursor-click = pointer
  cursor-scroll = ns-resize

  [module/ewmh]
  type = internal/xworkspaces
  enable-click = true
  enable-scroll = true

  label-active = %name%
  label-active-padding = 2
  label-active-overline = "#B5BBC9"
  label-occupied = %name%
  label-occupied-foreground = "#919AB3"
  label-occupied-padding = 2
  label-empty = %name%
  label-empty-foreground = "#4B546C"
  label-empty-padding = 2
  label-urgent = %name%
  label-urgent-foreground = "#C45500"

  [module/xwindow]
  type = internal/xwindow
  label = "| %title:0:31:...%"

  [module/date]
  type = internal/date
  interval = 60
  date = "%a %d %Y - %H:%M"
  label = %date%

  [module/pulseaudio]
  type = internal/pulseaudio

  format-volume = <label-volume>
  label-volume = vol %percentage%%
  label-volume-foreground = ${root.foreground}

  [settings]
  screenchange-reload = true

  [global/wm]
  margin-top = 0
  margin-bottom = 0
  #+end_src
