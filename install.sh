#!/usr/bin/sh
printf "\033[31m-----------\nSetting variables........\n-----------\n"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_DATA_DIRS="$HOME/.local/share:$XDG_DATA_DIRS"
export XDG_CACHE_HOME="$HOME/.cache"
export MF_DATA_DIR="/run/media/personal"
export GTK_IM_MODULE=fcitx5
export QT_IM_MODULE=fcitx5
export XMODIFIERS=@im=fcitx5
export ALTERNATE_EDITOR=""
export EDITOR="nvim"
export VISUAL="emacsclient -cn -a 'emacs'"
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/scripts:$PATH"
export PATH="$CARGO_HOME/bin:$PATH"
export STARDICT_DATA_DIR="$XDG_DATA_HOME"
export C_INCLUDE_PATH="$HOME/.local/include"
export LD_LIBRARY_PATH="$HOME/.local/lib"
export PKG_CONFIG_PATH="$HOME/.local/lib/pkgconfig"
export MANPATH="$HOME/.local/share/man:/usr/share/man:"
export GOPATH="$XDG_CACHE_HOME/go"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"

printf "\033[31m-----------\nUPDATING SYSTEM........\n-----------\n"
paru -Syyu
printf "\033[31m-----------\nDevel Tools and Dependecies........\n-----------\n"
paru -S --needed base-devel rustup pkgconf git neovim libxft-bgra ueberzug cmake intltool ccache doxygen python-yaml clang wget aspell blas lapack boost libcdr double-conversion gc gdl glib2 gsl gspell gtk3 gtkmm3 gtkspell3 hunspell jemalloc lcms2 imagemagick pango libpng poppler-glib poppler potrace readline librevenge libsigc++ libsoup libvisio libwpg perl-xml-parser libxml2 libxslt python-lxml zlib
printf "\033[31m-----------\nInstalling X11........\n-----------\n"
paru -S --needed xorg xorg-server xorg-apps xorg-xmessage libx11 libxinerama libxrandr libxss xclip xdotool xorg-xwininfo xorg-setxkbmap sx hsetroot picom dunst xsel slop shotgun stalonetray 
printf "\033[31m-----------\nInstalling GTK........\n-----------\n"
paru -S --needed gtk gtk2 gtk3 gtk4 lxappearance-gtk3
printf "\033[31m-----------\nInstalling Fonts........\n-----------\n"
paru -S --needed ttf-roboto ttf-ubuntu-font-family noto-fonts noto-fonts-emoji ttf-nerd-fonts-symbols-mono ttf-iosevka-nerd otf-openmoji ttf-symbola
printf "\033[31m-----------\nInstalling Xmonad........\n-----------\n"
paru -S --needed xmonad-git xmonad-contrib-git xmobar-git gnome-keyring polkit-gnome 
printf "\033[31m-----------\nInstalling Sound........\n-----------\n"
paru -S --needed pipewire pipewire-pulse pipewire-media-session pavucontrol 
printf "\033[31m-----------\nInstalling Input........\n-----------\n"
paru -S --needed fcitx5 fcitx5-configtool fcitx5-hangul fcitx5-qt 
printf "\033[31m-----------\nInstalling File Management........\n-----------\n"
paru -S --needed spacefm timeshift gparted ranger dua-cli
printf "\033[31m-----------\nInstalling CLI........\n-----------\n"
paru -S --needed dash zsh zsh-autosuggestions zsh-syntax-highlighting exa fd sd ripgrep translate-shell yt-dlp hyperfine opendoas topgrade tealdeer aria2 jq cpupower optipng jpegoptim skim chafa opustags btop fontpreview-ueberzug-git expac mimeo highlight appimagelauncher 
printf "\033[31m-----------\nInstalling Formatters........\n-----------\n"
paru -S --needed tidy prettier ormolu-bin shfmt shellcheck astyle 
printf "\033[31m-----------\nInstalling Office........\n-----------\n"
paru -S --needed zathura zathura-cb zathura-djvu zathura-pdf-mupdf zathura-ps libreoffice musescore anki texlive-most pandoc
printf "\033[31m-----------\nInstalling Media........\n-----------\n"
paru -S --needed mpd mpc ncmpcpp ario mpv imv
printf "\033[31m-----------\nInstalling Design........\n-----------\n"
paru -S --needed inkscape darktable kontrast
printf "\033[31m-----------\nInstalling NET........\n-----------\n"
paru -S --needed kdeconnect ulauncher telegram-desktop qutebrowser chromium-widevine syncthing 

# su -c "mkdir -p /etc/zsh"
# su -c 'echo "export ZDOTDIR=\$HOME/.config/zsh" > /etc/zsh/zshenv'
# su -c 'chmod +x /etc/zsh/zshenv'
# su -c 'chsh -s /usr/bin/zsh'
