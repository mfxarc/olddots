;;; init.el --- My init.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: março 18, 2022
;;
;;; Commentary:
;;  
;;  My configuration for init.el
;;  
;;; Code:

(defun emacs-init-time ()
  "Show the time required to emacs load."
  (interactive)
  (message "Emacs ready in %s with %d garbage collections."
	   (format "%.2f seconds" (float-time(time-subtract after-init-time before-init-time))) gcs-done))
(add-hook 'emacs-startup-hook 'emacs-init-time)
(add-hook 'emacs-startup-hook (lambda () (setq gc-cons-threshold 16777216 gc-cons-percentage 0.1)))

(setq frame-title-format '("%b"))

(defvar mf-lisp-dir (expand-file-name "utils" user-emacs-directory))
(defvar mf-env-dir (expand-file-name "environments" user-emacs-directory))
(defvar mf-user-dir (expand-file-name "personal" mf-cache-dir))
(defvar mf-themes-dir (expand-file-name "themes" user-emacs-directory))
(add-to-list 'load-path mf-lisp-dir)
(add-to-list 'load-path mf-env-dir)
(add-to-list 'load-path mf-user-dir)
(add-to-list 'custom-theme-load-path mf-themes-dir)

;; EXWM
(straight-use-package '(exwm :type git :host github :repo "ch11ng/exwm"))
(require 'exwm)
(require 'mf-exwm-utils)
(require 'mf-exwm)
(require 'user)
;;; Common utils
(require 'mf-setup)
(require 'mf-ui)
(require 'mf-utils)
(require 'mf-editor)
(require 'mf-dir)
(require 'mf-sh)
(require 'mf-apps)
(require 'mf-langs)
(require 'mf-org)
(require 'mf-latex)
(require 'mf-elfeed)
;;; Environments setup
(require 'mf-git)
(require 'mf-media)
(require 'mf-note-taking)
(require 'mf-university)
(require 'mf-linguist)
(require 'mf-internet)
(require 'mf-documents)
(require 'mf-spreadsheet)
(require 'mf-youtube)
(require 'mf-blog)
;;; Better defaults
(require 'mf-key)
(require 'mf-lkeys)
(require 'mf-theming)
(require 'mf-defaults)
;;; init.el ends here
