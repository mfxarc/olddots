;;; mf-key.el --- My keybinding setup -*- lexical-binding: t; -*-
(setup (:straight transient)
  (require 'transient))
(setup (:straight undo-fu))
(setup (:straight undo-fu-session)
  (global-undo-fu-session-mode))
(setup (:straight evil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-w-in-emacs-state nil)

  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-echo-state nil)
  (setq evil-window-map nil)
  (setq evil-respect-visual-line-mode t)
  (setq evil-default-state 'normal)
  (setq evil-undo-system 'undo-fu)
  (evil-mode)
  (dolist (mode '(custom-mode
                  eshell-mode
                  git-rebase-mode
                  erc-mode
                  circe-server-mode
                  circe-chat-mode
                  circe-query-mode
                  sauron-mode
                  vterm-mode
                  term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))
(setup (:straight evil-collection)
  (evil-collection-init '(magit help helpful doc-view proced vterm mpdel ibuffer compile calc calendar vterm)))
;;; --------------------------------------------------
(define-transient-command helpful-management
  "Helpful transient management."
  [
   ["Helpful"
    ("s" "Describe symbol at point" xref-find-definitions-other-window)
    ("f" "Describe function" helpful-function)
    ("v" "Describe variable" helpful-variable)
    ]
   [
    ""
    ("k" "Describe key" helpful-key)
    ("c" "Describe face" describe-face)
    ]
   ["Straight"
    ("u p" "Install package" straight-use-package)
    ("u d" "Pull package" straight-pull-package-and-deps)
    ("u r" "Remove unused packages" straight-remove-unused-repos)
    ]
   ]
  )

(define-transient-command file-management ()
  "Manage your files, archives and folders."
  [
   ["Find"
    ("g" "File in current dir" find-file)
    ("f" "File in work-dirs" fd-file)
    ("r" "Recent files" consult-buffer)
    ("j" "Current directory" dired-jump)
    ]
   ["Save"
    ("s" "Save" save-buffer)
    ("a" "Save as..." write-file)
    ]
   ["Create"
    ("c f" "File" make-directory)
    ("c d" "Folder" make-empty-file)
    ]
   ["Others"
    ("d f" "Delete file" delete-file)
    ("d d" "Delete directory" delete-directory)
    ("b" "Rename file" rename-file)
    ("v" "Copy file" copy-file)
    ]
   ["Sudo"
    ("u" "Find file"  sudo-find-file)
    ("e" "Edit current buffer" sudo-edit-file)
    ]
   ]
  )

(define-transient-command toggle-management
  "Toggles transient management."
  [
   ["Toggle"
    ("w"  "Word wrap" toggle-word-wrap)
    ("t"  "Truncate lines" toggle-truncate-lines)
    ("l"  "Line numbers" display-line-numbers-mode)
    ("C-f" "Flyspell" flyspell-mode)
    ]
   ["Margins"
    ("v v" "Margins enable/disable" visual-fill-column-mode)
    ("v l" "Max" (lambda() (interactive) (setq visual-fill-column-width '64)(visual-fill-column-mode)))
    ("v m" "Large" (lambda() (interactive) (setq visual-fill-column-width '74)(visual-fill-column-mode)))
    ("v s" "Small" (lambda() (interactive) (setq visual-fill-column-width '100)(visual-fill-column-mode)))
    ("v n" "Normal" (lambda() (interactive) (setq visual-fill-column-width '110)(visual-fill-column-mode)))
    ]
   ]
  )

(define-transient-command buffer-management
  "Buffer transient management."
  ["Buffer"
   ("b" "Scratch with current major mode" scratch)
   ("n" "New blank w/o mode" evil-buffer-new)
   ("r" "Revert chanes" revert-buffer)
   ("k" "Kill current buffer" kill-current-buffer)
   ("l" "List of buffers" ibuffer)
   ]
  )

(define-transient-command editor-management
  "Editor transient management."
  [
   ["Case"
    ("u"  "Upper" string-inflection-upcase)
    ("l"  "Lower" string-inflection-lower-camelcase)
    ("t"  "Capitalize" string-inflection-toggle)
    ]
   ["Symbols"
    ("c"   "Comment line" evilnc-comment-or-uncomment-lines)
    ("q"   "Quotes" insert-quotes)
    ("p"   "Parentheses" insert-parentheses)
    ("b"   "Brackets" insert-brackets)
    ("C-q" "Double-quotes" insert-double-quotes)
    ]
   ["Lorem Ipsum"
    ("i p" "Insert lorem paragraph" lorem-ipsum-insert-paragraphs)
    ("i s" "Insert lorem sentence" lorem-ipsum-insert-sentences)
    ("i l" "Insert lorem list" lorem-ipsum-insert-list)
    ]
   ]
  )

(defun global-key (&rest args)
  "Bind KEY to COMMAND globally."
  (dolist (arglist args)
    (dolist (state '(normal visual insert motion))
      (let* ((key (nth 0 arglist))
	     (command (nth 1 arglist)))
	(evil-global-set-key state (kbd key) command)))))

(defun state-key (state &rest args)
  "Bind KEY to COMMAND globally in specific STATE."
  (dolist (arglist args)
    (let* ((key (nth 0 arglist))
	   (command (nth 1 arglist)))
      (evil-global-set-key state (kbd key) command))))

(defun leader-key (&rest args)
  "Bind KEY to COMMAND globally in normal mode after leader."
  (dolist (arglist args)
    (dolist (state '(normal visual))
      (let* ((key (nth 0 arglist))
	     (command (nth 1 arglist)))
	(evil-global-set-key state (kbd (concat "SPC " key)) command)))))

(global-key '("s-u" exwm-switch-to-previous-buffer)
	    '("s-c" kill-window))

(state-key 'insert
	   '("C-f" corfu-insert)
	   '("C-j" corfu-next)
	   '("C-k" corfu-previous)
	   '("C-q" evil-normal-state)
	   '("C-h" backward-delete-char))

(state-key 'normal
	   '("C-s" consult-line)
	   '("C-l" end-of-line)
	   '("gh" end-of-buffer))

;; <SPC> Global leader keys
(leader-key '("SPC" execute-extended-command)
	    '("f" file-management)
	    '("b" buffer-management)
	    '("h" helpful-management)
	    '("t" toggle-management)
	    '("g" hugo-management)
	    '("q" editor-management)
	    '("u" uni-note-menu)
	    '("ns" magit-status-bare)
	    '("nf" magit-status-here)
	    )

;; <n> Leader keys to specific modes
;; Available keys -
;; "n" as local leader key
;; t C-t T C-T
;; m C-m M C-M
;; n C-n N C-N
;; , C-, < C-<
;; ç C-ç Ç C-Ç
;; s C-s S C-S
(provide 'mf-key)
;;; mf-key.el ends here
