;;; mf-meow.el --- My mf-meow.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;;
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: janeiro 28, 2022
;; Modified: janeiro 28, 2022
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(provide 'mf-meow)
;;; mf-meow.el ends here
