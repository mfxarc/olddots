;;; mf-setup.el --- My package management setup -*- lexical-binding: t; -*-
(require 'setup)
(setup-define :straight
  (lambda (recipe)
    `(unless (ignore-errors (straight-use-package ',recipe) t)
       (+setup-warn ":straight error: %S" ',recipe)
       ,(setup-quit)))
  :documentation
  "Install RECIPE with `straight-use-package'.
This macro can be used as HEAD, and will replace itself with the
first RECIPE's package."
  :repeatable t
  :shorthand (lambda (sexp)
               (let ((recipe (cadr sexp)))
                 (if (consp recipe)
                     (car recipe)
                   recipe))))
;; ---------------------------------
(setup-define :also-straight
  (lambda (recipe) `(setup (:straight ,recipe)))
  :documentation "Install RECIPE with `straight-use-package', after loading FEATURE."
  :repeatable t
  :after-loaded t)
;; ---------------------------------
(setup-define :git
  (lambda (recipe repo) `(straight-use-package '(,recipe :host github :repo ,repo)))
  :documentation "Install git RECIPE with straight-use-package"
  :repeatable t
  :shorthand #'cadr)
;; ---------------------------------
(setup-define :load-after
  (lambda (&rest features)
    (let ((body `(require ',(setup-get 'feature))))
      (dolist (feature (nreverse features))
	(setq body `(with-eval-after-load ',feature ,body)))
      body))
  :documentation "Load the current feature after FEATURES.")
;; ---------------------------------
(setup-define :with-state
  (lambda (state &rest body)
    (let (bodies)
      (dolist (state (ensure-list state))
        (push (let ((setup-opts (cons `(state . ,state) setup-opts)))
                (setup-expand body))
              bodies))
      (macroexp-progn (nreverse bodies))))
  :indent 1
  :debug '(sexp setup)
  :documentation "Change the evil STATE that BODY will bind to. If STATE is a list, apply BODY
  to all elements of STATE. This is intended to be used with ':bind'.")
;; ---------------------------------
(setup-define :bind
  (lambda (key command)
    (let ((state (cdr (assq 'state setup-opts)))
          (map (setup-get 'map))
          (key (setup-ensure-kbd key))
          (command (setup-ensure-function command)))
      (if state
          `(with-eval-after-load 'evil
	     (evil-define-key* ',state ,map ,key ,command))
	`(define-key ,map ,key ,command))))
  :documentation "Bind KEY to COMMAND in current map, and optionally for current evil states."
  :after-loaded t
  :debug '(form sexp)
  :repeatable t)
;; ---------------------------------
(provide 'mf-setup)
