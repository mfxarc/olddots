;;; mf-org.el --- My mf-org.el setup -*- lexical-binding: t; -*-

(setup (:straight org)
  (:with-hook org-mode-hook
    (:hook 'prettify-symbols-mode))
  ;; Defaults
  (setq org-confirm-babel-evaluate nil)
  (setq org-link-elisp-confirm-function nil)
  (setq org-M-RET-may-split-line nil)
  (setq org-insert-heading-respect-content t)
  (setq org-return-follows-link t)
  (setq org-mouse-1-follows-link nil)
  ;; Source blocks
  (setq org-src-preserve-indentation nil)
  (setq org-src-tab-acts-natively nil)
  (setq org-src-window-setup 'current-window)
  ;; Appearance
  (setq org-blank-before-new-entry nil)
  (setq org-fontify-quote-and-verse-blocks nil)
  (setq org-fontify-whole-heading-line nil)
  (setq org-fontify-whole-block-delimiter-line nil)
  (setq org-edit-src-content-indentation 0)
  (setq org-odt-fontify-srcblocks nil)
  (setq org-hide-leading-stars nil)
  (setq org-cycle-separator-lines 0)
  (setq org-descriptive-links t)
  (setq org-adapt-indentation t)
  (setq org-cycle-separator-lines 0)
  (setq line-spacing 0.07)
  (setq org-imenu-depth 3)
  (setq org-log-done 'time)
  (setq org-startup-truncated t)
  (setq org-startup-folded 'content)
  (setq org-support-shift-select 'always)
  (setq org-hide-emphasis-markers t)
  (setq org-todo-window-setup 'current-window)
  (setq org-log-into-drawer t)
  (setq org-todo-keywords '((sequence "TODO(t)" "|" "DONE(d)" "CANCELLED(c)" )))
  ;;; Images
  (setq image-use-external-converter t)
  (setq image-converter 'imagemagick)
  (setq org-startup-with-inline-images nil)
  (setq org-image-actual-width '(400))
  ;; Agenda
  (setq org-agenda-sorting-strategy '(scheduled-up))
  (setq org-agenda-include-deadlines t)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-unavailable-files t)
  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-start-on-weekday nil)
  (setq org-agenda-span 'day)
  (setq org-agenda-inhibit-startup t)
  (require 'org))

(setup (:straight ox-pandoc)
  (:load-after org)
  (:when-loaded
    (add-to-list 'org-export-backends 'pandoc)
    (setq org-pandoc-options
	  '((standalone . t)
	    (mathjax . t)
	    (variable . "revealjs-url=https://revealjs.com"))))
  (require 'ox-pandoc))
(require 'ob-awk)
(require 'ob-shell)
(require 'ob-emacs-lisp)
(require 'ob-lua)
(require 'ob-gnuplot)
(require 'ob-calc)
(require 'ob-lilypond)
(require 'org-tempo)
(require 'ox-latex)
(setup (:straight ob-mermaid))
(setup (:straight org-appear)
  (:hook-into org-mode-hook))
(setup (:straight literate-calc-mode))
(setup (:straight org-download)
  (setq org-download-backend "aria2c \"%s\" | xargs jpegoptim --strip-all -S 50k")
  (setq org-download-timestamp "")
  (setq org-download-screenshot-method "shotgun -g \"$(slop)\" %s")
  (setq org-download-screenshot-basename "screenshot.png")
  (org-download-enable))
(setup (:straight org-inline-pdf))
(setup (:straight org-fragtog)
  (:hook-into org-mode-hook))

(defface org-horizontal-rule
  '((default :inherit org-hide) (((background light)) :strike-through "gray70") (t :strike-through "gray30"))
  "Face used for horizontal ruler.")

(font-lock-add-keywords
 'org-mode
 '(("^ *\\([-]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))
   ("^ *\\([+]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "◦"))))
   ("^-\\{5,\\}$" 0 '(face org-horizontal-rule display (space :width text)))))

(setq prettify-symbols-unprettify-at-point 'right-edge)
(setup (:straight mixed-pitch)
  (:hook-into org-mode-hook eww-mode-hook))
(add-hook 'text-mode-hook 'auto-fill-mode)
;; https://www.reddit.com/r/emacs/comments/9wukv8/hide_all_stars_in_org_mode/
(defun org-hide-stars ()
  "Make org heading star invisible."
  (font-lock-add-keywords
   nil '(("^\\*+ " (0 (prog1 nil (put-text-property (match-beginning 0)
						    (match-end 0) 'face
						    (list :foreground (face-attribute 'default :background)))))))))

(defun read-mode ()
  (setq-local visual-fill-column-width '64)
  (setq-local word-wrap t)
  (setq-local adaptive-wrap-prefix-mode t)
  (visual-line-mode)
  (org-hide-stars)
  (org-inline-pdf-mode)
  (visual-fill-column-mode))
(add-hook 'org-mode-hook 'read-mode)

(defun org-bold ()
  "Make as bold the current region or word."
  (interactive)
  (surround-word-or-region "*" "*"))

(defun org-italic ()
  "Make italic the current region or word."
  (interactive)
  (surround-word-or-region "\/" "\/"))

(defun org-verbatim ()
  "Make italic the current region or word."
  (interactive)
  (surround-word-or-region "=" "="))

(defun org-html-export-on-save ()
  (interactive)
  (if (memq 'org-html-export-to-html after-save-hook)
      (progn
        (remove-hook 'after-save-hook 'org-html-export-to-html t)
        (message "Disabled org html export on save for current buffer..."))
    (add-hook 'after-save-hook 'org-html-export-to-html nil t)
    (message "Enabled org html export on save for current buffer...")))
;;

(define-transient-command org-menu ()
  "Menu to manage org-mode."
  [
   ["Insert"
    ("l" "Insert link" org-insert-link)
    ("p" "Paste image" org-download-yank)
    ("d" "Create table" org-table-create)
    ]
   ["Markup"
    ("C-b" "Make bold" org-bold)
    ("C-v" "Make verbatim" org-verbatim)
    ("C-i" "Make italic" org-italic)
    ("f" "Format buffer" format-buffer)
    ]
   ["Export"
    ("e" "Various formats" org-export-dispatch)
    ("b" "Babel tangle" org-babel-tangle)
    ("c" "Babel clean results" org-babel-remove-result-one-or-many)
    ]
   ["Toggle"
    ("t" "Todo" org-todo)
    ("h" "Heading" org-toggle-heading)
    ("x" "Checkbox state"org-toggle-checkbox)
    ("i" "Bullet item"org-toggle-item)
    ("I" "Inline image" org-toggle-inline-images)
    ("s" "Sort" org-sort)
    ]
   ]
  )
(provide 'mf-org)
;;; mf-org.el ends here
