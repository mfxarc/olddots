;;; mf-documents.el --- My mf-documents.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 26, 2022
;;
;;; Commentary:
;;  
;;  My configuration for mf-documents.el
;;  
;;; Code:

(defun groff-compile ()
  "Compile groff document and open in Zathura."
  (interactive)
  (let* ((file (file-name-nondirectory buffer-file-name))
	 (file-ext (file-name-extension file))
	 (file-pdf (concat (file-name-base file) ".pdf")))
    (when (and (eq major-mode 'nroff-mode)
	       (string-match "ms" file-ext))
      (save-buffer)
      (start-process-shell-command "groff" nil
				   (format "groff -k -ms -tbl -T pdf %s > %s" file file-pdf))
      (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" file-pdf))))
	(start-process "zathura" nil "zathura" file-pdf))
      (message "%s compiled" file))))
(add-hook 'after-save-hook 'groff-compile)

(defun latex-compile ()
  "Compile latex document and open in Zathura."
  (interactive)
  (let* ((file (file-name-nondirectory buffer-file-name))
	 (file-base (file-name-base file))
	 (file-ext (file-name-extension file))
	 (file-pdf (concat file-base ".pdf"))
	 (file-log (concat file-base ".log")))
    (when (and (eq major-mode 'latex-mode)
	       (string-match "tex" file-ext))
      (start-process-shell-command
       "xelatex" nil
       (format "xelatex -interaction=batchmode %s 1>/dev/null" file))
      (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" file-pdf))))
	(start-process "zathura" nil "zathura" file-pdf)
	(delete-file file-log))
      (message "%s compiled" file))))

(defun lilypond-compile ()
  "Compile lilypond document and open in Zathura."
  (interactive)
  (let* ((file (file-name-nondirectory buffer-file-name))
	 (file-ext (file-name-extension file))
	 (file-pdf (concat (file-name-base file) ".pdf")))
    (when (string-match "ly" file-ext)
      (let ((command (format "lilypond -s %s" file)))
	(compilation-start command)
	(when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" file-pdf))))
	  (start-process "zathura" nil "zathura" file-pdf))))))

(provide 'mf-documents)
;;; mf-documents.el ends here
