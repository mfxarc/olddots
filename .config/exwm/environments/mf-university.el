;;; mf-university.el --- My mf-university.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://gitlab.com/mfxarc>
;; Created: march 16, 2022
;;
;;; Commentary:
;;
;;; Code:
(require 'transient)
(require 'consult)
(require 'org)

;; ----------------- Variables

(defvar uni-root-path "~/uni-notes"
  "Defaults path to university notes.")

(defvar uni-latex-template (expand-file-name "template/notebook.org")
  "Defaults path to latex export template.")

(defvar uni-latex-enable nil
  "Enable experimental latex options or not.")

;; ----------------- Little utils

(defun uni--list-notes ()
  "List notes of uni directory"
  (directory-files uni-root-path nil ".org" nil nil))

(defun uni--get-title (file)
  "Get title of note."
  (replace-regexp-in-string
   ".*: " "" (substring
	      (shell-command-to-string
	       (format "rg -F -I '#+title:' %s" (expand-file-name file uni-root-path))) 0 -1)))

(defun uni--get-title-path (file)
  "Get title and path of note as a list."
  (list (expand-file-name file uni-root-path) (uni--get-title file)))

(defun uni--insert-note (file)
  "Insert note as a org-link."
  (insert "[[file:" (nth 0 (uni--get-title-path file)) "]["
	  (nth 1 (uni--get-title-path file)) "]]"))

(defun uni--rg-file (str)
  "Return a list of lines containing regexp STR."
  (let* ((default-directory uni-root-path)
	 (files (shell-command-to-string
		 (concat "rg -t org -F -I " (shell-quote-argument str) " 2>/dev/null"))))
    (split-string files "\n" t)))

(defun uni--rg-file-list (str)
  "Return a list of files containing regexp STR."
  (let* ((default-directory uni-root-path)
	 (files (shell-command-to-string
		 (concat "rg -t org -F -l " (shell-quote-argument str) " 2>/dev/null"))))
    (split-string files "\n" t)))

;; TODO Tabulated menu to see all notes with following columns
;;      - path | title | class | date
;; Add
;; (mapcar (apply-partially #'format "BEFORE %s AFTER")
;;         (directory-files uni-root-path))

;; ----------------- Main functions
(defun uni-new-note (title)
  "New university note."
  (interactive "sDiscipline topic name: ")
  (let* ((filename (downcase (replace-regexp-in-string "[[:space:]]+" "_" title)))
	 (fileorg (concat filename ".org"))
	 (filepathorg (expand-file-name fileorg uni-root-path))
	 (filepath (expand-file-name filename uni-root-path))
	 (fileother (expand-file-name title uni-root-path))
	 (date (format-time-string "%Y-%m-%d")))
    (if (or (file-exists-p filepath)
	    (file-exists-p fileother)
	    (file-exists-p filepathorg)
	    (file-exists-p (expand-file-name title uni-root-path)))
	(find-file filepath)
      (progn (make-empty-file filepathorg)
	     (find-file filepathorg)
	     (insert "#+title: " title)
	     (insert "\n#+id: " (read-string "Class id: ")
		     "\n#+class: " (read-string "Class name: ")
		     "\n#+date: " date)
	     (when uni-latex-enable
	       (insert "\n#+setupfile: " uni-latex-template))
	     (insert "\n\n\n\n* References\n")
	     (if uni-latex-enable
		 (goto-line 7)
	       (goto-line 6))
	     (write-file filepathorg))
      (message filename))))

(defun uni-delete-note ()
  "Delete university note."
  (interactive)
  (let* ((file (completing-read "Delete blog note: " (uni--list-notes)))
	 (filepath (expand-file-name file uni-root-path)))
    (when (yes-or-no-p (format "Really want to delete this note? %s" file))
      (progn (delete-file filepath)
	     (message "Note delete: %s" file)))))

(defun uni-rename-note ()
  "Rename university note."
  (interactive)
  (let* ((default-directory uni-root-path)
	 (file (completing-read "Rename blog note: " (uni--list-notes)))
	 (filepath (expand-file-name file uni-root-path))
	 (title (uni--get-title filepath))
	 (renamed (read-string (format "Rename %s to: " title)))
	 (filename (concat (downcase (replace-regexp-in-string "[[:space:]]+" "_" renamed)) ".org")))
    (when (shell-command (format "sd \"%s\" \"%s\" %s" (concat "title: " title) (concat "title: " renamed) filepath))
      (rename-file filepath filename)
      (message "Note renamed to: %s" renamed))))

;; --- Searching

(defun uni-find-note ()
  "List university notes."
  (interactive)
  (uni-new-note
   (completing-read "Select note: " (uni--list-notes))))

(defun uni-find-class ()
  "Find notes by class."
  (interactive)
  (let ((files (uni--rg-file "#+id:")))
    (if files
	(let* ((class (replace-regexp-in-string ".*[[:space:]]+" ""
						(completing-read "Find note by class: " files)))
	       (note (completing-read (format "Notes of class %s: " class)
				      (uni--rg-file-list (format "#+id: %s" class)))))
	  (find-file (expand-file-name note uni-root-path)))
      (user-error "No notes found"))))

(defun uni-find-rg ()
  "Return a list of files containing regexp STR."
  (interactive)
  (consult-ripgrep uni-root-path))

;; --- Inserting

(defun uni-insert-note ()
  "Insert note of notes list as a org-link."
  (interactive)
  (uni--insert-note (completing-read "Insert note: " (uni--list-notes))))

;; --- Exporting

(when uni-latex-enable
  (add-to-list 'org-latex-logfiles-extensions "tex")
  (setq org-export-with-toc nil)
  (setq org-export-coding-system 'utf-8-unix)
  (setq org-latex-pdf-process '("xelatex -interaction=batchmode %f"))
  (setq org-latex-compiler "xelatex")
  (setq org-latex-remove-logfiles t)
  (add-to-list 'org-latex-classes '("notebook"
				    "\\documentclass{article}"
				    ("\\section{%s}" . "\\section*{%s}")
				    ("\\subsection{%s}" . "\\subsection*{%s}")
				    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				    ("\\paragraph{%s}" . "\\paragraph*{%s}")
				    ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; ----------------- Transient menus

(define-transient-command uni-note-menu ()
  "Menu to manage university notes."
  [
   ["Management"
    ("c"  "Create new note" uni-new-note)
    ("d"  "Delete existent note" uni-delete-note)
    ("C-r"  "Rename note" uni-rename-note)
    ]
   ["Searching"
    ("f"  "Find notes" uni-find-note)
    ("s"  "Find notes by class" uni-find-class)
    ("r"  "Find notes by regex" uni-find-rg)
    ]
   ["Insert"
    ("o"  "Insert note as org-link" uni-insert-note)
    ]
   ]
  )

(provide 'mf-university)
;;; mf-university.el ends here
