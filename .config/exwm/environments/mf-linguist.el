;;; mf-linguist.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 15, 2022
;;
;;; Commentary:
;;  
;; My configuration for language learning, linguistic work
;; and everything related to human languages.
;;  
;;; Code:

(setq ispell-program-name "hunspell")
(setup flyspell
  (setq flyspell-issue-message-flag nil)
  (let ((langs '("en_US" "pt_BR")))
    (setq lang-ring (make-ring (length langs)))
    (dolist (elem langs) (ring-insert lang-ring elem)))
  (defun cycle-ispell-languages ()
    (interactive)
    (let ((lang (ring-ref lang-ring -1)))
      (ring-insert lang-ring lang)
      (ispell-change-dictionary lang))))

(setup (:straight flyspell-correct))
(setup (:straight languagetool)
  (:option languagetool-java-arguments '("-Dfile.encoding=UTF-8" "-cp" "/usr/share/languagetool:/usr/share/java/languagetool/*")
	   languagetool-console-command "org.languagetool.commandline.Main"
	   languagetool-server-command "org.languagetool.server.HTTPServer"))

(setq ispell-dictionary "pt_BR")
(setq default-input-method "korean-hangul")

(defun lookup-in (engine)
  "Look up the WORD under cursor in ENGINE.
	   If there is a text selection (a phrase), use that."
  (let ((word (replace-regexp-in-string " " "_" (get-word-regional))))
    (eww-browse-url (concat engine word))))
(defun lookup-in-dicio ()
  "Look up in dicio."
  (interactive)
  (lookup-in "https://www.dicio.com.br/"))
(defun lookup-in-wikipedia ()
  "Look up in wikipedia."
  (interactive)
  (lookup-in "http://en.wikipedia.org/wiki/"))

(defun translate-at-point ()
  "Translate word or region at point"
  (interactive)
  (let* ((language (completing-read "Translate to: " '("en" "fr" "ko" "it" "pt" "es")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b :%s %s" ,language ,word)))
		 (lambda (result)
		   (pos-tip-show-no-propertize result 'default)
                   (kill-new result)))))

(defun define-word-at-point ()
  "Define word"
  (interactive)
  (let ((word (prin1-to-string (current-word))))
    (async-start `(lambda () (shell-command-to-string (format "sdcv -nj %s | jq -r '.[2,1].definition' | sed -e '/^$/d' | fmt -cu -w80" ,word)))
		 (lambda (result) (pos-tip-show-no-propertize result 'org-verbatim)))))

(defun speak-at-point ()
  "Talk word or region at point"
  (interactive)
  (let* ((language (completing-read "Speak in: " '("en" "fr" "ko" "it" "pt")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b -p :%s %s" ,language ,word))) nil)))

(defun define-word-at-point ()
  "Define word at point and print results in buffer."
  (interactive)
  (let* ((word (current-word))
	 (buffer (get-buffer-create (concat word "-definition"))))
    (with-current-buffer buffer
      (insert (shell-command-to-string "sdcv -nj %s | jq -r '.[2,1].definition' | sed '/^$/d'"))
      (goto-char 0))
    (display-buffer buffer '(display-buffer-at-bottom . nil))
    (switch-to-buffer-other-window buffer)))

;; 너
;; sdcv -nj book | jq -r '.[1].definition' | sed -e "/^$/d" -e "s/\///g" | fmt -cu -w80

;; English pronunciation and meaning
;; sdcv -jn banana | jq -r '.[2,1].definition'

(provide 'mf-linguist)
;;; mf-linguist.el ends here
