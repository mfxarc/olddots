;;; exwm.el --- My exwm setup -*- lexical-binding: t; -*-
(use-package exwm)
(use-package exwm-edit)
(use-package app-launcher 
	     :straight
	     '(app-launcher :host github :repo "SebastienWae/app-launcher"))
;; IF A POPUP DOES HAPPEN, DON'T RESIZE WINDOWS TO BE EQUAL_SIZED
(setq display-buffer-base-action
      '(
	display-buffer-reuse-mode-window
	display-buffer-reuse-window
	display-buffer-same-window
	))
(setq even-window-sizes nil)
;; SET DEFAULT MODE TO CHAR MODE
(setq exwm-manage-configurations '((t char-mode t)))
;; ONLY SHOW BUFFER IN THEIR RIGHT WORKSPACE
(setq exwm-layout-show-all-buffers t)
(setq exwm-workspace-show-all-buffers t)
;; SET NUMBER OF WORKSPACES
(setq exwm-workspace-number 4)
;; NAME WORKSPACE FROM 1 INSTED OF 0
(setq exwm-workspace-index-map (lambda (index) (number-to-string (1+ index))))
;; HIDE MODELINE BY DEFAULT IN X WINDOWS
(add-hook 'exwm-manage-finish-hook #'exwm-layout-hide-mode-line)
;; HIDE MODELINE FOR FLOATING WINDOWS
(add-hook 'exwm-floating-setup-hook #'exwm-layout-hide-mode-line)
;; UPDATE TITLE NAME
(add-hook 'exwm-update-class-hook
	  (lambda () (exwm-workspace-rename-buffer (format "%s" exwm-title))))
;; GO TO NEXT WORKSPACE
(defun exwm-go-to-next-workspace ()
  (interactive)
  (exwm-workspace-switch (+ exwm-workspace-current-index 1)))
;; GO TO PREVIOUS WORKSPACE
(defun exwm-go-to-prev-workspace ()
  (interactive)
  (exwm-workspace-switch (- exwm-workspace-current-index 1)))
;; MOVE BUFFER TO NEXT WORKSPACE
(defun exwm-move-buffer-to-next-workspace ()
  (interactive)
  (exwm-workspace-move-window (+ exwm-workspace-current-index 1)))
;; MOVE BUFFER TO PREV WORKSPACE
(defun exwm-move-buffer-to-prev-workspace ()
  (interactive)
  (exwm-workspace-move-window (- exwm-workspace-current-index 1)))
;; DELETE NEXT WORKSPACE
(defun exwm-del-next-workspace ()
  (interactive)
  (exwm-workspace-delete (+ exwm-workspace-current-index 1)))
;; DELETE PREVIOUS WORKSPACE
(defun exwm-del-prev-workspace ()
  (interactive)
  (exwm-workspace-delete (- exwm-workspace-current-index 1)))
;; ---------- SPLIT VERTICALLY
(defun exwm-split-window-right ()
  (interactive)
  (progn (split-window-right)
	 (windmove-right)
	 (switch-to-buffer "scratch")))
;; ---------- SPLIT HORIZONTALLY
(defun exwm-split-window-below ()
  (interactive)
  (progn (split-window-below)
	 (windmove-down)
	 (switch-to-buffer "scratch")))
;; ---------- TOGGLE BETWEEN HORIZONTAL/VERTICAL SPLIT
(defun exwm-toggle-window-split ()
  "switch between vertical and horizontal split only works for 2 windows"
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))
;; ---------- CLOSE OTHER WINDOWS AND KEEP ONLY THE FOCUSED
(defun exwm-toggle-single-window ()
  (interactive)
  (if (= (count-windows) 1)
      (when single-window--last-configuration
        (set-window-configuration single-window--last-configuration))
    (setq single-window--last-configuration (current-window-configuration))
    (delete-other-windows)))

;; ---------- SWITCH TO PREVIOUS BUFFER
(defun exwm-switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))
;; ---------- KILL CURRENT BUFFER WITHOUT ASKING
(defun exwm-kill-buffer-noask ()
  (interactive)
  (set-buffer-modified-p nil)
  (kill-current-buffer))

;; ----- LAUNCH PROGRAMS OR RUN SHELL COMMANDS
(defun exwm-command (name command)
  (start-process-shell-command name nil command))
;; ----- PROMPT TO INSTALL PACKAGES IN THE SYSTEM
(defun exwm-paru-install-prompt (command)
  (interactive (list(completing-read "Paru Install: " (split-string(shell-command-to-string "paru -Slq")))))
  (start-process-shell-command (concat "paru install" command) nil (concat " st -e paru -S " command)))
;; ----- PROMPT TO UNINSTALL SYSTEM PACKAGES
(defun exwm-pacman-uninstall-prompt (command)
  (interactive (list(completing-read "Pacman Uninstall: " (split-string(shell-command-to-string "pacman -Q")))))
  (start-process-shell-command (concat "Pacman uninstall" command) nil (concat "st -e sudo pacman -Rns " command)))
;; ----- TOGGLE MODELINE
(defun toggle-mode-line ()
  (interactive)
  (setq mode-line-format (if (equal mode-line-format nil)
			     (default-value 'mode-line-format)) )
  (redraw-display))

(add-hook 'exwm-update-title-hook (lambda () (pcase exwm-class-name
					       ("firefox" (exwm-workspace-rename-buffer (format "Firefox: %s" exwm-title)))
					       ("libreoffice-startcenter" (exwm-workspace-rename-buffer (format "LibreOffice: %s" exwm-title)))
					       ("libreoffice-writer" (exwm-workspace-rename-buffer (format "LibreOffice: %s" exwm-title)))
					       ("mpv" (exwm-workspace-rename-buffer (format "%s" exwm-title)))
					       ("zathura" (exwm-workspace-rename-buffer (format "zathura: %s" exwm-title)))
					       ("st" (exwm-workspace-rename-buffer (format "terminal: %s" exwm-title)))
					       ("gimp" (exwm-workspace-rename-buffer (format "gimp: %s" exwm-title))))))

(defun exwm-window-rules ()
  (interactive)
  (exwm-layout-hide-mode-line)
  (pcase exwm-class-name
    ;; HOME WORKSPACE
    ;; DEV WORKSPACE
    ("St" (exwm-workspace-move-window 0)(exwm-workspace-switch 0))
    ;; DOX/MEDIA WORKSPACE
    ("mpv" (exwm-workspace-move-window 1)(exwm-workspace-switch 1))
    ("zathura" (exwm-workspace-move-window 1)(exwm-workspace-switch 1))
    ("firefox" (exwm-workspace-move-window 1)(exwm-workspace-switch 1))
    ;; WORK WORKSPACE
    ("Gimp" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))
    ("Inkscape" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))
    ("libreoffice-writer" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))
    ("libreoffice-startcenter" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))
    ;; CHAT WORKSPACE
    ("discord" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))
    ("Microsoft Teams - Preview" (exwm-workspace-move-window 2)(exwm-workspace-switch 2))))
;; ADD HOOK
(add-hook 'exwm-manage-finish-hook 'exwm-window-rules)

(defun polybar ()
  "Launch polybar"
  (interactive)
  (exwm-command "polybar" "killall polybar ; polybar main"))

(defun exwm-key (key function)
  (exwm-input-set-key (kbd key)	function)
  (global-set-key (kbd key) function))
(defun exwm-shell-prompt (command)
  (interactive
   (list (read-shell-command "$ ")))
  (start-process-shell-command command nil command))
(defun emacs-reload-init ()
  (interactive)
  (load-file (expand-file-name "init.el" user-emacs-directory)))
;; LAUNCH APPS
(exwm-key "s-p"     'app-launcher-run-app)
(exwm-key "C-s-p"   'exwm-shell-prompt)
(exwm-key "C-s-1"   'polybar)
(exwm-key "s-<return>" (lambda() (interactive) (exwm-command "firefox" "firefox")))
(exwm-key "C-s-w"   (lambda() (interactive) (exwm-command "dmbmarks" "dmbmarks")))
(exwm-key "C-s-s"   (lambda() (interactive) (exwm-command "dmsearch" "dmsearch")))
(exwm-key "C-s-b"   (lambda() (interactive) (exwm-command "dmaddbmark" "dmaddbmark")))
(exwm-key "<print>" (lambda() (interactive) (exwm-command "screenshot" "deepin-screenshot")))
;; APP
(exwm-key "s-a f"	  #'elfeed)
(exwm-key "s-a m"   #'emms-play-file)
(exwm-key "s-a d"   #'dashboard-refresh-buffer)
;; ROA
(exwm-key "s-a o f" #'org-roam-node-find)
(exwm-key "s-a o c" #'org-roam-dailies-capture-today)
;; UTI
(exwm-key "s-f"     #'find-file)
(exwm-key "s-q"     #'consult-ripgrep)
(exwm-key "s-r"     #'exwm-edit--compose)
(exwm-key "s-w"     #'execute-extended-command)
(exwm-key "C-s-d"   #'dired-jump)
(exwm-key "s-0"     #'exwm-paru-install-prompt)
(exwm-key "s-9"     #'exwm-pacman-uninstall-prompt)
(exwm-key "s-t"     #'vterm)
(exwm-key "s-y"     (lambda() (interactive) (exwm-command "st" "st")))
(exwm-key "s-,"     'emms-previous)
(exwm-key "s-;"     'emms-next)
(exwm-key "C-s-."   'emms-play-file)
(exwm-key "s-."     'emms-pause)
;; BUF/WORE SHORTCUTS
(exwm-key "s-e"     #'consult-buffer)
(exwm-key "C-s-a"   #'consult-bookmark)
;; WINS
(exwm-key "C-s-f"   #'exwm-layout-toggle-fullscreen)
(exwm-key "C-s-t"   #'exwm-floating-toggle-floating)
(exwm-key "s-M"     #'toggle-mode-line)
;; WIN
(exwm-key "s-j"     #'windmove-down)
(exwm-key "s-k"     #'windmove-up)
(exwm-key "s-h"     #'windmove-left)
(exwm-key "s-l"     #'windmove-right)
;; WINNT
(exwm-key "s-J"     #'windmove-swap-states-down)
(exwm-key "s-K"     #'windmove-swap-states-up)
(exwm-key "s-L"     #'windmove-swap-states-right)
(exwm-key "s-H"     #'windmove-swap-states-left)
;; WINNG
(exwm-key "s-C-j"   #'enlarge-window)
(exwm-key "s-C-k"   #'shrink-window)
(exwm-key "s-C-l"   #'enlarge-window-horizontally)
(exwm-key "s-C-h"   #'shrink-window-horizontally)
;; WIN
(exwm-key "s-v"     #'exwm-split-window-right)
(exwm-key "s-s"     #'exwm-split-window-below)
(exwm-key "s-ç"     #'exwm-toggle-single-window)
(exwm-key "C-s-ç"   #'exwm-toggle-window-split)
;; EXWE
(exwm-key "C-s-q"   #'exwm-workspace-move-window)
(exwm-key "s-u"     #'exwm-switch-to-previous-buffer)
(exwm-key "s-i"     #'exwm-go-to-prev-workspace)
(exwm-key "s-o"     #'exwm-go-to-next-workspace)
(exwm-key "C-s-i"   #'exwm-move-buffer-to-prev-workspace)
(exwm-key "C-s-o"   #'exwm-move-buffer-to-next-workspace)
(exwm-key "s-1"     (lambda() (interactive) (exwm-workspace-switch 0)))
(exwm-key "s-2"     (lambda() (interactive) (exwm-workspace-switch 1)))
(exwm-key "s-3"     (lambda() (interactive) (exwm-workspace-switch 2)))
(exwm-key "s-4"     (lambda() (interactive) (exwm-workspace-switch 3)))
(exwm-key "s-5"     (lambda() (interactive) (exwm-workspace-switch 4)))
(exwm-key "C-s-1"   (lambda() (interactive) (exwm-workspace-move-window 0)))
(exwm-key "C-s-2"   (lambda() (interactive) (exwm-workspace-move-window 1)))
(exwm-key "C-s-3"   (lambda() (interactive) (exwm-workspace-move-window 2)))
(exwm-key "C-s-4"   (lambda() (interactive) (exwm-workspace-move-window 3)))
(exwm-key "C-s-5"   (lambda() (interactive) (exwm-workspace-move-window 4)))
;; DEL
(exwm-key "s-x"     #'exwm-kill-buffer-noask)
(exwm-key "s-c"     #'delete-window)
(exwm-key "C-M-s-q" #'save-buffers-kill-emacs)

(when (get-buffer "*window-manager*")
  (kill-buffer "*window-manager*"))
(when (get-buffer "*window-manager-error*")
  (kill-buffer "*window-manager-error*"))
(when (executable-find "wmctrl")
  (shell-command "wmctrl -m ; echo $?" "*window-manager*" "*window-manager-error*"))
;; if there was an error detecting the window manager, initialize EXWM
(when (and (get-buffer "*window-manager-error*")
           (eq window-system 'x))
  ;; exwm startup goes here
  (exwm-enable)
  (exwm-command "polybar" "killall polybar ; sleep 3 && polybar main"))
(provide 'exwm)
;;; exwm.el ends here
