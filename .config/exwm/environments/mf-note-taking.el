;;; mf-note-taking.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 15, 2022
;;
;;; Commentary:
;;
;;  My configuration for note-taking
;;
;;; Code:

(setup (:straight org-roam)
  (setq org-roam-db-gc-threshold most-positive-fixnum)
  (setq org-roam-node-default-sort nil)
  (setq org-roam-completion-everywhere t)
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (:when-loaded (org-roam-db-autosync-mode)))

(setup (:straight org-roam-ui))

(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (push arg args))
	(org-roam-capture-templates (list (append (car org-roam-capture-templates)
						  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))

(defun org-roam-rg ()
  "Ripgrep into org-roam-directory"
  (interactive)
  (consult-ripgrep org-roam-directory))

(require 'org-protocol)
(setq org-html-validation-link nil)

(define-transient-command org-roam-management ()
  "Manage your hugo blog."
  [
   ["Capture"
    ("c c" "templates" org-capture)
    ("c h" "today" org-roam-dailies-capture-today)
    ("c d" "date" org-roam-dailies-capture-date)
    ("c a" "tomorrow" org-roam-dailies-capture-tomorrow)
    ]
   ["Find"
    ("f f" "nodes" org-roam-node-find)
    ("f y" "yesterday" org-roam-dailies-find-yesterday)
    ("f h" "today" org-roam-dailies-find-today)
    ("f d" "date" org-roam-dailies-find-date)
    ("f a" "tomorrow" org-roam-dailies-find-tomorrow)
    ]
   ["Others"
    ("i" "insert note" org-roam-node-insert-immediate)
    ("o" "Roam UI" org-roam-ui-mode)
    ("s" "Ripgrep notes" org-roam-rg)
    ]
   ])

(provide 'mf-note-taking)
;;; mf-note-taking.el ends here
