-- copy-link.lua
function copyPermalink()
    local pos = mp.get_property_number("time-pos")
    local uri = mp.get_property("path")
    if string.match(uri, "https") then
        local bookmark = string.format("%s&t=%i", uri, pos)
        local pipe = io.popen("xclip -sel clip", "w")
        pipe:write(bookmark)
        pipe:close()
    else -- Local file
        local time_in_seconds = pos
        local time_seg = pos % 60
        pos = pos - time_seg
        local time_hours = math.floor(pos / 3600)
        pos = pos - (time_hours * 3600)
        local time_minutes = pos / 60
        time = string.format("%02d:%02d:%02d", time_hours, time_minutes, time_seg)
        local bookmark = string.format('"%s - %s"', time, uri)
        local pipe = io.popen("xclip -sel clip", "w")
        pipe:write(bookmark)
        pipe:close()
    end
    mp.osd_message("Link to position copied to clipboard")
end

function openPermalink()
    local uri = mp.get_property("path")
    if string.match(uri, "https") then
        mp.osd_message("Opening in browser...")
        local pos = mp.get_property_number("time-pos")
        local bookmark = string.format(" %s&t=%i", uri, pos)
        local pipe = io.popen("browser " .. bookmark)
        pipe:close()
    else
        mp.osd_message("It's a local file.")
    end
end

function appendPermalink()
    local pipe = io.popen("xclip -o -sel clip", "r")
    local clipboard = pipe:read("*all")
    mp.commandv("loadfile", clipboard, "append-play")
    mp.osd_message("Clipboard: " .. clipboard)
    pipe:close()
end

mp.add_key_binding("ctrl+y", "copy-permalink", copyPermalink)
mp.add_key_binding("ctrl+o", "open-permalink", openPermalink)
mp.add_key_binding("a", "append-permalink", appendPermalink)
