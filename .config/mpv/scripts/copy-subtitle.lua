-- copy-subtitle.lua

function copy_subtitle ()
  local subtitle = mp.get_property("sub-text")
  mp.commandv(
    "run", "/bin/bash", "-c",
    "echo -n \"" .. subtitle:gsub("\"", "\\%0") .. "\" | LANG=en_US.UTF-8 xclip -selection primary"
  )
  mp.osd_message("Subtitle line copied to clipboard")
end

mp.add_key_binding("y", "copy-subtitle", copy_subtitle)
