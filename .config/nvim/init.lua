-- init.lua
local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({"git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path})
    vim.cmd "packadd packer.nvim"
end

require("packer").startup(
    function()
        use "wbthomason/packer.nvim"
        use "hoob3rt/lualine.nvim"
        use "is0n/fm-nvim"
        use "windwp/nvim-autopairs"
        use "folke/which-key.nvim"
        use "beauwilliams/focus.nvim"
        use {"nvim-telescope/telescope.nvim", requires = {{"nvim-lua/plenary.nvim"}}}
        use {"michaelb/sniprun", run = "bash ./install.sh"}
        use "terrortylor/nvim-comment"
        use "sbdchd/neoformat"
        use "ellisonleao/gruvbox.nvim"
        use "lukas-reineke/indent-blankline.nvim"
        use "nvim-treesitter/nvim-treesitter"
        use "nvim-orgmode/orgmode"
        use "akinsho/org-bullets.nvim"
        use "L3MON4D3/LuaSnip"
    end
)

-- settings.lua
local opt = vim.opt -- global/buffer/windows-scoped options
local cmd = vim.cmd -- command
local set = vim.g -- command
-----------------------------------------------------------
-- System
-----------------------------------------------------------
opt.clipboard = "unnamedplus" -- copy/paste to system clipboard
opt.hidden = true -- enable background buffers
opt.history = 100 -- remember n lines in history
opt.lazyredraw = true -- faster scrolling
opt.synmaxcol = 240 -- max column for syntax highlight
opt.mouse = "a" -- enable your mouse
opt.encoding = "utf-8" -- the encoding displayed
opt.fileencoding = "utf-8" -- the encoding written to file
opt.autoindent = true -- good auto indent
opt.smartcase = true -- ignore lowercase for the whole pattern
opt.smartindent = true -- autoindent new lines
opt.tabstop = 2 -- insert 2 spaces for a tab
opt.shiftwidth = 2 -- change the number of space characters inserted for indentation
opt.smarttab = true -- makes tabbing smarter will realize you have 2 vs 4
opt.expandtab = true -- converts tabs to spaces
opt.showtabline = 2 -- always show tabs
opt.showmode = false -- we don't need to see things like -- INSERT -- anymore
opt.backup = false -- this is recommended by coc
opt.writebackup = false -- this is recommended by coc
opt.updatetime = 300 -- faster completion
opt.timeoutlen = 500 -- by default timeoutlen is 1000 ms
opt.autochdir = true -- working directory will always be the same as your working directory

-----------------------------------------------------------
-- Neovim UI
-----------------------------------------------------------
opt.syntax = "enable" -- enable syntax highlighting
opt.showmatch = true -- highlight matching parenthesis
opt.foldmethod = "marker" -- enable folding (default 'foldmarker')
opt.splitright = true -- vertical split to the right
opt.splitbelow = true -- orizontal split to the bottom
opt.ignorecase = true -- ignore case letters when search
opt.termguicolors = true -- enable 24-bit RGB colors
opt.number = true -- enable current line number
opt.relativenumber = true -- enable relative line numbers
opt.pumheight = 10 -- makes popup menu smaller
set.aniseedenv = true
set.cursorline = false
set.cursorcolumn = false
set.scrolljump = 5
set.lazyredraw = true
set.synmaxcol = 180
-----------------------------------------------------------
-- Neovim EXT-UI
-----------------------------------------------------------
cmd [[colorscheme gruvbox]]
cmd [[hi Normal guibg=NONE ctermbg=NONE]] -- Make neovim background transparent
require("which-key").setup {}
require("focus").setup()
set.bufferline = {["icons"] = false}
local actions = require("telescope.actions")
require("telescope").setup {
    defaults = {
        find_command = {
            "rg",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
            "--smart-case"
        },
        initial_mode = "insert",
        selection_strategy = "reset",
        sorting_strategy = "descending",
        layout_strategy = "horizontal",
        prompt_prefix = "   ",
        selection_caret = " ",
        layout_config = {
            width = 0.75,
            preview_cutoff = 120,
            prompt_position = "bottom",
            vertical = {mirror = false},
            horizontal = {
                mirror = false,
                preview_width = 0.6
            }
        },
        file_sorter = require("telescope.sorters").get_fuzzy_file,
        file_ignore_patterns = {".git", "node_modules", "__pycache__"},
        generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
        winblend = 0,
        scroll_strategy = "cycle",
        border = {},
        borderchars = {
            "─",
            "│",
            "─",
            "│",
            "╭",
            "╮",
            "╯",
            "╰"
        },
        color_devicons = true,
        use_less = true,
        set_env = {["COLORTERM"] = "truecolor"}, -- default = nil,
        file_previewer = require("telescope.previewers").vim_buffer_cat.new,
        grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
        qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
        mappings = {
            i = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<C-q>"] = actions.close, -- works like a toggle, sometimes can be buggy
                ["<CR>"] = actions.select_default + actions.center,
                ["<C-f>"] = actions.select_default + actions.center
            },
            n = {
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<C-q>"] = actions.close, -- works like a toggle, sometimes can be buggy
                ["<C-f>"] = actions.select_default + actions.center
            }
        }
    }
}
require("indent_blankline").setup {
    buftype_exclude = {"terminal"},
    show_current_context = true
}

require("lualine").setup(
    {
        options = {theme = "gruvbox_dark", section_separators = {"", ""}, component_separators = {"", ""}},
        sections = {
            lualine_a = {"mode"},
            lualine_b = {"filename"},
            lualine_c = {"location"},
            lualine_x = {"branch", "diff"},
            lualine_y = {""},
            lualine_z = {"filetype"}
        }
    }
)

-- maps.lua
-----------------------------------------------------------
-- HELPERS
-----------------------------------------------------------
-- Map globally in all modes except insert
function mapg(input, exec)
    vim.api.nvim_set_keymap("", input, exec, {["noremap"] = true})
end
-- Map globally space leader
function map_leader(input, exec)
    vim.api.nvim_set_keymap("", "<Space>" .. input, exec, {["noremap"] = true})
end
-- Map in insert mode
function mapi(input, exec)
    vim.api.nvim_set_keymap("i", input, exec, {["noremap"] = true})
end

function mapm(input, exec, desc)
    require("which-key").register({[input] = {exec, desc}})
end
function mapl(input, exec, desc)
    require("which-key").register({["<Space>" .. input] = {exec, desc}})
end
function mapt(input, desc)
    require("which-key").register({["<Space>" .. input] = {name = desc}})
end
-----------------------------------------------------------
-- KEYS
-----------------------------------------------------------
-- Remapping
mapi("jk", "<Esc>")
mapi("kj", "<Esc>")
mapg("<C-g>", "<C-c>")
mapg("<C-j>", "<C-n>")
mapg("<C-k>", "<C-p>")
mapi("<C-q>", "<Esc>")
mapg("q", "")
mapg("Q", "q")
mapl("<Space>", ":", "CMD")
mapl("s", "/", "Search")
-- Window management
mapt("w", "+Window")
mapl("ws", ":split<CR>", "Vertical split")
mapl("wv", ":vsplit<CR>", "Horizontal split")
mapl("wl", "<C-w>l", "Go right")
mapl("wh", "<C-w>h", "Go left")
mapl("wj", "<C-w>j", "Go down")
mapl("wk", "<C-w>k", "Go up")
mapl("wc", "<C-w>q!", "Close")
-- Buffer management
mapt("b", "+Buffer")
mapl("bk", ":bdelete<CR>", "Delete")
mapl("bb", ":Telescope buffers<CR>", "List")
-- Plugins
map_leader("v", ":Term<CR>")
map_leader("<C-r>", ":ReloadConfig<CR>")
-- Toggle
mapt("t", "+Toggle")
mapl("tw", ":set wrap!<CR>", "Word wrap")
-- Editor
mapt("q", "+Editor")
mapl("qc", ":CommentToggle<CR>", "Comment")
mapl("qe", ':lua require"sniprun".run()<CR>', "Eval code")
mapl("qf", ":Neoformat<CR>", "Format buffer")
-- File
mapl("dd", ":Ranger<CR>", "File manager")
-- File
mapt("f", "+File")
mapl("ff", ":Telescope fd<CR>", "Find")
mapl("fs", ":w!<CR>", "Save")

mapg("<C-s>", ":Telescope live_grep<CR>")

require("nvim_comment").setup {}
require("nvim-autopairs").setup {}
require "sniprun".setup {}

require("fm-nvim").setup {
    config = {
        edit_cmd = "edit", -- opts: 'tabedit'; 'split'; 'pedit'; etc...
        border = "rounded", -- opts: 'rounded'; 'double'; 'single'; 'solid'; 'shawdow'
        height = 0.8,
        width = 0.9
    }
}

require("orgmode").setup_ts_grammar {}
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.org = {
    install_info = {
        url = "https://github.com/milisims/tree-sitter-org",
        revision = "f110024d539e676f25b72b7c80b0fd43c34264ef",
        files = {"src/parser.c", "src/scanner.cc"}
    },
    filetype = "org"
}

require "nvim-treesitter.configs".setup {
    highlight = {
        enable = true,
        disable = {"org"}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
        additional_vim_regex_highlighting = {"org"} -- Required since TS highlighter doesn't support all syntax features (conceal)
    },
    ensure_installed = {"org"} -- Or run :TSUpdate org
}

function _G.ReloadConfig()
    for name, _ in pairs(package.loaded) do
        package.loaded[name] = nil
    end
    dofile(vim.env.MYVIMRC)
    print("Neovim RELOADED!!!")
end
vim.cmd [[command! ReloadConfig lua ReloadConfig()]]

local function createWin(cmd, x, y, bound)
    local Buf = vim.api.nvim_create_buf(false, true)
    local win_height = math.ceil(vim.api.nvim_get_option("lines") * y - 4)
    local win_width = math.ceil(vim.api.nvim_get_option("columns") * x)
    local row = math.ceil((vim.api.nvim_get_option("lines") - win_height) / 2 - 1)
    local col = math.ceil((vim.api.nvim_get_option("columns") - win_width) / 2)
    local opts = {
        style = "minimal",
        relative = "editor",
        border = bound,
        width = win_width,
        height = win_height,
        row = row,
        col = col
    }
    local Win = vim.api.nvim_open_win(Buf, true, opts)
    vim.fn.termopen(cmd)
    vim.api.nvim_command("startinsert")
    vim.api.nvim_win_set_option(Win, "winhl", "Normal:Normal")
    vim.api.nvim_buf_set_keymap(
        Buf,
        "t",
        "<C-v>",
        "<C-\\><C-n>:lua vim.api.nvim_win_close(Win, true)<CR>",
        {silent = true}
    )
end

function _G.Term()
    createWin("zsh", 0.8, 0.8, "rounded")
end
vim.cmd [[command! Term lua Term()]]
