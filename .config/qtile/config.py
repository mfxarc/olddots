import os
import re
import socket
import subprocess
from libqtile import qtile
from typing import List
from libqtile import hook
from libqtile.config import Group, Match, Screen
from libqtile.layout.floating import Floating
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.widget.spacer import Spacer
from os.path import expanduser
# Widgets
from libqtile import bar, layout, widget
# Keys
from libqtile.config import EzKey as Keyz
from libqtile.config import EzClick as Click, EzDrag as Drag
# Wayland
from libqtile.backend import base
from libqtile.backend.wayland import InputConfig

wl_input_rules = {
    "*": InputConfig(xkb_options="ctrl:swapcaps,altwin:swap_alt_win,altwin:alt_win"),
}

mod = "mod1"
terminal = "footclient"
browser = "qutebrowser"
emacs = "emacsclient -qcn --alternate-editor '/usr/local/bin/emacs'"
nvim = terminal + " -T nvim -e nvim "
filemanager = emacs + " -e '(dired-jump)'"

def latest_group(qtile):
    qtile.current_screen.set_group(qtile.current_screen.previous_group)

keys = [
   # Window
   Keyz("A-C-c", lazy.window.kill()),
   Keyz("A-l", lazy.layout.grow()),
   Keyz("A-h", lazy.layout.shrink()),
   # Focus
   Keyz("A-j", lazy.group.next_window()),
   Keyz("A-k", lazy.group.prev_window()),
   # Keyz("A-m", lazy.window.toggle_minimize()),
   # Keyz("A-C-m", lazy.window.bring_to_front()),
   # Layouts
   Keyz("A-f", lazy.window.toggle_fullscreen()),
   Keyz("A-C-f", lazy.window.toggle_floating()),
   Keyz("A-<space>", lazy.next_layout()),
   Keyz("A-C-<space>", lazy.layout.flip()),
   # Groups
   Keyz("A-o", lazy.screen.next_group()),
   Keyz("A-i", lazy.screen.prev_group()),
   Keyz("A-v", lazy.function(latest_group)),
   # Spawn 
   Keyz("A-e", lazy.spawn(terminal)),
   Keyz("A-r", lazy.spawn(filemanager)),
   Keyz("A-0", lazy.spawn(emacs)),
   Keyz("A-9", lazy.spawn(nvim)),
   Keyz("A-<Return>", lazy.spawn(browser)),
   # Qtile
   Keyz("A-C-r", lazy.reload_config()),
   Keyz("A-C-q", lazy.shutdown()),
]

mouse = [
    Drag("A-1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag("A-3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click("A-2", lazy.window.bring_to_front())
]
    # Move windows
    # Key(A-shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    # Key(A-shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    # Key(A-shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    # Key(A-shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Resize windows
    # Key(A-C"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    # Key(A-C"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    # Key(A-C"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    # Key(A-C"], "k", lazy.layout.grow_up(), desc="Grow window up"),

groups = [
    Group('1', label="대", layout="monadtall", matches=[
          Match(wm_class='Emacs'), Match(wm_class='nvim'), Match(wm_class='Zathura')]),
    Group('2', label="내", layout="monadtall", matches=[
          Match(wm_class='LibreWolf'), Match(wm_class='qutebrowser'), Match(wm_class='Ferdi')]),
    Group('3', label="시", layout="monadtall", matches=[
          Match(wm_class='St'), Match(wm_class='Spacefm'), Match(wm_class='Timeshift-gtk')]),
    Group('4', label="더", layout="monadtall", matches=[
          Match(wm_class='wpsoffice'), Match(wm_class='libreoffice'), Match(wm_class='MuseScore3'), Match(wm_class='Anki'), Match(wm_class='wps')]),
    Group('5', label="미", layout="monadtall"),
    Group('6', label="지", layout="monadtall"),
    Group('7', label="인", layout="monadtall"),
]

for i in groups:
    keys.extend([
        Keyz("A-" + i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)),
        Keyz("A-S-" + i.name, lazy.window.togroup(i.name), desc="move focused window to group {}".format(i.name)),
    ])

theme = {
    "border_width": 2,
    "border_focus": "BBAC8D",
    "border_normal": "1F1F1F"
}

layouts = [
    layout.MonadTall(**theme),
    layout.Max(),
    layout.Floating(**theme),
]

floating_layout = layout.Floating(
    float_rules=[
        # Run qtile cmd-obj -o cmd -f windows to see the wm_class
        # *layout.Floating.default_float_rules,
        Match(title='Quit and close tabs?'),
        Match(wm_type='utility'),
        Match(wm_type='notification'),
        Match(wm_type='toolbar'),
        Match(wm_type='splash'),
        Match(wm_type='dialog'),
        Match(wm_class='gimp-2.99'),
        Match(wm_class='Firefox'),
        Match(wm_class='file_progress'),
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(title='About LibreOffice'),
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ],
)

widget_defaults = dict(
    font="Ubuntu",
    foreground='#C9C1B5',
    fontsize=12,
    padding=4,
    margin_y=-9,
)

extension_defaults = widget_defaults.copy()

screens = [
    Screen(top=bar.Bar(
        [
            widget.GroupBox(disable_drag=True,
                            # Current workspace
                            block_highlight_text_color='#FFFFFF',
                            highlight_color='#E05256',
                            borderwidth=0,
                            # 
                            margin_y=None,
                            fontsize=14,
                            active='#7C6F64', # Workspaces with windows
                            inactive='#3E3B3A', # Workspaces without windows
                            highlight_method='line',
                            background='#292929'),
            widget.Sep(),
            widget.Prompt(),
            widget.WindowName(),
            widget.Clock(format="%a %d %Y - %H:%M"),
            widget.ThermalSensor(),
            # Spacer(length=525),
            widget.Systray(),
        ],
        size=23,
        background='#292929',
    ), ),
]

@hook.subscribe.startup_once
def autostart():
    processes = [
        ['foot', '-s'],
        ['swaybg', '-i', expanduser('~/pix/gruvbox/europeana.jpg'), '-m', 'fill'],
    ]

    for p in processes:
        subprocess.Popen(p)

@hook.subscribe.client_new
def modify_window(client):
    for group in groups:  # follow on auto-move
        match = next((m for m in group.matches if m.compare(client)), None)
        if match:
            targetgroup = client.qtile.groups_map[group.name]  # there can be multiple instances of a group
            targetgroup.cmd_toscreen(toggle=False)
            break

follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wmname = "LG3D"
