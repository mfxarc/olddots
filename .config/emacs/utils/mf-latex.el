;;; m-latex.el --- My m-latex.el setup -*- lexical-binding: t; -*-

(setup (:github litex-mode "Atreyagaurav/litex-mode")
  (require 'litex-mode))

(setup (:elpa auctex)
  (add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))
  (:with-hook LaTeX-mode-hook
    (:hook 'TeX-fold-mode))
  (:with-hook after-save-hook
    (:hook 'latex-compile)))

;; https://orgmode.org/worg/org-tutorials/org-latex-export.html
(provide 'mf-latex)
;;; m-latex.el ends here
