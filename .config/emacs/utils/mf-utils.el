;;; m-utils.el --- My utilities -*- lexical-binding: t; -*-
(setup (:elpa transient)
  (require 'transient)
  (:option transient-show-common-commands nil))
(setup (:elpa popup))
(defadvice async-shell-command (around hide-async-windows activate)
  (save-window-excursion ad-do-it))

(defun get-word-regional ()
  "Get word at cursor or current region."
  (if (use-region-p)
      (buffer-substring-no-properties (region-beginning) (region-end))
    (current-word)))

(defun surround-word-or-region (char1 char2)
  (let* ((bds (bounds-of-thing-at-point 'symbol))
	 (beg (region-beginning))
	 (end (region-end))
	 (text (buffer-substring beg end)))
    (if (use-region-p)
	(when beg (delete-region beg end)
	      (progn (goto-char (region-beginning))
		     (insert char1)
		     (insert text)
		     (goto-char (region-end))
		     (insert char2)))
      (progn (goto-char (car bds))
	     (insert char1)
	     (goto-char (+ (cdr bds) 1))
	     (insert char2)))))
;; ---------------------------------------------------------------
(setq m-image-dir (expand-file-name "images" m-cache-dir))

(defun self-screenshot (&optional type)
  "Save a screenshot of type TYPE of the current Emacs frame.
    As shown by the function `', type can weild the value `svg',
    `png', `pdf'.

    This function will output in /tmp a file beginning with \"Emacs\"
    and ending with the extension of the requested TYPE."
  (interactive)
  (let* ((type (if type type
		 (intern (completing-read "Screenshot Type: " '(png pdf postscript)))))
	 (extension (pcase type
		      ('png        ".png")
		      ('pdf        ".pdf")
		      ('postscript ".ps")
		      (otherwise (error "Cannot export screenshot of type %s" otherwise))))
	 (filename (make-temp-file "Emacs-" nil extension))
	 (finalfile (expand-file-name (file-name-nondirectory filename) m-image-dir))
	 (data     (x-export-frames nil type)))
    (with-temp-file filename
      (insert data))
    (copy-file filename finalfile)
    (kill-new finalfile)
    (delete-file filename)
    (find-file finalfile)
    (message finalfile)))

(defun self-screenshot-png ()
  "Save a PNG screenshot of Emacs.
  See `self-screenshot'."
  (interactive)
  (self-screenshot 'png))
;; ---------------------------------------------------------------
(defun minibuffer-backward-kill (arg)
  "When minibuffer is completing a file name delete up to parent folder, otherwise delete normally"
  (interactive "p")
  (if minibuffer-completing-file-name
      (when (string-match-p "/." (minibuffer-contents))
	(zap-up-to-char (- arg) ?/))
    (if (eq major-mode 'vterm-mode)
	(vterm-send-C-h)
      (unless (or (get-text-property (point) 'read-only)
		  (eq (point) (point-min))
		  (get-text-property (1- (point)) 'read-only))
	(delete-char -1)))))
;; ---------- SWITCH TO PREVIOUS BUFFER
(defun exwm-switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))
;; ---------- CLOSE OTHER WINDOWS AND KEEP ONLY THE FOCUSED
(defun exwm-toggle-single-window ()
  (interactive)
  (if (= (count-windows) 1)
      (when single-window--last-configuration
	(set-window-configuration single-window--last-configuration))
    (setq single-window--last-configuration (current-window-configuration))
    (delete-other-windows)))
;; ---------- TOGGLE BETWEEN HORIZONTAL/VERTICAL SPLIT
(defun exwm-toggle-window-split ()
  "switch between vertical and horizontal split only works for 2 windows"
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(defun compile-run (command &optional mode name-function highlight-regexp)
  (or mode (setq mode 'compilation-mode))
  (let* ((name-of-mode
	  (if (eq mode t)
	      "compilation"
	    (replace-regexp-in-string "-mode\\'" "" (symbol-name mode))))
	 (thisdir default-directory)
	 (thisenv compilation-environment)
	 (buffer-path (and (local-variable-p 'exec-path) exec-path))
	 (buffer-env (and (local-variable-p 'process-environment)
			  process-environment))
	 outwin outbuf)
    (with-current-buffer
	(setq outbuf
	      (get-buffer-create
	       (compilation-buffer-name name-of-mode mode name-function)))
      (let ((comp-proc (get-buffer-process (current-buffer))))
	(if comp-proc
	    (if (or (not (eq (process-status comp-proc) 'run))
		    (eq (process-query-on-exit-flag comp-proc) nil)
		    (yes-or-no-p
		     (format "A %s process is running; kill it? "
  name-of-mode)))
		(condition-case ()
		    (progn
		      (interrupt-process comp-proc)
		      (sit-for 1)
		      (delete-process comp-proc))
		  (error nil))
	      (error "Cannot have two processes in `%s' at once"
		     (buffer-name)))))
      ;; first transfer directory from where M-x compile was called
      (setq default-directory thisdir)
      ;; Make compilation buffer read-only.  The filter can still write it.
      ;; Clear out the compilation buffer.
      (let ((inhibit-read-only t)
	    (default-directory thisdir))
	;; Then evaluate a cd command if any, but don't perform it yet, else
	;; start-command would do it again through the shell: (cd "..") AND
	;; sh -c "cd ..; make"
	(cd (cond
	     ((not (string-match "\\`\\s *cd\\(?:\\s +\\(\\S +?\\|'[^']*'\\|\"\\(?:[^\"`$\\]\\|\\\\.\\)*\"\\)\\)?\\s *[;&\n]"
				 command))
	      default-directory)
	     ((not (match-end 1)) "~")
	     ((eq (aref command (match-beginning 1)) ?\')
	      (substring command (1+ (match-beginning 1))
			 (1- (match-end 1))))
	     ((eq (aref command (match-beginning 1)) ?\")
	      (replace-regexp-in-string
	       "\\\\\\(.\\)" "\\1"
	       (substring command (1+ (match-beginning 1))
			  (1- (match-end 1)))))
	     ;; Try globbing as well (bug#15417).
	     (t (let* ((substituted-dir
			(substitute-env-vars (match-string 1 command)))
		       ;; FIXME: This also tries to expand `*' that were
		       ;; introduced by the envvar expansion!
		       (expanded-dir
			(file-expand-wildcards substituted-dir)))
		  (if (= (length expanded-dir) 1)
		      (car expanded-dir)
		    substituted-dir)))))
	(erase-buffer)
	;; Select the desired mode.
	(if (not (eq mode t))
	    (progn
	      (buffer-disable-undo)
	      (funcall mode))
	  (setq buffer-read-only nil)
	  (with-no-warnings (comint-mode))
	  (compilation-shell-minor-mode))
	;; Remember the original dir, so we can use it when we recompile.
	;; default-directory' can't be used reliably for that because it may be
	;; affected by the special handling of "cd ...;".
	;; NB: must be done after (funcall mode) as that resets local variables
	(setq-local compilation-directory thisdir)
	(setq-local compilation-environment thisenv)
	(if buffer-path
	    (setq-local exec-path buffer-path)
	  (kill-local-variable 'exec-path))
	(if buffer-env
	    (setq-local process-environment buffer-env)
	  (kill-local-variable 'process-environment))
	(if highlight-regexp
	    (setq-local compilation-highlight-regexp highlight-regexp))
	(if (or compilation-auto-jump-to-first-error
		(eq compilation-scroll-output 'first-error))
	    (setq-local compilation-auto-jump-to-next t))
	;; Output a mode setter, for saving and later reloading this buffer.
	(insert "-*- mode: " name-of-mode
		"; default-directory: "
		(prin1-to-string (abbreviate-file-name default-directory))
		" -*-\n"
		(format "%s started at %s\n\n"
			mode-name
			(substring (current-time-string) 0 19))
		command "\n")
	;; Mark the end of the header so that we don't interpret
	;; anything in it as an error.
	(put-text-property (1- (point)) (point) 'compilation-header-end t)
	(setq thisdir default-directory))
      (set-buffer-modified-p nil))
    ;; Pop up the compilation buffer.
    ;; https://lists.gnu.org/r/emacs-devel/2007-11/msg01638.html
    (setq outwin (display-buffer outbuf '(nil (allow-no-window . t))))
    (with-current-buffer outbuf
      (let ((process-environment
	     (append
	      compilation-environment
	      (and (derived-mode-p 'comint-mode)
		   (comint-term-environment))
	      (list (format "INSIDE_EMACS=%s,compile" emacs-version))
	      (copy-sequence process-environment))))
	(setq-local compilation-arguments
		    (list command mode name-function highlight-regexp))
	(setq-local revert-buffer-function 'compilation-revert-buffer)
	(and outwin
	     ;; Forcing the window-start overrides the usual redisplay
	     ;; feature of bringing point into view, so setting the
	     ;; window-start to top of the buffer risks losing the
	     ;; effect of moving point to EOB below, per
	     ;; compilation-scroll-output, if the command is long
	     ;; enough to push point outside of the window.  This
	     ;; could happen, e.g., in `rgrep'.
	     (not compilation-scroll-output)
	     (set-window-start outwin (point-min)))

	;; Position point as the user will see it.
	(let ((desired-visible-point
	       ;; Put it at the end if `compilation-scroll-output' is set.
	       (if compilation-scroll-output
		   (point-max)
		 ;; Normally put it at the top.
		 (point-min))))
	  (goto-char desired-visible-point)
	  (when (and outwin (not (eq outwin (selected-window))))
	    (set-window-point outwin desired-visible-point)))

	;; The setup function is called before compilation-set-window-height
	;; so it can set the compilation-window-height buffer locally.
	(if compilation-process-setup-function
	    (funcall compilation-process-setup-function))
	(and outwin (compilation-set-window-height outwin))
	;; Start the compilation.
	(if (fboundp 'make-process)
	    (let ((proc
		   (if (eq mode t)
		       ;; On remote hosts, the local `shell-file-name'
		       ;; might be useless.
		       (with-connection-local-variables
			;; comint uses `start-file-process'.
			(get-buffer-process
			 (with-no-warnings
			   (comint-exec
			    outbuf (downcase mode-name)
			    shell-file-name
			    nil `(,shell-command-switch ,command)))))
		     (start-file-process-shell-command (downcase mode-name)
						       outbuf command))))
	      ;; Make the buffer's mode line show process state.
	      (setq mode-line-process
		    '((:propertize ":%s" face compilation-mode-line-run)
		      compilation-mode-line-errors))

	      ;; Set the process as killable without query by default.
	      ;; This allows us to start a new compilation without
	      ;; getting prompted.
	      (when compilation-always-kill
		(set-process-query-on-exit-flag proc nil))

	      (set-process-sentinel proc #'compilation-sentinel)
	      (unless (eq mode t)
		;; Keep the comint filter, since it's needed for proper
		;; handling of the prompts.
		(set-process-filter proc #'compilation-filter))
	      ;; Use (point-max) here so that output comes in
	      ;; after the initial text,
	      ;; regardless of where the user sees point.
	      (set-marker (process-mark proc) (point-max) outbuf)
	      (when compilation-disable-input
		(condition-case nil
		    (process-send-eof proc)
		  ;; The process may have exited already.
		  (error nil)))
	      (run-hook-with-args 'compilation-start-hook proc)
	      (compilation--update-in-progress-mode-line)
	      (push proc compilation-in-progress))
	  ;; No asynchronous processes available.
	  (message "Executing `%s'..." command)
	  ;; Fake mode line display as if `start-process' were run.
	  (setq mode-line-process
		'((:propertize ":run" face compilation-mode-line-run)
		  compilation-mode-line-errors))
	  (force-mode-line-update)
	  (sit-for 0)			; Force redisplay
	  (save-excursion
	    ;; Insert the output at the end, after the initial text,
	    ;; regardless of where the user sees point.
	    (goto-char (point-max))
	    (let* ((inhibit-read-only t) ; call-process needs to modify outbuf
		   (compilation-filter-start (point))
		   (status (call-process shell-file-name nil outbuf nil "-c"
					 command)))
	      (run-hooks 'compilation-filter-hook)
	      (cond ((numberp status)
		     (compilation-handle-exit
		      'exit status
		      (if (zerop status)
			  "finished\n"
			(format "exited abnormally with code %d\n" status))))
		    ((stringp status)
		     (compilation-handle-exit 'signal status
					      (concat status "\n")))
		    (t
		     (compilation-handle-exit 'bizarre status status)))))
	  (set-buffer-modified-p nil)
	  (message "Executing `%s'...done" command)))
      (setq default-directory thisdir)
      (when compilation-scroll-output
	(goto-char (point-max))))
    (setq next-error-last-buffer outbuf)))

(defun compile-finish (buffer outstr)
  (unless (string-match "finished" outstr)
    (switch-to-buffer-other-window buffer)) t)
(setq compilation-finish-functions 'compile-finish)
(defadvice compilation-start
    (around inhibit-display
	    (command &optional mode name-function highlight-regexp))
  (if (not (string-match "^\\(find\\|grep\\)" command))
      (cl-letf ((display-buffer   #'ignore)
		(set-window-point #'ignoreco)
		(goto-char        #'ignore))
	(save-window-excursion
	  ad-do-it))
    ad-do-it))
(ad-activate 'compilation-start)

(defun fd-file ()
  "Find any file in the home directory."
  (interactive)
  (let ((default-directory "~/"))
    (recentf-push
     (find-file
      (completing-read
       "Find file: "
       (process-lines "fd"
		      "--threads=4"
		      ;; "--search-path=dl"
		      ;; "--search-path=dox"
		      ;; "--search-path=.config"
		      ;; "--search-path=.local/bin"
		      ;; "--search-path=.local/scripts"
		      "--search-path=/media/offline"
		      "--search-path=/media/personal"
		      "--color=never"
		      "--regex"))))))

(defun file-chmod ()
  (interactive)
  (let ((file (file-name-nondirectory buffer-file-name)))
    (shell-command (format "chmod +x %s" file))))

(defun pandoc-file-to (format)
  (let ((file (if (eq major-mode 'dired-mode)
		  (dired-get-filename)
		(file-name-nondirectory buffer-file-name))))
    (shell-command (format "pandoc \'%s\' -o \'%s.%s\'" file (file-name-base file) format) nil nil)))
(defun pandoc-to-org ()
  "Convert file to org document."
  (interactive) (pandoc-file-to "org"))
(defun pandoc-to-docx ()
  "Convert file to docx document."
  (interactive) (pandoc-file-to "docx")
  (message "File %s exported to docx" (buffer-file-name)))
(defun pandoc-to-pdf ()
  "Convert file to docx document."
  (interactive) (pandoc-file-to "pdf")
  (message "File %s exported to pdf" (buffer-file-name)))
(defun pandoc-to-odt ()
  "Convert file to odt document."
  (interactive) (pandoc-file-to "odt")
  (message "File %s exported to odt" (buffer-file-name)))

(defun dired-term ()
  "Open dired buffer in terminal"
  (interactive)
  (call-process-shell-command "st &"))

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (interactive)
  (indent-according-to-mode))

(defun TeX-insert-macro (symbol)
  (interactive (list (completing-read (concat "Macro: " TeX-esc)
				      (TeX-symbol-list-filtered) nil nil nil
				      'TeX-macro-history TeX-default-macro)))
  (when (called-interactively-p 'any)
    (setq TeX-default-macro symbol))
  (TeX-parse-macro symbol (cdr-safe (assoc symbol (TeX-symbol-list))))
  (evil-insert-state)
  (run-hooks 'TeX-after-insert-macro-hook))
;; ------------------------------------------------------------------------------------------------
(defcustom large-file-threshold (* 10 1024) "File size threshold for `large-file-behaviours'.")
(defun large-file-behaviours ()
  "React to files which are larger than a given size.
Used in `find-file-hook'."
  (when (> (buffer-size) large-file-threshold)
    (message "%s" "Large file detected.")
    (when (derived-mode-p 'js2-mode)
      (prog-mode))))
(add-hook 'find-file-hook 'large-file-behaviours)
;; ------------------------------------------------------------------------------------------------
(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max))
  (whitespace-cleanup)
  (save-buffer))
(defun org-cleanup ()
  (interactive)
  (org-edit-special)
  (indent-buffer)
  (org-edit-src-exit))
(defun format-buffer ()
  (interactive)
  (progn (cond ((org-in-src-block-p) (org-cleanup))
	       ((eq major-mode 'org-mode) (indent-buffer))
	       (t (progn (format-all-buffer t))))
	 (whitespace-cleanup)
	 (save-buffer)))
;; ------------------------------------------------------------------------------------------------
;; (let ((text (with-current-buffer "m-utils.el" (buffer-string))))
;;   (pos-tip-show-no-propertize text nil nil nil 5 600 600))
;; ------------------------------------------------------------------------------------------------
(defun insert-hour()
  (interactive)
  (forward-char)
  (insert (format-time-string "%H:%M"))
  (evil-insert 1))
(defun insert-date()
  (interactive)
  (forward-char)
  (insert (format-time-string "%d/%m/%Y"))
  (evil-insert 1))

(defun my/zoom-in ()
  "Increase font size by 20 points"
  (interactive)
  (set-face-attribute 'variable-pitch nil :height (+ (face-attribute 'default :height) 20)))

(defun my/zoom-out ()
  "Decrease font size by 20 points"
  (interactive)
  (set-face-attribute 'variable-pitch nil :height (- (face-attribute 'default :height) 20)))

(defun default-zoom ()
  "Default font size."
  (interactive)
  (set-face-attribute 'default nil :height (face-attribute 'variable-pitch :height)))

;; change font size, interactively
(global-set-key (kbd "C-0") 'default-zoom)
(global-set-key (kbd "C-=") 'my/zoom-in)
(global-set-key (kbd "C--") 'my/zoom-out)

;;
(defun chunyang-elisp-function-or-variable-quickhelp (symbol)
  "Display summary of function or variable at point.

Adapted from `describe-function-or-variable'."
  (interactive
   (let* ((v-or-f (variable-at-point))
	  (found (symbolp v-or-f))
	  (v-or-f (if found v-or-f (function-called-at-point))))
     (list v-or-f)))
  (if (not (and symbol (symbolp symbol)))
      (message "You didn't specify a function or variable")
    (let* ((fdoc (when (fboundp symbol)
		   (or (documentation symbol t) "Not documented.")))
	   (fdoc-short (and (stringp fdoc)
			    (substring fdoc 0 (string-match "\n" fdoc))))
	   (vdoc (when  (boundp symbol)
		   (or (documentation-property symbol 'variable-documentation t)
		       "Not documented as a variable.")))
	   (vdoc-short (and (stringp vdoc)
			    (substring vdoc 0 (string-match "\n" vdoc)))))
      (and (require 'popup nil 'no-error)
	   (popup-tip
	    (or
	     (and fdoc-short vdoc-short
		  (concat fdoc-short "\n\n"
			  (make-string 30 ?-) "\n" (symbol-name symbol)
			  " is also a " "variable." "\n\n"
			  vdoc-short))
	     fdoc-short
	     vdoc-short)
	    :margin t)))))

(defun set-background-for-terminal (&optional frame)
  (or frame (setq frame (selected-frame)))
  "unsets the background color in terminal mode"
  (unless (display-graphic-p frame)
    (set-face-background 'default "unspecified-bg" frame)))
(add-hook 'after-make-frame-functions 'set-background-for-terminal)
(add-hook 'window-setup-hook 'set-background-for-terminal)

(defun make-window-half ()
  (interactive)
  (other-window 1)
  (enlarge-window (/ (window-height (next-window)) 2))
  (other-window 1))

(defun make-horizontal-half ()
  (interactive)
  (other-window 1)
  (enlarge-window-horizontally (/ (window-height (next-window)) 2))
  (other-window 1))

(defun kill-window ()
  (interactive)
  (if (and (>= (count-windows) 2)
	   (not (neo-global--window-exists-p)))
      (progn (kill-buffer (current-buffer))
	     (delete-window))
    (kill-buffer (current-buffer))))

(defun end-line ()
  "Go to end of line."
  (interactive)
  (if (use-region-p)
      (progn (end-of-line) (backward-char) (forward-char))
    (end-of-line)))

(provide 'mf-utils)
;;; m-utils.el ends here
