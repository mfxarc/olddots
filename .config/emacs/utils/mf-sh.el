;;; m-sh.el --- My shell and terminal related tools setup -*- lexical-binding: t; -*-

(setup (:elpa vterm-toggle))
(setup (:elpa vterm)
  (:also-elpa eterm-256color)
  (setq vterm-term-environment-variable "eterm-color")
  (setq vterm-kill-buffer-on-exit t)
  (setq vterm-max-scrollback 5000)
  (setq vterm-timer-delay 0)
  (setq confirm-kill-process 'nil))
(defun make-bottom-window ()
  (progn (exwm-toggle-window-split)
	 (make-window-half)))
(add-hook 'vterm-toggle-show-hook 'make-bottom-window)

(setup (:elpa restart-emacs))

(setup (:elpa '(archwiki :build t :type git :repo "https://labs.phundrak.com/phundrak/archwiki.el")))

(setup (:github async "jwiegley/emacs-async"))

(provide 'mf-sh)
