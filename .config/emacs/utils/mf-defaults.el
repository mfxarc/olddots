;;; m-defaults.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 17, 2022
;;
;;; Commentary:
;;  
;;  My configuration for m-defaults.el
;;  
;;; Code:

;; Appearance
(tooltip-mode -1)
(global-linum-mode -1)
(electric-indent-mode -1)
(electric-pair-mode -1)
(blink-cursor-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(window-divider-mode)
;; Better defaults
(setq calendar-date-style 'european)
(setq message-log-max 50)
(setq x-gtk-use-system-tooltips nil)
(setq backup-inhibited t)
(setq x-select-enable-clipboard t)
;; Supress warnings
(setq native-comp-async-report-warnings-errors nil)
(setq native-comp-warning-on-missing-source nil)
(setq byte-compile-warnings '(not t))
(setq warning-minimum-level :emergency)
;; UI Less verbose
(setq make-pointer-invisible t)
(setq auth-source-debug nil)
(setq ring-bell-function 'ignore)
(setq auto-save-no-message t)
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq confirm-kill-processes nil)
(setq cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)
(setq global-visual-line-mode nil)
(setq find-file-suppress-same-file-warnings t)
(setq split-width-threshold 0)
(setq split-height-threshold nil)
;; File-system
(setq confirm-nonexistent-file-or-buffer nil)
(setq find-file-visit-truename t)
(setq sentence-end-double-space nil)
(setq require-final-newline t)
;; Recent Files
(setq recentf-auto-cleanup nil)
(setq recentf-max-menu-items 250)
(setq recentf-max-saved-items 500)
(add-hook 'find-file-hook 'recentf-save-list)
;; Kill/Yank
(setq kill-ring-max 250)
(setq kill-do-not-save-duplicates t)
(setq mouse-yank-at-point t)
(setq save-interprogram-paste-before-kill t)
;; Indentation
(setq tab-width 4)
(setq indent-tabs-mode nil)
(setq backward-delete-char-untabify-method 'hungry)
;; Auto-revert
(setq auto-revert-verbose nil)
(setq auto-revert-use-notify nil)
(setq auto-revert-stop-on-user-input nil)
(setq revert-without-query (list "."))
(global-auto-revert-mode)
;; Enable
(recentf-mode)
;; Performance
;;; Scrolling
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling t)
(setq scroll-conservatively 101)
(setq scroll-margin 0)
(setq scroll-preserve-screen-position t)
(setq pixel-scroll-precision-large-scroll-height 40.0)
(setq pixel-scroll-precision-interpolation-factor 30)
(pixel-scroll-precision-mode)
;;; Scrolling
(setq process-adaptive-read-buffering nil)
(setq redisplay-skip-fontification-on-input t)
(setq idle-update-delay 1.0)
(setq jit-lock-defer-time 0)
(setq window-resize-pixelwise t)
(setq frame-resize-pixelwise t)
(setq widget-image-enable nil)
(setq bidi-display-reordering 'left-to-right)
(setq bidi-paragraph-direction 'left-to-right)
(setq bidi-inhibit-bpa t)
(setq inhibit-compacting-font-caches t)
(setq window-combination-resize t)
(setq frame-inhibit-implied-resize t)
(setq ffap-machine-p-known 'reject)
(setq auto-mode-case-fold nil)
(setq delete-selection-mode t)

(provide 'mf-defaults)
;;; m-defaults.el ends here
