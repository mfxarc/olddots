;;; m-org.el --- My m-org.el setup -*- lexical-binding: t; -*-

(setup org
  (:with-hook org-mode-hook
    (:hook 'prettify-symbols-mode))
  ;; Defaults
  (setq org-confirm-babel-evaluate nil)
  (setq org-link-elisp-confirm-function nil)
  (setq org-M-RET-may-split-line nil)
  (setq org-insert-heading-respect-content t)
  (setq org-return-follows-link t)
  (setq org-mouse-1-follows-link nil)
  ;; Source blocks
  (setq org-src-preserve-indentation nil)
  (setq org-src-tab-acts-natively nil)
  (setq org-src-window-setup 'current-window)
  ;; Appearance
  (setq org-auto-align-tags t)
  (setq org-blank-before-new-entry nil)
  (setq org-fontify-quote-and-verse-blocks nil)
  (setq org-fontify-whole-heading-line nil)
  (setq org-fontify-whole-block-delimiter-line nil)
  (setq org-edit-src-content-indentation 0)
  (setq org-odt-fontify-srcblocks nil)
  (setq org-hide-leading-stars nil)
  (setq org-cycle-separator-lines 0)
  (setq org-descriptive-links t)
  (setq org-adapt-indentation t)
  (setq org-cycle-separator-lines 0)
  (setq org-imenu-depth 3)
  (setq org-log-done 'time)
  (setq org-startup-truncated t)
  (setq org-startup-folded 'content)
  (setq org-support-shift-select 'always)
  (setq org-hide-emphasis-markers t)
  (setq org-todo-window-setup 'current-window)
  (setq org-log-into-drawer t)
  (setq org-todo-keywords '((sequence "TODO(t)" "|" "DONE(d)" "CANCELLED(c)" )))
  ;;; Images
  (setq image-use-external-converter t)
  (setq image-converter 'imagemagick)
  (setq preview-image-type 'svg)
  (setq org-startup-with-inline-images t)
  (setq org-image-actual-width '(400))
  ;; Agenda
  (setq org-agenda-sorting-strategy '(scheduled-up))
  (setq org-agenda-include-deadlines t)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-unavailable-files t)
  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-start-on-weekday nil)
  (setq org-agenda-span 'day)
  (setq org-agenda-inhibit-startup t)
  (require 'org))

(setup (:elpa ox-pandoc)
  (:load-after org)
  (:when-loaded
    (add-to-list 'org-export-backends 'pandoc)
    (setq org-pandoc-options
	  '((standalone . t)
	    (mathjax . t)
	    (variable . "revealjs-url=https://revealjs.com"))))
  (require 'ox-pandoc))
(require 'ob-awk)
(require 'ob-shell)
(require 'ob-emacs-lisp)
(require 'ob-lua)
(require 'ob-gnuplot)
(require 'ob-dot)
(require 'ob-calc)
(require 'ob-lilypond)
(require 'org-tempo)
(require 'ox-latex)
(setup (:elpa ob-mermaid))
(setup (:elpa org-reveal)
  (require 'ox-reveal)
  (:option org-reveal-root (expand-file-name "revealjs" m-cache-dir)
	   org-reveal-plugins '(highlight)
	   org-reveal-theme "serif"
	   org-reveal-single-file t
	   org-reveal-transition "none"
	   org-reveal-revealjs-version "4")
  (defun org-reveal-export-to-html-and-browse
      (&optional async subtreep visible-only body-only ext-plist)
    "Export current buffer to a reveal.js and browse HTML file."
    (interactive)
    (browse-url (concat "file://" (expand-file-name (org-reveal-export-to-html async subtreep visible-only body-only ext-plist)) "?print-pdf"))))
(setup (:elpa org-appear)
  (:hook-into org-mode-hook))
(setup (:elpa literate-calc-mode))
(setup (:elpa el2org))
(setup (:elpa org-download)
  (setq org-download-backend 't)
  (setq org-download-timestamp "%H%M%S")
  (setq org-download-screenshot-method "shotgun -g \"$(slop)\" %s")
  (setq-default org-download-image-dir "./images")
  (setq org-download-screenshot-basename "screenshot")
  (org-download-enable))
(setup (:elpa org-inline-pdf))
(setup (:elpa org-fragtog)
  (:hook-into org-mode-hook))
(defface org-horizontal-rule
  '((default :inherit org-hide) (((background light)) :strike-through "gray70") (t :strike-through "gray30"))
  "Face used for horizontal ruler.")
(setup (:elpa valign)
  (:hook-into org-mode-hook)
  (:option valign-fancy-bar t))

;; Taken and adapted from org-colored-text
(org-add-link-type
 "color"
 (lambda (path) "No follow action.")
 (lambda (color description backend)
   (cond ((eq backend 'latex)                  ; added by TL
	  (format "{\\color{%s}%s}" color description)) ; added by TL
	 ((eq backend 'html)
	  (let ((rgb (assoc color color-name-rgb-alist)) r g b)
	    (if rgb
		(progn
		  (setq r (* 255 (/ (nth 1 rgb) 65535.0))
			g (* 255 (/ (nth 2 rgb) 65535.0))
			b (* 255 (/ (nth 3 rgb) 65535.0)))
		  (format "<span style=\"color: rgb(%s,%s,%s)\">%s</span>"
			  (truncate r) (truncate g) (truncate b)
			  (or description color)))
              (format "No Color RGB for %s" color)))))))

(defun mark-current-word (&optional arg allow-extend)
  "Put point at beginning of current word, set mark at end."
  (interactive "p\np")
  (setq arg (if arg arg 1))
  (if (and allow-extend
           (or (and (eq last-command this-command) (mark t))
               (region-active-p)))
      (set-mark
       (save-excursion
         (when (< (mark) (point))
           (setq arg (- arg)))
         (goto-char (mark))
         (forward-word arg)
         (point)))
    (let ((wbounds (bounds-of-thing-at-point 'word)))
      (unless (consp wbounds)
        (error "No word at point"))
      (if (>= arg 0)
          (goto-char (car wbounds))
        (goto-char (cdr wbounds)))
      (push-mark (save-excursion
                   (forward-word arg)
                   (point)))
      (activate-mark))))

(defun get-word-regional ()
  "Get word at cursor or current region."
  (let* ((bds (bounds-of-thing-at-point 'symbol))
	 (beg (car bds))
	 (end (cdr bds)))
    (if (use-region-p)
	(yank (kill-region (region-beginning) (region-end)))
      (progn (mark-current-word)
	     (yank (kill-region (region-beginning) (region-end)))))))

(defun org-colorize ()
  "Add color to text at point."
  (interactive)
  (org-insert-link nil (concat "color:" (read-string "Color name: ")) (get-word-regional)))

(font-lock-add-keywords
 'org-mode
 '(("^ *\\([-]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))
   ("^ *\\([+]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "◦"))))
   ("^-\\{5,\\}$" 0 '(face org-horizontal-rule display (space :width text)))))

(setq prettify-symbols-unprettify-at-point 'right-edge)
(setup (:elpa mixed-pitch)
  (require 'mixed-pitch)
  (:hook-into eww-mode-hook)
  (setq mixed-pitch-fixed-pitch-faces (delete 'org-table mixed-pitch-fixed-pitch-faces)))
;; https://www.reddit.com/r/emacs/comments/9wukv8/hide_all_stars_in_org_mode/
(defun org-hide-stars ()
  "Make org heading star invisible."
  (font-lock-add-keywords
   nil '(("^\\*+ " (0 (prog1 nil (put-text-property (match-beginning 0)
						    (match-end 0) 'face
						    (list :foreground (face-attribute 'default :background)))))))))

(setq-default fill-column 64)
(setq-default line-spacing 0.07)

(defun read-mode ()
  (olivetti-mode)
  (org-hide-stars)
  (org-inline-pdf-mode)
  (org-indent-mode))

(add-hook 'org-mode-hook 'read-mode)

(defun org-bold ()
  "Make as bold the current region or word."
  (interactive)
  (surround-word-or-region "*" "*"))

(defun org-italic ()
  "Make italic the current region or word."
  (interactive)
  (surround-word-or-region "\/" "\/"))

(defun org-verbatim ()
  "Make italic the current region or word."
  (interactive)
  (surround-word-or-region "=" "="))

(defun org-html-export-on-save ()
  (interactive)
  (if (memq 'org-html-export-to-html after-save-hook)
      (progn
        (remove-hook 'after-save-hook 'org-html-export-to-html t)
        (message "Disabled org html export on save for current buffer..."))
    (add-hook 'after-save-hook 'org-html-export-to-html nil t)
    (message "Enabled org html export on save for current buffer...")))

;;
(defun org-babel-tangle-block()
  "Tangle only the current block."
  (interactive)
  (let ((current-prefix-arg '(4)))
    (call-interactively 'org-babel-tangle)))

(define-transient-command org-menu ()
  "Menu to manage org-mode."
  [
   ["Insert"
    ("l" "Link" org-insert-link)
    ("p" "Image" org-download-yank)
    ("d" "Table" org-table-create)
    ]
   ["Markup"
    ("C-b" "Bold" org-bold)
    ("C-v" "Verbatim" org-verbatim)
    ("C-i" "Italic" org-italic)
    ]
   [""
    ("C-c" "Colorized" org-colorize)
    ("f" "Format" format-buffer)
    ]
   ["Export"
    ("e" "Various formats" org-export-dispatch)
    ("b" "Babel block" org-babel-tangle-block)
    ("v" "Babel whole file" org-babel-tangle)
    ("c" "Babel clean results" org-babel-remove-result-one-or-many)
    ]
   ["Toggle"
    ("t" "Todo" org-todo)
    ("h" "Heading" org-toggle-heading)
    ("x" "Checkbox state"org-toggle-checkbox)
    ]
   [""
    ("i" "Bullet item"org-toggle-item)
    ("I" "Inline image" org-toggle-inline-images)
    ("s" "Sort" org-sort)
    ]
   ]
  )
(provide 'mf-org)
;;; m-org.el ends here
