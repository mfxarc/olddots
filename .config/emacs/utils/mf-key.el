;;; m-key.el --- My keybinding setup -*- lexical-binding: t; -*-
(setup (:elpa transient)
  (require 'transient))
(setup (:elpa undo-fu))
(setup (:elpa undo-fu-session)
  (global-undo-fu-session-mode))
(setup (:elpa evil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-w-in-emacs-state nil)
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-echo-state nil)
  (setq evil-window-map nil)
  (setq evil-respect-visual-line-mode t)
  (setq evil-default-state 'normal)
  (setq evil-undo-system 'undo-fu)
  (evil-mode)
  (dolist (mode '(custom-mode eshell-mode git-rebase-mode erc-mode circe-server-mode
			      circe-chat-mode circe-query-mode sauron-mode vterm-mode term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))
(setup (:elpa evil-collection)
  (evil-collection-init '(magit help helpful doc-view proced vterm mpdel ibuffer compile calc calendar vterm woman)))
;;; --------------------------------------------------
(define-transient-command helpful-management
  "Helpful transient management."
  [
   ["Helpful"
    ("s" "Describe symbol at point" xref-find-definitions-other-window)
    ("f" "Describe function" helpful-function)
    ("v" "Describe variable" helpful-variable)
    ]
   [
    ""
    ("k" "Describe key" helpful-key)
    ("c" "Describe face" describe-face)
    ]
   ["Straight"
    ("u p" "Install package" straight-use-package)
    ("u d" "Pull package" straight-pull-package-and-deps)
    ("u r" "Remove unused packages" straight-remove-unused-repos)
    ]
   ]
  )

(define-transient-command file-management ()
  "Manage your files, archives and folders."
  [
   ["Find"
    ("g" "File in current dir" find-file)
    ("f" "File in work-dirs" fd-file)
    ("r" "Recent files" consult-buffer)
    ("j" "Current directory" dired-jump)
    ]
   ["Save"
    ("s" "Save" save-buffer)
    ("a" "Save as..." write-file)
    ]
   ["Create"
    ("c f" "File" make-empty-face)
    ("c d" "Folder" make-directory)
    ]
   ["Others"
    ("d f" "Delete file" delete-file)
    ("d d" "Delete directory" delete-directory)
    ("b" "Rename file" rename-file)
    ("v" "Copy file" copy-file)
    ]
   ["Sudo"
    ("u" "Find file"  sudo-find-file)
    ("e" "Edit current buffer" sudo-edit-file)
    ]
   ]
  )

(define-transient-command toggle-management
  "Toggles transient management."
  [
   ["Toggle"
    ("w"  "Word wrap" toggle-word-wrap)
    ("t"  "Truncate lines" toggle-truncate-lines)
    ("l"  "Line numbers" display-line-numbers-mode)
    ("C-f" "Flyspell" flyspell-mode)
    ]
   ]
  )

(define-transient-command buffer-management
  "Buffer transient management."
  ["Buffer"
   ("b" "Scratch with current major mode" scratch)
   ("n" "New blank w/o mode" evil-buffer-new)
   ("r" "Revert chanes" revert-buffer)
   ("k" "Kill current buffer" kill-current-buffer)
   ("l" "List of buffers" ibuffer)
   ]
  )

(define-transient-command editor-management
  "Editor transient management."
  [
   ["Case"
    ("u"  "Upper" string-inflection-upcase)
    ("l"  "Lower" string-inflection-lower-camelcase)
    ("t"  "Capitalize" string-inflection-toggle)
    ]
   ["Symbols"
    ("c"   "Comment line" evilnc-comment-or-uncomment-lines)
    ("p"   "Parentheses" insert-parentheses)
    ("b"   "Brackets" insert-brackets)
    ]
   [""
    ("w"   "Writer quotes" insert-writer-quotes)
    ("q"   "Code Quotes" insert-double-quotes)
    ("v"   "Code S-Quotes" insert-quotes)
    ]
   ["Lorem Ipsum"
    ("i p" "Insert lorem paragraph" lorem-ipsum-insert-paragraphs)
    ("i s" "Insert lorem sentence" lorem-ipsum-insert-sentences)
    ("i l" "Insert lorem list" lorem-ipsum-insert-list)
    ]
   ]
  )

(define-transient-command roam-management ()
  "Roam transient management."
  [
   [
    ("f" "Find or create note" org-roam-node-find)
    ("c" "Capture a template" org-capture)
    ("s" "Ripgrep notes" org-roam-rg)
    ]
   ])

(define-transient-command search-management ()
  "Search transient management."
  [
   ["Errors"
    ("f" "Code with flymake" consult-flymake)
    ("s" "Spelling with flyspell" consult-flyspell)
    ]
   ])

(define-transient-command linguist-management
  "Checker transient management."
  [
   ["Check"
    ("w" "Spell error at point" flyspell-correct-at-point)
    ("p" "Pronunciation at point" speak-at-point)
    ]
   ["Languages"
    ("t" "Translate word/region" translate-at-point)
    ("d" "Define word" define-word-at-point)
    ]
   ])

(defun global-key (&rest args)
  "Bind KEY to COMMAND globally."
  (dolist (arglist args)
    (dolist (state '(normal visual insert motion replace emacs))
      (let* ((key (nth 0 arglist))
	     (command (nth 1 arglist)))
	(evil-global-set-key state (kbd key) command)))))

(defun state-key (state &rest args)
  "Bind KEY to COMMAND globally in specific STATE."
  (dolist (arglist args)
    (let* ((key (nth 0 arglist))
	   (command (nth 1 arglist)))
      (evil-global-set-key state (kbd key) command))))

(defun leader-key (&rest args)
  "Bind KEY to COMMAND globally in normal mode after leader."
  (dolist (arglist args)
    (dolist (state '(normal visual replace))
      (let* ((key (nth 0 arglist))
	     (command (nth 1 arglist)))
	(evil-global-set-key state (kbd (concat "SPC " key)) command)))))

(defun undefine-evil (&rest args)
  "Unbind evil keys."
  (with-eval-after-load 'evil-maps
    (dolist (state '(normal visual replace))
      (dolist (arglist args)
	(let* ((key (nth 0 arglist)))
	  (evil-global-set-key state (kbd key) nil))))))

(undefine-evil '("g") '("C-w") '("SPC"))

(global-key '("s-u" exwm-switch-to-previous-buffer)
	    '("s-ç" exwm-toggle-single-window)
	    '("s-]" toggle-theme)
	    '("s-C-ç" exwm-toggle-window-split)
	    '("C-<dead-tilde>" eval-expression)
	    '("s-c" kill-window))

(state-key 'insert
	   '("C-f" corfu-insert)
	   '("C-j" corfu-next)
	   '("C-k" corfu-previous)
	   '("C-q" evil-normal-state)
	   '("C-h" backward-delete-char))

(state-key 'normal
	   '(":" eval-expression)
	   '("C-p" consult-yank-pop)
	   '("C-o" embark-act)
	   '("C-w" centaur-tabs-switch-group)
	   '("M-e" centaur-tabs-forward-group)
	   '("M-q" centaur-tabs-backward-group)
	   '("C-e" centaur-tabs-forward)
	   '("C-q" centaur-tabs-backward)
	   '("M-j" move-line-down)
	   '("M-k" move-line-up)
	   '("C-f" consult-outline)
	   '("C-s" consult-line)
	   '("C-a" crux-move-beginning-of-line)
	   '("C-l" end-line))

(global-key '("C-." olivetti-expand)
	    '("C-," olivetti-shrink))

(state-key 'motion
	   '("g" nil)
	   '("gb" goto-last-change)
	   '("gg" beginning-of-buffer)
	   '("gc" avy-goto-char)
	   '("gw" avy-goto-word-0)
	   '("gl" avy-goto-line)
	   '("gh" end-of-buffer))

;; <SPC> Global leader keys
(leader-key '("SPC" execute-extended-command)
	    '("f" file-management)
	    '("b" buffer-management)
	    '("h" helpful-management)
	    '("o" roam-management)
	    '("t" toggle-management)
	    '("g" hugo-management)
	    '("q" editor-management)
	    '("c" linguist-management)
	    '("u" uni-note-menu)
	    '("s" search-management)
	    '("a" consult-bookmark)
	    '("ns" magit-status-bare)
	    '("nf" magit-status-here)
	    ;; 
	    '("TAB" cycle-ispell-languages)
	    '("v" vterm-toggle)
	    '("C-s" consult-ripgrep)
	    ;; 
	    '("ww" neotree-toggle)
	    '("wc" evil-window-delete)
	    '("wl" evil-window-right)
	    '("wh" evil-window-left)
	    '("wk" evil-window-up)
	    '("wj" evil-window-down)
	    '("wv" evil-window-vsplit)
	    '("ws" evil-window-split)
	    )

;; <SPC> Global leader key
;; <n> Leader keys to local major modes

;; Available keys -
;; t C-t T C-T
;; m C-m M C-M
;; n C-n N C-N
;; , C-, < C-<
;; ç C-ç Ç C-Ç
;; s C-s S C-S
(provide 'mf-key)
;;; m-key.el ends here
