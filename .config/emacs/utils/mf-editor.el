;;; m-editor.el --- My m-editor.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;;
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: janeiro 12, 2022
;; Modified: janeiro 12, 2022
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(setup (:elpa aggressive-indent) (:hook-into emacs-lisp-mode))
(setup (:elpa macrostep))
(setup (:elpa string-inflection))
(setup (:elpa format-all))
(setup (:elpa evil-nerd-commenter))
(setup (:elpa lorem-ipsum))
(setup (:elpa embark)
  (setq embark-prompter 'embark-completing-read-prompter)
  (setq embark-indicators nil))
(setup (:elpa crux))
(setup (:elpa yasnippet)
  (:also-elpa yasnippet-snippets)
  (yas-global-mode)
  (add-to-list 'yas-snippet-dirs (concat m-cache-dir "snippets/"))
  (defun mf/snippet (NAME MODE) (yas-expand-snippet (yas-lookup-snippet NAME MODE)))
  (defun mf/yas-elisp-bp () (interactive) (mf/snippet "elisp" 'emacs-lisp-mode))
  (defun mf/yas-latex-bp () (interactive) (mf/snippet "latex" 'latex-mode))
  (defun mf/yas-shell-bp () (interactive) (mf/snippet "shell" 'sh-mode))
  (defun mf/yas-lua-bp () (interactive) (mf/snippet "lua-init" 'lua-mode))
  (defun mf/yas-groff-bp () (interactive) (mf/snippet "groff" 'nroff-mode)))
(setup (:elpa autoinsert)
  (auto-insert-mode)
  (setq auto-insert-query nil)
  (setq auto-insert-alist nil)
  (add-hook 'find-file-hook 'auto-insert)
  (dolist (snippet (list '("\\.el$" . [mf/yas-elisp-bp])
			 '("\\.sh$" . [mf/yas-shell-bp])
			 '("\\.lua$" . [mf/yas-lua-bp])
			 '("\\.tex$" . [mf/yas-latex-bp])
			 '("\\.ms$" . [mf/yas-groff-bp])))
    (add-to-list 'auto-insert-alist snippet)))
(setup (:elpa scratch))

(defun insert-parentheses ()
  (interactive)
  (surround-word-or-region "\(" "\)"))
(defun insert-brackets ()
  (interactive)
  (surround-word-or-region "\{" "\}"))
(defun insert-quotes ()
  (interactive)
  (surround-word-or-region "\'" "\'"))
(defun insert-double-quotes ()
  (interactive)
  (surround-word-or-region "\"" "\""))
(defun insert-writer-quotes ()
  (interactive)
  (surround-word-or-region "\“" "\”"))

(provide 'mf-editor)
;;; m-editor.el ends here
