;;; m-documents.el --- My m-documents.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 26, 2022
;;
;;; Commentary:
;;  
;;  My configuration for m-documents.el
;;  
;;; Code:

(defun groff-compile ()
  "Compile groff document and open in Zathura."
  (interactive)
  (let* ((buffer (buffer-name))
	 (ext (file-name-extension buffer))
	 (pdf (concat (file-name-base buffer) ".pdf")))
    (when (and (eq major-mode 'nroff-mode)
	       (string-match "ms" ext))
      (progn (call-process-shell-command (format "groff -k -ms -tbl -T pdf %s > %s" buffer pdf))
	     (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	       (call-process-shell-command (format "zathura %s &" pdf)))
	     (message "Document %s compiled" buffer)))))

(add-hook 'after-save-hook 'groff-compile)

(defun latex-compile ()
"Compile latex document and open in Zathura."
(interactive)
(let* ((buffer (buffer-name))
       (base (file-name-base buffer))
       (ext (file-name-extension buffer))
       (pdf (concat (file-name-base buffer) ".pdf")))
  (when (and (eq major-mode 'latex-mode)
	     (string-match "tex" ext))
    (progn (call-process-shell-command (format "xelatex -interaction=batchmode %s 1>/dev/null &" buffer))
	   (call-process-shell-command (format "biber %s 1>/dev/null &" base))
	   (call-process-shell-command (format "xelatex -interaction=batchmode %s 1>/dev/null &" buffer))
	   (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	     (call-process-shell-command (format "zathura %s &" pdf)))
	   (message "Document %s compiled" base)))))

(defun lilypond-compile ()
"Compile lilypond document and open in Zathura."
(interactive)
(let* ((file (buffer-name))
       (ext (file-name-extension file))
       (pdf (concat (file-name-base file) ".pdf")))
  (when (string-match "ly" ext)
    (let ((command (format "lilypond -s %s" file)))
      (compilation-start command)
      (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	(call-process-shell-command (format "zathura %s &" pdf)))))))

(provide 'mf-documents)
;;; m-documents.el ends here
