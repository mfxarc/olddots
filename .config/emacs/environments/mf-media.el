;;; m-media.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 17, 2022
;;
;;; Commentary:
;;
;;  My configuration for m-media.el
;;
;;; Code:

;; TODO Create a live chat popup when elfeed entry is a live.
;; ping https://www.youtube.com/live_chat?is_popout=1&v=YOUTUBEID
;; to see if it's livestream, if true, then open in browser.
;; I could use Surf or anoter minimal webbrowser to do that
;; TODO Podcast download function that saves elfeed entry with
;; podcast tag to podcast folder with POSIX filename

;; https://yewtu.be/watch?v=XOMj7JSGR78&t=3
(defun elfeed-open ()
  (interactive)
  (dolist (entry (elfeed-search-selected))
    (let ((link (elfeed-entry-link entry)))
      (if (string-match "watch" link)
	  (when (call-process-shell-command (format "mpv-tube \"%s\" &" link))
	    (message "Opening %s" (elfeed-entry-title entry))
	    (elfeed-search-untag-all-unread))
	(elfeed-search-show-entry entry)
	(message "Not a video entry")))))

(defun elfeed-copy-link ()
  (interactive)
  (dolist (entry (elfeed-search-selected))
    (let ((link (elfeed-entry-link entry)))
      (message "URL copied: %s" link)
      (kill-new link))))

(defun elfeed-copy-org-link ()
  (interactive)
  (dolist (entry (elfeed-search-selected))
    (let ((link (elfeed-entry-link entry))
	  (title (elfeed-entry-title entry)))
      (message "URL copied: %s" link)
      (kill-new (concat "[[" link "][" title "]]")))))

(defun elfeed-open-browser ()
  (interactive)
  (dolist (entry (elfeed-search-selected))
    (let ((link (elfeed-entry-link entry)))
      (when (call-process-shell-command (format "qutebrowser \"%s\" &" link))
	(message "Opening %s" (elfeed-entry-title entry))
	(elfeed-search-untag-all-unread)))))

(defun yt-dl-video-prompt ()
  (interactive)
  (let* ((link (completing-read "Enter a link: " nil))
	 (quality (completing-read "Max height resolution: " '("480" "720" "1080")))
	 (format (format "-f 'bestvideo[height<=%s]+bestaudio/best[height<=%s]'" quality quality))
	 (output "-o '%(title)s.%(ext)s'")
	 (default-directory m-offline-dir)
	 (output-buffer (generate-new-buffer "*YouTube-DL*")))
    (async-shell-command
     (format "youtube-dl %s %s '%s'" format output link) output-buffer)))

(defun elfeed-db-remake ()
  "Remove elfeed database and reload with new feeds"
  (interactive)
  (progn (elfeed-db-unload)
	 (delete-directory elfeed-db-directory t nil)
	 (elfeed-update)
	 (elfeed)))

(setup (:elpa mpdel))
(setq libmpdel-music-directory (expand-file-name "musics" m-data-dir))

(defun mpdel-seek-forward ()
  "Seek 10 seconds forward in song."
  (interactive)
  (libmpdel-send-command "seekcur +10"))
(defun mpdel-seek-back ()
  "Seek 10 seconds back in song."
  (interactive)
  (libmpdel-send-command "seekcur -10"))

(provide 'mf-media)
;;; m-media.el ends here
