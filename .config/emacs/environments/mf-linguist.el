;;; m-linguist.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 15, 2022
;;
;;; Commentary:
;;  
;; My configuration for language learning, linguistic work
;; and everything related to human languages.
;;  
;;; Code:

(setq ispell-program-name "hunspell")
(setup flyspell
  (setq flyspell-issue-message-flag nil)
  (let ((langs '("en_US" "pt_BR")))
    (setq lang-ring (make-ring (length langs)))
    (dolist (elem langs) (ring-insert lang-ring elem)))
  (defun cycle-ispell-languages ()
    (interactive)
    (let ((lang (ring-ref lang-ring -1)))
      (ring-insert lang-ring lang)
      (ispell-change-dictionary lang))))

(add-hook 'org-mode 'flyspell-mode)
(add-hook 'text-mode 'flyspell-mode)

(setup (:elpa consult-flyspell))

(setup (:elpa flyspell-correct))

(setq consult-flyspell-select-function 'flyspell-auto-correct-word)
(setq ispell-dictionary "pt_BR")
(setq default-input-method "ipa-praat")

(defun lookup-in (engine)
  "Look up the WORD under cursor in ENGINE.
	   If there is a text selection (a phrase), use that."
  (let ((word (replace-regexp-in-string " " "_" (get-word-regional))))
    (eww-browse-url (concat engine word))))
(defun lookup-in-dicio ()
  "Look up in dicio."
  (interactive)
  (lookup-in "https://www.dicio.com.br/"))
(defun lookup-in-wikipedia ()
  "Look up in wikipedia."
  (interactive)
  (lookup-in "http://en.wikipedia.org/wiki/"))

(defun translate-at-point ()
  "Translate word or region at point"
  (interactive)
  (let* ((language (completing-read "Translate to: " '("en" "fr" "ko" "it" "pt" "es")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b :%s %s" ,language ,word)))
		 (lambda (result)
		   (pos-tip-show-no-propertize result 'default)
                   (kill-new result)
		   (message result)))))

(defun speak-at-point ()
  "Talk word or region at point"
  (interactive)
  (let* ((language (completing-read "Speak in: " '("en" "fr" "ko" "it" "pt")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b -p :%s %s" ,language ,word))) nil)))

(define-derived-mode dict-mode fundamental-mode "dict"
  "major mode for editing my language code."
  (setq font-lock-defaults '(("-->\\([^<]+?\\)$" . 'font-lock-function-name-face)))
  (goto-char 0)
  (toggle-word-wrap)
  (read-only-mode))

(defun define-word-at-point ()
  "Define word at point and print results in buffer."
  (interactive)
  (let* ((word (current-word))
	 (buffer (get-buffer-create (concat word "-definition"))))
    (with-current-buffer buffer
      (insert (shell-command-to-string (format "sdcv -ne %s | sed '/^$/d'" word)))
      (dict-mode))
    (display-buffer buffer '(display-buffer-at-bottom . nil))
    (switch-to-buffer-other-window buffer)))

(provide 'mf-linguist)
;;; m-linguist.el ends here
