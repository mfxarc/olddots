;;; m-note-taking.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 15, 2022
;;
;;; Commentary:
;;
;;  My configuration for note-taking
;;
;;; Code:

(setup (:elpa org-roam)
  (setq org-roam-db-gc-threshold most-positive-fixnum)
  (setq org-roam-node-default-sort nil)
  (setq org-roam-completion-everywhere t)
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (:when-loaded (org-roam-db-autosync-mode)))

(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (push arg args))
	(org-roam-capture-templates (list (append (car org-roam-capture-templates)
						  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))

(defun org-roam-rg ()
  "Ripgrep into org-roam-directory"
  (interactive)
  (consult-ripgrep org-roam-directory))

(require 'org-protocol)
(setq org-html-validation-link nil)

(provide 'mf-note-taking)
;;; m-note-taking.el ends here
