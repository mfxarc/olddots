;;; uni-notes.el --- My uni-notes.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://gitlab.com/mfxarc>
;; Created: march 16, 2022
;; Package-Requires: ((emacs "24.4"))
;;

;;; Commentary:
;;
;;; Code:

;; ----------------- Dependecies

(setup (:elpa org-ref)
  (:also-elpa bibtex-completion)
  (require 'bibtex-completion)
  (require 'org-ref)
  (:option bibtex-completion-bibliography (list (expand-file-name "references/references.bib" uni-root-path))
	   bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"
	   bibtex-completion-additional-search-fields '(keywords)
	   bibtex-completion-display-formats
	   '((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
	     (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
	     (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	     (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	     (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
	   bibtex-completion-pdf-open-function (lambda (fpath) (call-process "open" nil 0 nil fpath))))

(add-to-list 'org-latex-listings-langs '(shell . "bash"))
(setq org-preview-latex-default-process 'dvisvgm)
(setq org-preview-latex-image-directory ".ltximg/")
(setq org-latex-preview-ltxpng-directory ".ltximg/")
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
(add-to-list 'org-latex-logfiles-extensions "tex")
(setq org-latex-compiler "xelatex")
(setq org-latex-remove-logfiles nil)
(setq org-latex-listings t)
(setq org-export-with-toc nil)
(setq org-export-coding-system 'utf-8-unix)
(setq org-latex-classes '(("article"
			   "\\documentclass{article}"
			   ("\\section{%s}" . "\\section*{%s}")
			   ("\\subsection{%s}" . "\\subsection*{%s}")
			   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			   ("\\paragraph{%s}" . "\\paragraph*{%s}")
			   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
			  ("book"
			   "\\documentclass{memoir}"
			   ("\\section{%s}" . "\\section*{%s}")
			   ("\\subsection{%s}" . "\\subsection*{%s}")
			   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			   ("\\paragraph{%s}" . "\\paragraph*{%s}")
			   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; ----------------- Variables

(defvar uni-root-path nil
  "Default path to university notes.")

(defvar uni-class-path nil
  "Default path to university classes.")

(defvar uni-books-path nil
  "Default path to university books reviews.")

(defvar uni-template-path nil
  "Default path to latex export template.")

(defvar uni-latex-enable nil
  "Enable experimental latex options or not.")

;; ----------------- Little utils

(defun uni--list-pdf ()
  "List notes of uni directory."
  (directory-files uni-root-path nil ".pdf" nil nil))

(defun uni--list-notes ()
  "List notes of uni directory."
  (directory-files uni-root-path nil ".org" nil nil))

(defun uni--list-classes ()
  "List classes of uni directory."
  (directory-files uni-class-path nil ".org" nil nil))

(defun uni--list-books ()
  "List books of uni directory."
  (directory-files uni-books-path nil "\\.pdf\\|\\.epub\\'" nil nil))

(defun uni--get-title (file)
  "Get title of note FILE."
  (replace-regexp-in-string
   ".*: " "" (substring
	      (shell-command-to-string
	       (format "rg -F -I '#+title:' %s" (expand-file-name file uni-root-path))) 0 -1)))

(defun uni--get-title-path (file)
  "Get title and path of note FILE as a list."
  (list (expand-file-name file uni-root-path) (uni--get-title file)))

(defun uni--insert-note (file)
  "Insert note FILE as a org-link."
  (let ((note (uni--get-title-path file)))
    (insert "[[file:" (nth 0 note) "][" (nth 1 note) "]]")))

(defun uni--rg-file (str &optional files-list)
  "Return a list of lines containing regexp STR.
if FILES-LIST true it returns a list of files instead."
  (let* ((default-directory uni-root-path)
	 (files (shell-command-to-string
		 (format "rg -t org -F %s %s 2>/dev/null" (if files-list "-l" "-I") (shell-quote-argument str)))))
    (split-string files "\n" t)))

;; ----------------- Main functions
(defun uni-new-note (title)
  "New university note with TITLE as argument."
  (interactive "sDiscipline topic name: ")
  (let* ((filename (downcase (replace-regexp-in-string "[[:space:]]+" "_" title)))
	 (fileorg (concat filename ".org"))
	 (filepathorg (expand-file-name fileorg uni-root-path))
	 (filepath (expand-file-name filename uni-root-path))
	 (fileother (expand-file-name title uni-root-path))
	 (date (format-time-string "%Y-%m-%d")))
    (if (or (file-exists-p fileother)
	    (file-exists-p filepath)
	    (file-exists-p filepathorg))
	(find-file (expand-file-name title uni-root-path))
      (let ((class (replace-regexp-in-string ".org" "" (completing-read "Class: " (uni--list-classes)))))
	(progn (make-empty-file filepathorg)
	       (find-file filepathorg)
	       (insert "#+title: " title "\n#+id: " class "\n#+date: " date)
	       (insert "\n#+setupfile: " uni-template-path)
	       (insert "\n\n\n\n\n#+LATEX:\referencias")
	       (goto-line 6)
	       (write-file filepathorg)
	       (uni-new-class class))))))

(defun uni-new-class (class)
  "New university CLASS file."
  (interactive "sClass id: ")
  (let* ((file (expand-file-name (concat "classes/" class ".org") uni-root-path)))
    (when (not (file-exists-p file))
      (progn (make-empty-file file)
	     (find-file file)
	     (insert "#+title: " class "\n")
	     (insert "#+topic: " (read-string "Class name: ") "\n")
	     (insert "#+author: \n")
	     (write-file file)
	     (kill-buffer (concat class ".org"))))))

(defun uni-delete-note ()
  "Delete university note."
  (interactive)
  (let* ((file (completing-read "Delete note: " (uni--list-notes)))
	 (filepath (expand-file-name file uni-root-path)))
    (when (yes-or-no-p (format "Really want to delete this note? %s" file))
      (progn (delete-file filepath)
	     (message "Note delete: %s" file)))))

(defun uni-rename-note ()
  "Rename university note."
  (interactive)
  (let* ((default-directory uni-root-path)
	 (file (completing-read "Rename note: " (uni--list-notes)))
	 (filepath (expand-file-name file uni-root-path))
	 (title (uni--get-title filepath))
	 (renamed (read-string (format "Rename %s to: " title)))
	 (filename (concat (downcase (replace-regexp-in-string "[[:space:]]+" "_" renamed)) ".org")))
    (when (shell-command (format "sd \"%s\" \"%s\" %s" (concat "title: " title) (concat "title: " renamed) filepath))
      (rename-file filepath filename)
      (message "Note renamed to: %s" renamed))))

(defun uni-open-folder ()
  "Open notes folder."
  (interactive)
  (find-file uni-root-path))

;; --- Searching

(defun uni-find-note ()
  "List university notes."
  (interactive)
  (uni-new-note (completing-read "Select note: " (uni--list-notes))))

(defun uni-find-note-class ()
  "Find notes by class."
  (interactive)
  (if-let ((files (uni--rg-file "#+id:"))
	   (class (replace-regexp-in-string ".*[[:space:]]+" "" (completing-read "Find note by class: " files)))
	   (note (completing-read (format "Notes of class %s: " class) (uni--rg-file (format "#+id: %s" class) t))))
      (find-file (expand-file-name note uni-root-path))
    (user-error "No notes found")))

(defun uni-find-class ()
  "List university classes."
  (interactive)
  (find-file (expand-file-name (completing-read "Select class: " (uni--list-classes)) uni-class-path)))

(defun uni-find-book ()
  "List university books and articles."
  (interactive)
  (find-file (expand-file-name (completing-read "Select book: " (uni--list-books)) uni-books-path)))

(defun uni-find-pdf ()
  "List notes exported to pdf."
  (interactive)
  (find-file (expand-file-name (completing-read "Select pdf: " (uni--list-pdf)) uni-root-path)))

(defun uni-find-rg ()
  "Return a list of files containing regexp STR."
  (interactive)
  (consult-ripgrep uni-root-path))

;; --- Inserting

(defun uni-insert-note ()
  "Insert note of notes list as a org-link."
  (interactive)
  (uni--insert-note (completing-read "Insert note: " (uni--list-notes))))

;; --- Exporting

(defun uni-export-pdf ()
  "Export note as pdf using latex."
  (interactive)
  (let* ((buffer (buffer-name))
	 (base (file-name-base buffer))
	 (tex (concat (file-name-base buffer) ".tex"))
	 (pdf (concat (file-name-base buffer) ".pdf")))
    (when (and (eq major-mode 'org-mode)
	       (string-match uni-root-path (file-name-directory buffer-file-name)))
      (progn (org-latex-export-to-latex)
	     (call-process-shell-command (format "xelatex -interaction=batchmode %s 1>/dev/null &" tex))
	     (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	       (call-process-shell-command (format "zathura %s &" pdf)))
	     (message "Compiling %s..." base)))))

(defun uni-publish-pdf ()
  "Export note as pdf using latex."
  (interactive)
  (let* ((buffer (buffer-name))
	 (base (file-name-base buffer))
	 (tex (concat (file-name-base buffer) ".tex"))
	 (xdv (concat (file-name-base buffer) ".xdv"))
	 (pdf (concat (file-name-base buffer) ".pdf")))
    (when (and (eq major-mode 'org-mode)
	       (string-match uni-root-path (file-name-directory buffer-file-name)))
      (progn (org-latex-export-to-latex)
	     (call-process-shell-command (format "latexmk -xelatex -interaction=batchmode %s 1>/dev/null &" tex))
	     (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	       (call-process-shell-command (format "zathura %s &" pdf)))
	     (message "Compiling %s..." base)))))

(add-hook 'after-save-hook 'uni-export-pdf)

(defun uni-variable-font ()
  (interactive)
  (when (string-match uni-root-path (buffer-file-name))
    (set-face-attribute 'variable-pitch nil :font "Libertinus Serif" :height 190)
    (variable-pitch-mode)))

(add-hook 'org-mode-hook 'uni-variable-font)

;; ----------------- Transient menus

(define-transient-command uni-note-menu ()
  "Menu to manage university notes."
  [
   ["Creating"
    ("n"  "Note" uni-new-note)
    ("C-c"  "Class" uni-new-class)
    ]
   ["List"
    ("f"  "Notes" uni-find-note)
    ("v"  "PDF" uni-find-pdf)
    ("c"  "Classes" uni-find-class)
    ]
   [""
    ("l"  "Books" uni-find-book)
    ]
   ["Searching"
    ("s"  "Notes by class" uni-find-note-class)
    ("r"  "Notes by regex" uni-find-rg)
    ]
   ["Management"
    ("p"  "Open notes folder" uni-open-folder)
    ("d"  "Delete note" uni-delete-note)
    ("C-r" "Rename note" uni-rename-note)
    ]
   ["Insert"
    ("o"  "Note" uni-insert-note)
    ("i"  "Reference" org-ref-insert-cite-link)
    ]
   ["Export"
    ("e"  "PDF" uni-publish-pdf)
    ]])

(provide 'uni-notes)
;;; uni-notes.el ends here
