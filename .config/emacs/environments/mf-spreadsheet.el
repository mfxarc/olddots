;;; m-spreadsheet.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 15, 2022
;;
;;; Commentary:
;;  
;; My configuration for spreadsheet working
;; and everything related to tables.
;;  
;;; Code:

(setup (:elpa csv-mode)
  (:file-match "\\.csv\\'")
  (:when-loaded (add-hook 'csv-mode-hook 'csv-align-mode)))

(setup (:elpa mermaid-mode))

(provide 'mf-spreadsheet)
;;; m-spreadsheet.el ends here
