;;; flat-theme.el --- A color theme for Emacs based on flatuicolors.com -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Copyright (C) 2021 Marcos Felipe
;;
;; Author: Marcos Felipe <https://gitlab.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: setembro 30, 2021
;; Modified: setembro 30, 2021
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;; Inspiration
;; Notion & Typora
;; TODO
(deftheme flat "FlatUI based color theme.")
;;; Color Palette
(defvar flatui-colors-alist
  '(("clouds"          . "#F5F7F7") ;--------
    ("silver"          . "#DFE4EA") ;--------
    ("darker"          . "#CED5DF") ;--------
    ("asbestos"        . "#7F8C8D") ;--------
    ("carrot"          . "#C0505F") ;--------
    ("french"          . "#0A74B9") ;--------
    ("independence"    . "#495C7E") ;--------
    ("midnight"        . "#2C3E50") ;--------
    ("viridian"        . "#12866F") ;--------
    ("emerald"         . "#2ECC71")
    ("nephritis"       . "#27AE60")
    ("amethyst"        . "#9B59B6")
    ("wisteria"        . "#8E44AD")
    ("sun-flower"      . "#F1C40F"))
  "List of FlatUI colors.
Each element has the form (NAME . HEX). ")
(defmacro flatui/with-color-variables (&rest body)
  "`let' bind all colors defined in `flatui-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
	     ,@(mapcar (lambda (cons)
		             (list (intern (car cons)) (cdr cons)))
		           flatui-colors-alist))
     ,@body))
;; Setting special fonts
(set-fontset-font "fontset-default" 'hangul '("Source Han Sans KR" . "unicode-bmp"))
;;; Theme Faces
(flatui/with-color-variables
  (custom-theme-set-faces
   'flat
   '(button ((t (:underline t))))
   ;; Default fonts
   `(default ((t (:font "Iosevka" :height 170 :foreground ,midnight :background ,clouds))))
   `(fixed-pitch ((t (:inherit default))))
   `(variable-pitch ((t (:font "Roboto" :height 170))))
   `(italic ((t (:inherit variable-pitch :slant italic))))
   ;; Windows
   `(fringe ((t (:inherit default :background nil))))
   `(window-divider ((t (:inherit default :background nil :foreground ,clouds))))
   `(window-divider-first-pixel ((t (:inherit window-divider))))
   `(window-divider-last-pixel ((t (:inherit window-divider))))
   ;; Minibuffer
   `(minibuffer-prompt ((t (:foreground ,midnight :weight bold))))
   `(Info-quoted ((t (:inherit minibuffer-prompt))))
   ;; Completion
   `(completions-annotations ((t (:inherit default :underline nil :foreground ,asbestos))))
   `(highlight ((t (:background ,silver))))
   `(vertico-current ((t (:background ,darker))))
   ;; Overlay
   `(eros-result-overlay-face ((t (:foreground ,midnight :box (:line-width 1 :color ,midnight)))))
   ;; Org-mode
   `(org-block ((t (:inherit default :background ,silver))))
   `(org-quote ((t (:inherit italic :background ,silver))))
   `(org-verbatim ((t (:inherit default :foreground ,carrot :box (:line-width 1 :color ,darker)))))
   `(org-document-title ((t (:foreground ,midnight))))
   `(org-document-info ((t (:foreground ,asbestos))))
   `(org-document-info-keyword ((t (:foreground ,asbestos))))
   ;; Elfeed
   `(elfeed-search-feed-face ((t (:foreground ,french))))
   `(elfeed-search-tag-face ((t (:inherit font-lock-variable-name-face))))
   `(elfeed-search-title-face ((t (:background nil :foreground ,asbestos))))
   `(elfeed-search-unread-title-face ((t (:background nil :foreground ,midnight))))
   `(message-header-name ((t (:inherit org-document-info-keyword))))
   `(message-header-subject ((t (:inherit org-document-title))))
   `(message-header-to ((t (:inherit org-document-title))))
   `(message-header-other ((t (:inherit org-document-info))))
   ;; Magit
   `(magit-section-heading ((t (:inherit org-level-1))))
   `(magit-diff-file-heading ((t (:inherit default))))
   `(magit-branch-remote ((t (:inherit font-lock-constant-face))))
   ;; LaTeX
   `(font-latex-sectioning-0-face ((t (:inherit org-level-1))))
   `(font-latex-sectioning-1-face ((t (:inherit org-level-2))))
   `(font-latex-sectioning-2-face ((t (:inherit org-level-3))))
   `(font-latex-sectioning-3-face ((t (:inherit org-level-4))))
   `(font-latex-sectioning-4-face ((t (:inherit org-level-5))))
   `(font-latex-sectioning-5-face ((t (:inherit org-level-6))))
   `(font-latex-italic-face ((t (:inherit italic))))
   `(font-latex-bold-face ((t (:weight bold))))
   `(font-latex-warning-face ((t (:inherit warning))))
   ;; Hydra
   `(hydra-face-red ((t (:foreground ,carrot))))
   `(hydra-face-blue ((t (:foreground ,french))))
   `(hydra-face-pink ((t (:foreground ,amethyst))))
   ;; EWW
   `(eww-valid-certificate ((t (:inherit default))))
   `(eww-form-text ((t (:inherit default :box (:line-width 1)))))
   `(eww-form-file ((t (:inherit eww-form-text))))
   `(eww-form-select ((t (:inherit eww-form-text))))
   `(eww-form-submit ((t (:inherit eww-form-text))))
   `(eww-form-checkbox ((t (:inherit eww-form-text))))
   `(eww-form-textarea ((t (:inherit eww-form-text))))
   ;;
   `(link ((t (:underline t))))
   `(link-visited ((t (:underline t))))
   `(cursor ((t (:foreground ,midnight :background ,midnight))))
   `(escape-glyph ((t (:foreground ,sun-flower :bold t))))
;;;;; compilation
   `(success ((t (:foreground ,nephritis :weight bold))))
   `(warning ((t (:foreground ,carrot :weight bold))))
   `(compilation-column-face ((t (:foreground ,french))))
   `(compilation-enter-directory-face ((t (:foreground ,viridian))))
   `(compilation-error-face ((t (:foreground ,carrot :weight bold :underline t))))
   `(compilation-face ((t (:foreground ,midnight))))
   `(compilation-info-face ((t (:foreground ,french))))
   `(compilation-info ((t (:foreground ,nephritis :underline t))))
   `(compilation-leave-directory-face ((t (:foreground ,wisteria))))
   `(compilation-line-face ((t (:foreground ,sun-flower))))
   `(compilation-line-number ((t (:foreground ,sun-flower))))
   `(compilation-message-face ((t (:foreground ,midnight))))
   `(compilation-warning-face ((t (:foreground ,carrot :weight bold :underline t))))
   `(compilation-mode-line-exit ((t (:foreground ,independence :weight bold))))
   `(compilation-mode-line-fail ((t (:foreground ,carrot :weight bold))))
   `(compilation-mode-line-run ((t (:foreground ,french :weight bold))))
;;;;; grep
   `(grep-context-face ((t (:foreground ,midnight))))
   `(grep-error-face ((t (:foreground ,carrot :weight bold :underline t))))
   `(grep-hit-face ((t (:foreground ,independence :weight bold))))
   `(grep-match-face ((t (:foreground ,sun-flower :weight bold))))
   `(match ((t (:background ,independence :foreground ,midnight))))
;;;;; isearch
   `(isearch ((t (:foreground ,clouds :weight bold :background ,carrot))))
   `(isearch-fail ((t (:foreground ,sun-flower :weight bold :background ,carrot))))
   `(lazy-highlight ((t (:foreground ,midnight :weight bold :background ,sun-flower))))

   `(menu ((t (:foreground ,midnight :background ,silver))))
   `(region ((,class (:background ,sun-flower :foreground ,midnight))
	         (t :inverse-video t)))
   `(secondary-selection ((t (:background ,independence))))
   `(trailing-whitespace ((t (:background ,carrot))))
   `(vertical-border ((t (:foreground ,silver))))
;;;;; font lock
   `(font-lock-builtin-face ((t (:foreground ,viridian))))
   `(font-lock-comment-face ((t (:foreground ,asbestos :slant italic))))
   `(sh-heredoc ((t (:inherit font-lock-comment-face))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,asbestos))))
   `(font-lock-constant-face ((t (:foreground ,carrot))))
   `(font-lock-doc-face ((t (:foreground ,asbestos))))
   `(font-lock-function-name-face ((t (:foreground ,midnight :weight normal))))
   `(font-lock-keyword-face ((t (:foreground ,french))))
   `(font-lock-negation-char-face ((t (:foreground ,french :weight normal))))
   `(font-lock-preprocessor-face ((t (:foreground ,carrot :weight normal))))
   `(font-lock-regexp-grouping-construct ((t (:foreground ,french :weight normal))))
   `(font-lock-regexp-grouping-backslash ((t (:foreground ,amethyst :weight normal))))
   `(font-lock-string-face ((t (:foreground ,independence))))
   `(font-lock-type-face ((t (:foreground ,french))))
   `(font-lock-variable-name-face ((t (:foreground ,wisteria))))
   `(font-lock-warning-face ((t (:foreground ,carrot :weight normal))))

   `(c-annotation-face ((t (:inherit font-lock-constant-face))))
;;;;; ledger
   `(ledger-font-directive-face ((t (:foreground ,nephritis))))
   `(ledger-font-periodic-xact-face ((t (:inherit ledger-font-directive-face))))
   `(ledger-occur-xact-face ((t (:background ,silver))))
;;;; Third-party
;;;;; ace-jump
   `(ace-jump-face-background
     ((t (:foreground ,asbestos :background ,clouds :inverse-video nil))))
   `(ace-jump-face-foreground
     ((t (:foreground ,carrot :background ,clouds :inverse-video nil))))
;;;;; anzu
   `(anzu-mode-line ((t (:foreground ,independence :weight bold))))
;;;;; auto-complete
   `(ac-candidate-face ((t (:background ,asbestos :foreground ,midnight))))
   `(ac-selection-face ((t (:background ,asbestos :foreground ,midnight))))
   `(popup-tip-face ((t (:inherit org-verbatim))))
   `(popup-scroll-bar-foreground-face ((t (:background ,asbestos))))
   `(popup-scroll-bar-background-face ((t (:background ,silver))))
   `(popup-isearch-match ((t (:background ,clouds :foreground ,midnight))))
;;;;; clojure-test-mode
   `(clojure-test-failure-face ((t (:foreground ,carrot :weight bold :underline t))))
   `(clojure-test-error-face ((t (:foreground ,carrot :weight bold :underline t))))
   `(clojure-test-success-face ((t (:foreground ,emerald :weight bold :underline t))))
;;;;; diff
   `(diff-added ((,class (:foreground ,nephritis :background ,clouds))
		         (t (:foreground ,nephritis :background ,clouds))))
   `(diff-changed ((t (:foreground ,carrot))))
   `(diff-context ((t (:foreground ,asbestos))))
   `(diff-removed ((,class (:foreground ,carrot :background ,clouds))
		           (t (:foreground ,carrot :background ,clouds))))
   `(diff-header ((,class (:foreground ,midnight :weight bold))
		          (t (:foreground ,midnight :weight bold))))
   `(diff-file-header
     ((,class (:foreground ,midnight :weight bold))
      (t (:foreground ,midnight :weight bold))))
   `(diff-hunk-header
     ((,class (:foreground ,wisteria :weight bold))
      (t (:foreground ,wisteria :weight bold))))

;;;;; diff-hl
   `(diff-hl-insert ((t (:foreground ,viridian :background ,nephritis))))
   `(diff-hl-delete ((t (:foreground ,carrot :background ,carrot))))
   `(diff-hl-change ((t (:foreground ,carrot :background ,carrot))))

;;;;; dired/dired+/dired-subtree
   `(diredp-display-msg ((t (:foreground ,french))))
   `(diredp-compressed-file-suffix ((t (:foreground ,amethyst))))
   `(diredp-date-time ((t (:foreground ,carrot))))
   `(diredp-deletion ((t (:foreground ,carrot))))
   `(diredp-deletion-file-name ((t (:foreground ,carrot))))
   `(diredp-dir-heading ((t (:foreground ,french :background ,silver :weight bold))))
   `(diredp-dir-priv ((t (:foreground ,french))))
   `(diredp-exec-priv ((t (:foreground ,carrot))))
   `(diredp-executable-tag ((t (:foreground ,carrot))))
   `(diredp-file-name ((t (:foreground ,midnight))))
   `(diredp-file-suffix ((t (:foreground ,wisteria))))
   `(diredp-flag-mark ((t (:foreground ,sun-flower))))
   `(diredp-flag-mark-line ((t (:foreground ,french))))
   `(diredp-ignored-file-name ((t (:foreground ,asbestos))))
   `(diredp-link-priv ((t (:foreground ,amethyst))))
   `(diredp-mode-line-flagged ((t (:foreground ,sun-flower))))
   `(diredp-mode-line-marked ((t (:foreground ,french))))
   `(diredp-no-priv ((t (:foreground ,midnight))))
   `(diredp-number ((t (:foreground ,french))))
   `(diredp-other-priv ((t (:foreground ,midnight))))
   `(diredp-rare-priv ((t (:foreground ,carrot))))
   `(diredp-read-priv ((t (:foreground ,viridian))))
   `(diredp-symlink ((t (:foreground ,clouds :background ,amethyst))))
   `(diredp-write-priv ((t (:foreground ,carrot))))
   `(dired-subtree-depth-1-face ((t (:background ,silver))))
   `(dired-subtree-depth-2-face ((t (:background ,asbestos))))
   `(dired-subtree-depth-3-face ((t (:background ,asbestos))))
;;;;; ediff
   `(ediff-current-diff-A ((t (:foreground ,midnight :background ,carrot))))
   `(ediff-current-diff-Ancestor ((t (:foreground ,midnight :background ,carrot))))
   `(ediff-current-diff-B ((t (:foreground ,midnight :background ,emerald))))
   `(ediff-current-diff-C ((t (:foreground ,midnight :background ,french))))
   `(ediff-even-diff-A ((t (:background ,clouds))))
   `(ediff-even-diff-Ancestor ((t (:background ,clouds))))
   `(ediff-even-diff-B ((t (:background ,silver))))
   `(ediff-even-diff-C ((t (:background ,silver))))
   `(ediff-fine-diff-A ((t (:foreground ,midnight :background ,carrot :weight bold))))
   `(ediff-fine-diff-Ancestor ((t (:foreground ,midnight :background ,carrot weight bold))))
   `(ediff-fine-diff-B ((t (:foreground ,midnight :background ,emerald :weight bold))))
   `(ediff-fine-diff-C ((t (:foreground ,midnight :background ,french :weight bold ))))
   `(ediff-odd-diff-A ((t (:background ,silver))))
   `(ediff-odd-diff-Ancestor ((t (:background ,silver))))
   `(ediff-odd-diff-B ((t (:background ,silver))))
   `(ediff-odd-diff-C ((t (:background ,silver))))
;;;;; eshell
   `(eshell-prompt ((t (:foreground ,carrot :weight bold))))
   `(eshell-ls-archive ((t (:foreground ,amethyst :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,french :weight bold))))
   `(eshell-ls-executable ((t (:foreground ,carrot))))
   `(eshell-ls-unreadable ((t (:foreground ,asbestos))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,sun-flower :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,clouds :background ,amethyst))))
;;;;; evil
   `(evil-search-highlight-persist-highlight-face ((t (:inherit lazy-highlight))))
;;;;; flx
   `(flx-highlight-face ((t (:foreground ,french :weight bold))))
;;;;; flycheck
   `(flycheck-error
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,carrot) :inherit unspecified))
      (t (:foreground ,carrot :weight bold :underline t))))
   `(flycheck-warning
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,french) :inherit unspecified))
      (t (:foreground ,sun-flower :weight bold :underline t))))
   `(flycheck-info
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria) :inherit unspecified))
      (t (:foreground ,amethyst :weight bold :underline t))))
   `(flycheck-fringe-error ((t (:foreground ,carrot :weight bold))))
   `(flycheck-fringe-warning ((t (:foreground ,french :weight bold))))
   `(flycheck-fringe-info ((t (:foreground ,wisteria :weight bold))))
;;;;; flymake
   `(flymake-errline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,carrot)
		           :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,carrot :weight bold :underline t))))
   `(flymake-warnline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,french)
		           :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,french :weight bold :underline t))))
   `(flymake-infoline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria)
		           :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,wisteria :weight bold :underline t))))
;;;;; flyspell
   `(flyspell-duplicate
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,french) :inherit unspecified))
      (t (:foreground ,french :weight bold :underline t))))
   `(flyspell-incorrect
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,carrot) :inherit unspecified))
      (t (:foreground ,carrot :weight bold :underline t))))
;;;;; git-gutter
   `(git-gutter:added ((t (:foreground ,emerald :weight bold))))
   `(git-gutter:deleted ((t (:foreground ,carrot :weight bold))))
   `(git-gutter:modified ((t (:foreground ,carrot :weight bold))))
   `(git-gutter:unchanged ((t (:foreground ,midnight :weight bold))))
;;;;; git-gutter-fr
   `(git-gutter-fr:added ((t (:foreground ,emerald  :weight bold))))
   `(git-gutter-fr:deleted ((t (:foreground ,carrot :weight bold))))
   `(git-gutter-fr:modified ((t (:foreground ,carrot :weight bold))))
;;;;; guide-key
   `(guide-key/highlight-command-face ((t (:foreground ,french))))
   `(guide-key/key-face ((t (:foreground ,french))))
   `(guide-key/prefix-command-face ((t (:foreground ,wisteria))))
;;;;; highlight-symbol
   `(highlight-symbol-face ((t (:background "gray88" :underline t))))
;;;;; hl-line-mode
   `(hl-line-face ((,class (:background ,silver)) (t :weight bold)))
   `(hl-line ((,class (:background ,silver)) (t :weight bold)))
;;;;; hl-sexp
   `(hl-sexp-face ((,class (:background ,silver)) (t :weight bold)))
;;;;; ido-mode
   `(ido-first-match ((t (:foreground ,carrot :weight bold))))
   `(ido-only-match ((t (:foreground ,carrot :weight bold))))
   `(ido-subdir ((t (:foreground ,french))))
   `(ido-indicator ((t (:foreground ,sun-flower :background ,carrot))))
;;;;; indent-guide
   `(indent-guide-face ((t (:foreground ,asbestos))))
;;;;; js2-mode
   `(js2-warning ((t (:underline ,carrot))))
   `(js2-error ((t (:foreground ,carrot :weight bold))))
   `(js2-jsdoc-tag ((t (:foreground ,amethyst))))
   `(js2-jsdoc-type ((t (:foreground ,wisteria))))
   `(js2-jsdoc-value ((t (:foreground ,french))))
   `(js2-function-param ((t (:foreground, midnight))))
   `(js2-external-variable ((t (:foreground ,carrot))))
;;;;; linum-mode
   `(linum ((t (:foreground ,midnight :background ,silver))))
;;;;; magit
   `(magit-header ((t (:foreground ,midnight :background nil :weight bold))))
   `(magit-section-title ((t (:foreground ,midnight :background nil :weight bold))))
   `(magit-branch ((t (:foreground ,midnight :background ,independence
				                   :weight bold
				                   :box (:line-width 1 :color ,viridian)))))
   `(magit-item-highlight ((t (:background ,silver))))
   `(magit-log-author ((t (:foreground ,french))))
   `(magit-log-sha1 ((t (:foreground ,carrot :weight bold))))
   `(magit-tag ((t (:foreground ,wisteria :weight bold))))
   `(magit-log-head-label-head ((t (:foreground ,midnight :background ,independence
						                        :weight bold
						                        :box (:line-width 1 :color ,viridian)))))
   `(magit-log-head-label-local ((t (:foreground ,midnight :background ,independence
						                         :weight bold
						                         :box (:line-width 1 :color ,viridian)))))
   `(magit-log-head-label-default ((t (:foreground ,midnight :background ,independence
						                           :weight bold
						                           :box (:line-width 1 :color ,viridian)))))
   `(magit-log-head-label-remote ((t (:foreground ,midnight :background ,sun-flower
						                          :weight bold
						                          :box (:line-width 1 :color ,french)))))
   `(magit-log-head-label-tags ((t (:foreground ,wisteria :weight bold))))
;;;;; org-mode
   `(org-agenda-date-today ((t (:foreground ,silver :slant italic :weight bold))) t)
   `(org-agenda-structure ((t (:inherit font-lock-comment-face))))
   `(org-archived ((t (:foreground ,midnight :weight bold))))
   `(org-checkbox ((t (:foreground ,midnight))))
   `(org-meta-line ((t (:foreground ,asbestos))))
   `(org-drawer ((t (:foreground ,silver))))
   `(org-date ((t (:foreground ,french :underline t))))
   `(org-deadline-announce ((t (:foreground ,carrot))))
   `(org-done ((t (:bold t :weight bold :foreground ,emerald))))
   `(org-formula ((t (:foreground ,sun-flower))))
   `(org-headline-done ((t (:foreground ,emerald))))
   `(org-hide ((t (:foreground ,clouds))))
   `(org-indent ((t (:inherit (org-hide default)))))
   `(org-level-1 ((t (:foreground ,midnight :weight bold :height 1.0))))
   `(org-level-2 ((t (:inherit org-level-1))))
   `(org-level-3 ((t (:inherit org-level-1))))
   `(org-level-4 ((t (:inherit org-level-1))))
   `(org-level-5 ((t (:inherit org-level-1))))
   `(org-level-6 ((t (:inherit org-level-1))))
   `(org-level-7 ((t (:inherit org-level-1))))
   `(org-level-8 ((t (:inherit org-level-1))))
   `(org-link ((t (:inherit link :foreground ,french))))
   `(org-scheduled ((t (:foreground ,nephritis))))
   `(org-special-keyword ((t (:inherit font-lock-comment-face))))
   `(org-table ((t (:foreground ,midnight))))
   `(org-tag ((t (:bold t :weight bold))))
   `(org-todo ((t (:bold t :foreground ,carrot :weight bold))))
   `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))
   `(org-warning ((t (:bold t :foreground ,carrot :weight bold :underline nil))))
   `(org-footnote ((t (:foreground ,amethyst :weight bold))))
;;;;; outline
   `(outline-1 ((t (:foreground ,viridian))))
   `(outline-2 ((t (:foreground ,french))))
   `(outline-3 ((t (:foreground ,wisteria))))
   `(outline-4 ((t (:foreground ,french))))
   `(outline-5 ((t (:foreground ,carrot))))
   `(outline-6 ((t (:foreground ,carrot))))
;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,midnight))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,independence))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,emerald))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,french))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,amethyst))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,sun-flower))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,carrot))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,carrot))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,viridian))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,nephritis))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,french))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,wisteria))))
;;;;; structured-haskell-mode
   `(shm-current-face ((t (:background ,silver))))
   `(shm-quarantine-face ((t (:inherit font-lock-error))))
;;;;; show-paren
   `(show-paren-mismatch ((t (:foreground ,sun-flower :background ,carrot :weight bold))))
   `(show-paren-match ((t (:foreground ,clouds :background ,amethyst :weight bold))))
;;;;; mode-line
   `(header-line ((t (:font "Iosevka Fixed" :height 150 :foreground ,midnight :background ,clouds))))
   `(mode-line ((,class (:foreground ,midnight :background ,silver :font "Noto Sans" :height 92 :box (:line-width 1 :color ,darker)))
		        (t :inverse-video t)))
   `(mode-line-inactive ((t (:inherit mode-line :foreground ,asbestos))))
   `(doom-modeline-panel ((t (:foreground nil :background nil))))
   `(persp-selected-face ((t (:foreground ,midnight :weight light :height 92))))
   `(mode-line-buffer-id ((t (:foreground ,midnight))))
   `(doom-modeline-buffer-modified ((t (:foreground ,carrot))))
;;;;; SLIME
   `(slime-repl-output-face ((t (:foreground ,midnight))))
   `(slime-repl-inputed-output-face ((t (:foreground ,midnight))))
   `(slime-error-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,carrot)))
      (t
       (:underline ,carrot))))
   `(slime-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,french)))
      (t
       (:underline ,french))))
   `(slime-style-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,sun-flower)))
      (t
       (:underline ,sun-flower))))
   `(slime-note-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria)))
      (t
       (:underline ,wisteria))))
   `(slime-highlight-face ((t (:inherit independence))))
;;;;; term
   `(term-color-black ((t (:foreground ,midnight :background ,midnight))))
   `(term-color-red ((t (:foreground ,carrot :background ,carrot))))
   `(term-color-green ((t (:foreground ,nephritis :background ,nephritis))))
   `(term-color-yellow ((t (:foreground ,french :background ,french))))
   `(term-color-blue ((t (:foreground ,french :background ,french))))
   `(term-color-magenta ((t (:foreground ,wisteria :background ,wisteria))))
   `(term-color-cyan ((t (:foreground ,viridian :background ,viridian))))
   `(term-color-white ((t (:foreground ,clouds :background ,clouds))))
   '(term-default-fg-color ((t (:inherit term-color-white))))
   '(term-default-bg-color ((t (:inherit term-color-black))))
;;;;; web-mode
   `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
   `(web-mode-css-at-rule-face ((t (:foreground ,carrot ))))
   `(web-mode-css-prop-face ((t (:foreground ,carrot))))
   `(web-mode-css-pseudo-class-face ((t (:foreground ,french :weight bold))))
   `(web-mode-css-rule-face ((t (:foreground ,french))))
   `(web-mode-doctype-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-folded-face ((t (:underline t))))
   `(web-mode-function-name-face ((t (:foreground ,midnight :weight bold))))
   `(web-mode-html-attr-name-face ((t (:foreground ,wisteria))))
   `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-html-tag-face ((t (:foreground ,independence :weight bold))))
   `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
   `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
   `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
   `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
   `(web-mode-server-background-face ((t (:background ,clouds))))
   `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
   `(web-mode-server-string-face ((t (:foreground ,carrot))))
   `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
   `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
   `(web-mode-whitespaces-face ((t (:background ,carrot))))
   `(web-mode-block-face ((t (:background "gray88"))))
   `(web-mode-current-element-highlight-face ((t (:inverse-video t))))
;;;;; whitespace-mode
   `(whitespace-space ((t (:background ,clouds :foreground ,sun-flower))))
   `(whitespace-hspace ((t (:background ,clouds :foreground ,sun-flower))))
   `(whitespace-tab ((t (:background ,carrot))))
   `(whitespace-newline ((t (:foreground ,sun-flower))))
   `(whitespace-trailing ((t (:background ,carrot))))
   `(whitespace-line ((t (:background nil :foreground ,carrot))))
   `(whitespace-space-before-tab ((t (:background ,clouds :foreground ,carrot))))
   `(whitespace-indentation ((t (:background ,clouds :foreground ,sun-flower))))
   `(whitespace-empty ((t (:background ,french))))
   `(whitespace-space-after-tab ((t (:background ,clouds :foreground ,carrot))))
;;;;; which-func-mode
   `(which-func ((t (:foreground ,wisteria :background ,silver))))
;;;;; yascroll
   `(yascroll:thumb-text-area ((t (:background ,silver))))
   `(yascroll:thumb-fringe ((t (:background ,silver :foreground ,sun-flower))))
   ))

;;; Theme Variables
(flatui/with-color-variables
  (custom-theme-set-variables
   'flat
;;;;; ansi-color
   `(ansi-color-names-vector [,clouds ,carrot ,emerald ,sun-flower
				                      ,french ,amethyst ,independence ,midnight])
;;;;; fill-column-indicator
   `(fci-rule-color ,sun-flower)
;;;;; vc-annotate
   `(vc-annotate-color-map
     '(( 30. . ,carrot)
       ( 60. . ,carrot)
       ( 90. . ,carrot)
       (120. . ,carrot)
       (150. . ,sun-flower)
       (180. . ,french)
       (210. . ,emerald)
       (240. . ,nephritis)
       (270. . ,independence)
       (300. . ,viridian)
       (330. . ,french)
       (360. . ,french)))
   `(vc-annotate-very-old-color ,french)
   `(vc-annotate-background ,clouds)
   ))
;;; Footer
;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
		          (file-name-as-directory
		           (file-name-directory load-file-name))))
(provide-theme 'flat)
;;; flat-theme.el ends here
