;;; flat-theme.el --- A color theme for Emacs based on flatuicolors.com -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Copyright (C) 2021 Marcos Felipe
;;
;; Author: Marcos Felipe <https://gitlab.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: setembro 30, 2021
;; Modified: setembro 30, 2021
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;; Inspiration
;; Notion & Typora
;; TODO
;; Add elfeed support
;; Add better magit colors

(deftheme flat-dark "FlatUI based color theme.")
;;; Color Palette
(defvar flatui-dark-colors-alist
  '(("silver"          . "#C3C9D5") ;------
    ("concrete"        . "#5A5D72") ;------
    ("blend"           . "#414856") ;------
    ("dark"            . "#282A33") ;------
    ("darker"          . "#21222A") ;------
    ("cadet"           . "#78A19B") ;------
    ("belize-hole"     . "#4B8F8C") ;------
    ("sun-flower"      . "#7389AE") ;------
    ("midnight"        . "#2C3E50") ;------
    ("pumpkin"         . "#CC707C") ;------
    ("pumpkin-dark"    . "#963643") ;------
    ("french"          . "#0A74B9") ;------
    ("orange"          . "#F1C40F") ;------
    ("emerald"         . "#2ECC71")
    ("nephritis"       . "#27AE60")
    ("amethyst"        . "#9B59B6")
    ("wisteria"        . "#8E44AD"))
  "List of FlatUI colors.
Each element has the form (NAME . HEX). ")
(defmacro flatui-dark/with-color-variables (&rest body)
  "`let' bind all colors defined in `flatui-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
	     ,@(mapcar (lambda (cons)
		             (list (intern (car cons)) (cdr cons)))
		           flatui-dark-colors-alist))
     ,@body))
;; Setting special fonts
(set-fontset-font "fontset-default" 'hangul '("Source Han Sans KR" . "unicode-bmp"))
;;; Theme Faces
(flatui-dark/with-color-variables
  (custom-theme-set-faces
   'flat-dark
;;;; Built-in
   '(button ((t (:underline t))))
   ;; Defaults
   `(default ((t (:font "Iosevka" :height 170 :foreground ,silver :background ,dark))))
   `(fixed-pitch ((t (:inherit default))))
   `(variable-pitch ((t (:font "Roboto" :weight regular :height 170))))
   `(italic ((t (:inherit variable-pitch :slant italic ))))
   ;; Windows
   `(fringe ((t (:inherit default :background nil))))
   `(window-divider ((t (:inherit default :background nil :foreground ,dark))))
   `(window-divider-first-pixel ((t (:inherit window-divider))))
   `(window-divider-last-pixel ((t (:inherit window-divider))))
   ;; Completion
   `(completions-annotations ((t (:inherit default :underline nil :slant italic :foreground ,concrete))))
   `(highlight ((t (:background ,darker))))
   `(vertico-current ((t (:background ,darker))))
   ;; Elfeed
   `(elfeed-search-feed-face ((t (:foreground ,belize-hole))))
   `(elfeed-search-tag-face ((t (:inherit font-lock-variable-name-face))))
   `(elfeed-search-date-face ((t (:foreground ,belize-hole))))
   `(elfeed-search-title-face ((t (:foreground ,blend))))
   `(elfeed-search-unread-title-face ((t (:background nil :foreground ,silver))))
   `(message-header-name ((t (:inherit org-document-info-keyword))))
   `(message-header-subject ((t (:inherit org-document-title))))
   `(message-header-to ((t (:inherit org-document-title))))
   `(message-header-other ((t (:inherit org-document-info))))
   ;; Org
   `(org-block ((t (:inherit default :background ,darker))))
   `(org-verbatim ((t (:inherit default :background ,darker :foreground ,emerald :box (:line-width 1 :color ,darker)))))
   `(org-document-title ((t (:foreground ,silver))))
   `(org-document-info ((t (:foreground ,blend))))
   `(org-document-info-keyword ((t (:inherit org-document-info))))
   `(org-property-value ((t (:inherit org-document-info))))
   `(org-special-keyword ((t (:inherit org-document-info))))
   `(org-meta-line ((t (:inherit org-document-info-keyword))))
   `(org-drawer ((t (:inherit org-document-info-keyword))))
   `(org-indent ((t (:inherit (org-hide default)))))
   `(org-level-1 ((t (:foreground ,silver :weight bold :height 1.0))))
   `(org-level-2 ((t (:inherit org-level-1))))
   `(org-level-3 ((t (:inherit org-level-1))))
   `(org-level-4 ((t (:inherit org-level-1))))
   `(org-level-5 ((t (:inherit org-level-1))))
   `(org-level-6 ((t (:inherit org-level-1))))
   `(org-level-7 ((t (:inherit org-level-1))))
   `(org-level-8 ((t (:inherit org-level-1))))
   ;; Magit
   `(magit-section-heading ((t (:inherit org-level-1))))
   `(magit-diff-file-heading ((t (:inherit default))))
   `(magit-diff-removed-highlight ((t (:foreground ,dark :background ,pumpkin))))
   `(diff-refine-removed ((t (:foreground ,silver :background ,pumpkin-dark))))
   `(magit-diff-context-highlight ((t (:inherit org-block))))
   `(magit-diff-file-heading-highlight ((t (:inherit hl-line-face))))
   `(magit-diff-hunk-heading-highlight ((t (:inherit magit-diff-file-heading-highlight))))
   `(magit-branch-remote ((t (:inherit font-lock-constant-face))))
   ;; EWW
   `(eww-valid-certificate ((t (:inherit default))))
   `(eww-form-text ((t (:inherit default :box (:line-width 1)))))
   `(eww-form-file ((t (:inherit eww-form-text))))
   `(eww-form-select ((t (:inherit eww-form-text))))
   `(eww-form-submit ((t (:inherit eww-form-text))))
   `(eww-form-checkbox ((t (:inherit eww-form-text))))
   `(eww-form-textarea ((t (:inherit eww-form-text))))
   ;; LaTeX
   `(font-latex-sectioning-0-face ((t (:inherit org-level-1))))
   `(font-latex-sectioning-1-face ((t (:inherit org-level-2))))
   `(font-latex-sectioning-2-face ((t (:inherit org-level-3))))
   `(font-latex-sectioning-3-face ((t (:inherit org-level-4))))
   `(font-latex-sectioning-4-face ((t (:inherit org-level-5))))
   `(font-latex-sectioning-5-face ((t (:inherit org-level-6))))
   `(font-latex-italic-face ((t (:inherit italic))))
   `(font-latex-bold-face ((t (:weight bold))))
   `(font-latex-warning-face ((t (:inherit warning))))
   ;; Hydra
   `(hydra-face-red ((t (:foreground ,pumpkin))))
   `(hydra-face-blue ((t (:foreground ,french))))
   `(hydra-face-pink ((t (:foreground ,amethyst))))
   `(shadow ((t (:foreground ,silver))))
   `(link ((t (:underline t))))
   `(link-visited ((t (:underline t))))
   `(cursor ((t (:foreground ,midnight :background ,midnight))))
   `(escape-glyph ((t (:foreground ,sun-flower :bold t))))
   `(header-line ((t (:inherit mode-line))))
   `(success ((t (:foreground ,nephritis :weight bold))))
   `(warning ((t (:foreground ,pumpkin :weight bold))))
;;;;; compilation
   `(compilation-column-face ((t (:foreground ,orange))))
   `(compilation-enter-directory-face ((t (:foreground ,cadet))))
   `(compilation-error-face ((t (:foreground ,pumpkin :weight bold :underline t))))
   `(compilation-face ((t (:foreground ,midnight))))
   `(compilation-info-face ((t (:foreground ,french))))
   `(compilation-info ((t (:foreground ,nephritis :underline t))))
   `(compilation-leave-directory-face ((t (:foreground ,wisteria))))
   `(compilation-line-face ((t (:foreground ,sun-flower))))
   `(compilation-line-number ((t (:foreground ,sun-flower))))
   `(compilation-message-face ((t (:foreground ,midnight))))
   `(compilation-warning-face ((t (:foreground ,pumpkin :weight bold :underline t))))
   `(compilation-mode-line-exit ((t (:foreground ,cadet :weight bold))))
   `(compilation-mode-line-fail ((t (:foreground ,pumpkin :weight bold))))
   `(compilation-mode-line-run ((t (:foreground ,orange :weight bold))))
;;;;; grep
   `(grep-context-face ((t (:foreground ,midnight))))
   `(grep-error-face ((t (:foreground ,pumpkin :weight bold :underline t))))
   `(grep-hit-face ((t (:foreground ,cadet :weight bold))))
   `(grep-match-face ((t (:foreground ,sun-flower :weight bold))))
   `(match ((t (:background ,cadet :foreground ,midnight))))
;;;;; isearch
   `(isearch ((t (:foreground ,silver :weight bold :background ,pumpkin))))
   `(isearch-fail ((t (:foreground ,sun-flower :weight bold :background ,pumpkin))))
   `(lazy-highlight ((t (:foreground ,midnight :weight bold :background ,sun-flower))))

   `(menu ((t (:foreground ,midnight :background ,silver))))
   `(minibuffer-prompt ((t (:foreground ,silver))))
   `(region ((,class (:background ,sun-flower :foreground ,midnight))
	     (t :inverse-video t)))
   `(secondary-selection ((t (:background ,cadet))))
   `(trailing-whitespace ((t (:background ,pumpkin))))
   `(vertical-border ((t (:foreground ,silver))))
;;;;; font lock
   `(font-lock-builtin-face ((t (:foreground ,cadet))))
   `(font-lock-comment-face ((t (:foreground ,concrete :slant italic))))
   `(sh-heredoc ((t (:inherit font-lock-comment-face))))
   `(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face))))
   `(font-lock-constant-face ((t (:foreground ,silver))))
   `(font-lock-doc-face ((t (:foreground ,concrete))))
   `(font-lock-function-name-face ((t (:foreground ,sun-flower :weight normal))))
   `(font-lock-keyword-face ((t (:foreground ,belize-hole))))
   `(font-lock-negation-char-face ((t (:foreground ,french :weight normal))))
   `(font-lock-preprocessor-face ((t (:foreground ,pumpkin :weight normal))))
   `(font-lock-regexp-grouping-construct ((t (:foreground ,orange :weight normal))))
   `(font-lock-regexp-grouping-backslash ((t (:foreground ,amethyst :weight normal))))
   `(font-lock-string-face ((t (:foreground ,belize-hole))))
   `(font-lock-type-face ((t (:foreground ,sun-flower))))
   `(font-lock-variable-name-face ((t (:foreground ,sun-flower))))
   `(font-lock-warning-face ((t (:foreground ,pumpkin :weight normal))))

   `(c-annotation-face ((t (:inherit font-lock-constant-face))))
;;;;; ledger
   `(ledger-font-directive-face ((t (:foreground ,nephritis))))
   `(ledger-font-periodic-xact-face ((t (:inherit ledger-font-directive-face))))
   `(ledger-occur-xact-face ((t (:background ,silver))))
;;;; Third-party
;;;;; ace-jump
   `(ace-jump-face-background
     ((t (:foreground ,concrete :background ,silver :inverse-video nil))))
   `(ace-jump-face-foreground
     ((t (:foreground ,pumpkin :background ,silver :inverse-video nil))))
;;;;; anzu
   `(anzu-mode-line ((t (:foreground ,cadet :weight bold))))
;;;;; auto-complete
   `(ac-candidate-face ((t (:background ,concrete :foreground ,midnight))))
   `(ac-selection-face ((t (:background ,concrete :foreground ,midnight))))
   `(popup-tip-face ((t (:inherit org-verbatim))))
   `(popup-scroll-bar-foreground-face ((t (:background ,concrete))))
   `(popup-scroll-bar-background-face ((t (:background ,silver))))
   `(popup-isearch-match ((t (:background ,silver :foreground ,midnight))))
;;;;; clojure-test-mode
   `(clojure-test-failure-face ((t (:foreground ,orange :weight bold :underline t))))
   `(clojure-test-error-face ((t (:foreground ,pumpkin :weight bold :underline t))))
   `(clojure-test-success-face ((t (:foreground ,emerald :weight bold :underline t))))
;;;;; diff
   `(diff-added ((,class (:foreground ,nephritis :background ,silver))
		 (t (:foreground ,nephritis :background ,silver))))
   `(diff-changed ((t (:foreground ,orange))))
   `(diff-context ((t (:foreground ,concrete))))
   `(diff-removed ((,class (:foreground ,pumpkin :background ,silver))
		   (t (:foreground ,pumpkin :background ,silver))))
   `(diff-refine-added ((t :inherit diff-added :background ,emerald :weight bold)))
   `(diff-refine-change ((t :inherit diff-changed :weight bold)))
   `(diff-header ((,class (:foreground ,midnight :weight bold))
		  (t (:foreground ,midnight :weight bold))))
   `(diff-file-header
     ((,class (:foreground ,midnight :weight bold))
      (t (:foreground ,midnight :weight bold))))
   `(diff-hunk-header
     ((,class (:foreground ,wisteria :weight bold))
      (t (:foreground ,wisteria :weight bold))))

;;;;; diff-hl
   `(diff-hl-insert ((t (:foreground ,cadet :background ,nephritis))))
   `(diff-hl-delete ((t (:foreground ,pumpkin :background ,pumpkin))))
   `(diff-hl-change ((t (:foreground ,pumpkin :background ,orange))))

;;;;; dired/dired+/dired-subtree
   `(diredp-display-msg ((t (:foreground ,belize-hole))))
   `(diredp-compressed-file-suffix ((t (:foreground ,amethyst))))
   `(diredp-date-time ((t (:foreground ,orange))))
   `(diredp-deletion ((t (:foreground ,pumpkin))))
   `(diredp-deletion-file-name ((t (:foreground ,pumpkin))))
   `(diredp-dir-heading ((t (:foreground ,french :background ,silver :weight bold))))
   `(diredp-dir-priv ((t (:foreground ,french))))
   `(diredp-exec-priv ((t (:foreground ,orange))))
   `(diredp-executable-tag ((t (:foreground ,pumpkin))))
   `(diredp-file-name ((t (:foreground ,midnight))))
   `(diredp-file-suffix ((t (:foreground ,wisteria))))
   `(diredp-flag-mark ((t (:foreground ,sun-flower))))
   `(diredp-flag-mark-line ((t (:foreground ,orange))))
   `(diredp-ignored-file-name ((t (:foreground ,concrete))))
   `(diredp-link-priv ((t (:foreground ,amethyst))))
   `(diredp-mode-line-flagged ((t (:foreground ,sun-flower))))
   `(diredp-mode-line-marked ((t (:foreground ,orange))))
   `(diredp-no-priv ((t (:foreground ,midnight))))
   `(diredp-number ((t (:foreground ,belize-hole))))
   `(diredp-other-priv ((t (:foreground ,midnight))))
   `(diredp-rare-priv ((t (:foreground ,pumpkin))))
   `(diredp-read-priv ((t (:foreground ,cadet))))
   `(diredp-symlink ((t (:foreground ,silver :background ,amethyst))))
   `(diredp-write-priv ((t (:foreground ,pumpkin))))
   `(dired-subtree-depth-1-face ((t (:background ,darker))))
   `(dired-subtree-depth-2-face ((t (:background ,darker))))
   `(dired-subtree-depth-3-face ((t (:background ,darker))))
;;;;; ediff
   `(ediff-current-diff-A ((t (:foreground ,midnight :background ,pumpkin))))
   `(ediff-current-diff-Ancestor ((t (:foreground ,midnight :background ,pumpkin))))
   `(ediff-current-diff-B ((t (:foreground ,midnight :background ,emerald))))
   `(ediff-current-diff-C ((t (:foreground ,midnight :background ,french))))
   `(ediff-even-diff-A ((t (:background ,silver))))
   `(ediff-even-diff-Ancestor ((t (:background ,silver))))
   `(ediff-even-diff-B ((t (:background ,silver))))
   `(ediff-even-diff-C ((t (:background ,silver))))
   `(ediff-fine-diff-A ((t (:foreground ,midnight :background ,pumpkin :weight bold))))
   `(ediff-fine-diff-Ancestor ((t (:foreground ,midnight :background ,pumpkin weight bold))))
   `(ediff-fine-diff-B ((t (:foreground ,midnight :background ,emerald :weight bold))))
   `(ediff-fine-diff-C ((t (:foreground ,midnight :background ,french :weight bold ))))
   `(ediff-odd-diff-A ((t (:background ,silver))))
   `(ediff-odd-diff-Ancestor ((t (:background ,silver))))
   `(ediff-odd-diff-B ((t (:background ,silver))))
   `(ediff-odd-diff-C ((t (:background ,silver))))
;;;;; eshell
   `(eshell-prompt ((t (:foreground ,pumpkin :weight bold))))
   `(eshell-ls-archive ((t (:foreground ,amethyst :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,french :weight bold))))
   `(eshell-ls-executable ((t (:foreground ,pumpkin))))
   `(eshell-ls-unreadable ((t (:foreground ,concrete))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,sun-flower :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,silver :background ,amethyst))))
;;;;; evil
   `(evil-search-highlight-persist-highlight-face ((t (:inherit lazy-highlight))))
;;;;; flx
   `(flx-highlight-face ((t (:foreground ,orange :weight bold))))
;;;;; flycheck
   `(flycheck-error
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,pumpkin) :inherit unspecified))
      (t (:foreground ,pumpkin :weight bold :underline t))))
   `(flycheck-warning
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,orange) :inherit unspecified))
      (t (:foreground ,sun-flower :weight bold :underline t))))
   `(flycheck-info
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria) :inherit unspecified))
      (t (:foreground ,amethyst :weight bold :underline t))))
   `(flycheck-fringe-error ((t (:foreground ,pumpkin :weight bold))))
   `(flycheck-fringe-warning ((t (:foreground ,orange :weight bold))))
   `(flycheck-fringe-info ((t (:foreground ,wisteria :weight bold))))
;;;;; flymake
   `(flymake-errline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,pumpkin)
		   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,pumpkin :weight bold :underline t))))
   `(flymake-warnline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,orange)
		   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,orange :weight bold :underline t))))
   `(flymake-infoline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria)
		   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,wisteria :weight bold :underline t))))
;;;;; flyspell
   `(flyspell-duplicate
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,orange) :inherit unspecified))
      (t (:foreground ,orange :weight bold :underline t))))
   `(flyspell-incorrect
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,pumpkin) :inherit unspecified))
      (t (:foreground ,pumpkin :weight bold :underline t))))
;;;;; git-gutter
   `(git-gutter:added ((t (:foreground ,emerald :weight bold))))
   `(git-gutter:deleted ((t (:foreground ,pumpkin :weight bold))))
   `(git-gutter:modified ((t (:foreground ,orange :weight bold))))
   `(git-gutter:unchanged ((t (:foreground ,midnight :weight bold))))
;;;;; git-gutter-fr
   `(git-gutter-fr:added ((t (:foreground ,emerald  :weight bold))))
   `(git-gutter-fr:deleted ((t (:foreground ,pumpkin :weight bold))))
   `(git-gutter-fr:modified ((t (:foreground ,orange :weight bold))))
;;;;; guide-key
   `(guide-key/highlight-command-face ((t (:foreground ,french))))
   `(guide-key/key-face ((t (:foreground ,orange))))
   `(guide-key/prefix-command-face ((t (:foreground ,wisteria))))
;;;;; highlight-symbol
   `(highlight-symbol-face ((t (:background "gray88" :underline t))))
;;;;; hl-line-mode
   `(hl-line-face ((,class (:foreground ,silver :background ,darker)) (t :weight bold)))
   `(hl-line ((,class (:foreground ,silver :background ,darker)) (t :weight bold)))
;;;;; hl-sexp
   `(hl-sexp-face ((,class (:background ,silver)) (t :weight bold)))
;;;;; ido-mode
   `(ido-first-match ((t (:foreground ,pumpkin :weight bold))))
   `(ido-only-match ((t (:foreground ,orange :weight bold))))
   `(ido-subdir ((t (:foreground ,orange))))
   `(ido-indicator ((t (:foreground ,sun-flower :background ,pumpkin))))
;;;;; indent-guide
   `(indent-guide-face ((t (:foreground ,concrete))))
;;;;; js2-mode
   `(js2-warning ((t (:underline ,pumpkin))))
   `(js2-error ((t (:foreground ,pumpkin :weight bold))))
   `(js2-jsdoc-tag ((t (:foreground ,amethyst))))
   `(js2-jsdoc-type ((t (:foreground ,wisteria))))
   `(js2-jsdoc-value ((t (:foreground ,french))))
   `(js2-function-param ((t (:foreground, midnight))))
   `(js2-external-variable ((t (:foreground ,pumpkin))))
;;;;; linum-mode
   `(linum ((t (:foreground ,midnight :background ,silver))))
;;;;; magit
   `(magit-header ((t (:foreground ,midnight :background nil :weight bold))))
   `(magit-section-title ((t (:foreground ,midnight :background nil :weight bold))))
   `(magit-branch ((t (:foreground ,midnight :background ,cadet
				   :weight bold
				   :box (:line-width 1 :color ,cadet)))))
   `(magit-item-highlight ((t (:background ,silver))))
   `(magit-log-author ((t (:foreground ,belize-hole))))
   `(magit-log-sha1 ((t (:foreground ,orange :weight bold))))
   `(magit-tag ((t (:foreground ,wisteria :weight bold))))
   `(magit-log-head-label-head ((t (:foreground ,midnight :background ,cadet
						:weight bold
						:box (:line-width 1 :color ,cadet)))))
   `(magit-log-head-label-local ((t (:foreground ,midnight :background ,cadet
						 :weight bold
						 :box (:line-width 1 :color ,cadet)))))
   `(magit-log-head-label-default ((t (:foreground ,midnight :background ,cadet
						   :weight bold
						   :box (:line-width 1 :color ,cadet)))))
   `(magit-log-head-label-remote ((t (:foreground ,midnight :background ,sun-flower
						  :weight bold
						  :box (:line-width 1 :color ,orange)))))
   `(magit-log-head-label-tags ((t (:foreground ,wisteria :weight bold))))
;;;;; org-mode
   `(org-agenda-date-today ((t (:foreground ,silver :slant italic :weight bold))) t)
   `(org-agenda-structure ((t (:inherit font-lock-comment-face))))
   `(org-archived ((t (:foreground ,midnight :weight bold))))
   `(org-checkbox ((t (:foreground ,silver))))
   `(org-date ((t (:foreground ,belize-hole :underline t))))
   `(org-deadline-announce ((t (:foreground ,pumpkin))))
   `(org-done ((t (:bold t :weight bold :foreground ,emerald))))
   `(org-formula ((t (:foreground ,sun-flower))))
   `(org-headline-done ((t (:foreground ,emerald))))
   `(org-hide ((t (:foreground ,dark))))
   `(org-link ((t (:inherit link :foreground ,amethyst))))
   `(org-scheduled ((t (:foreground ,nephritis))))
   `(org-special-keyword ((t (:inherit font-lock-comment-face))))
   `(org-table ((t (:foreground ,silver))))
   `(org-tag ((t (:bold t :weight bold))))
   `(org-todo ((t (:bold t :foreground ,pumpkin :weight bold))))
   `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))
   `(org-warning ((t (:bold t :foreground ,pumpkin :weight bold :underline nil))))
   `(org-footnote ((t (:foreground ,amethyst :weight bold))))
;;;;; outline
   `(outline-1 ((t (:foreground ,cadet))))
   `(outline-2 ((t (:foreground ,belize-hole))))
   `(outline-3 ((t (:foreground ,wisteria))))
   `(outline-4 ((t (:foreground ,orange))))
   `(outline-5 ((t (:foreground ,pumpkin))))
   `(outline-6 ((t (:foreground ,pumpkin))))
;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,midnight))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,cadet))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,emerald))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,french))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,amethyst))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,sun-flower))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,orange))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,pumpkin))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,cadet))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,nephritis))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,belize-hole))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,wisteria))))
;;;;; structured-haskell-mode
   `(shm-current-face ((t (:background ,silver))))
   `(shm-quarantine-face ((t (:inherit font-lock-error))))
;;;;; show-paren
   `(show-paren-mismatch ((t (:foreground ,sun-flower :background ,pumpkin :weight bold))))
   `(show-paren-match ((t (:foreground ,silver :background ,amethyst :weight bold))))
;;;;; mode-line
   `(mode-line ((,class (:foreground ,silver :background ,dark :font "Noto Sans" :height 92))))
   `(doom-modeline-bar ((t (:foreground nil :background nil))))
   `(doom-modeline-panel ((t (:foreground nil :background nil))))
   `(doom-modeline-bar-inactive ((t (:foreground nil :background nil))))
   `(mode-line-inactive ((t (:inherit mode-line :foreground ,concrete))))
   `(mode-line-buffer-id ((t (:foreground ,silver))))
   `(header-line ((,class (:inherit mode-line))))
   `(persp-selected-face ((t (:foreground ,midnight :weight light :height 92))))
;;;;; SLIME
   `(slime-repl-output-face ((t (:foreground ,midnight))))
   `(slime-repl-inputed-output-face ((t (:foreground ,midnight))))
   `(slime-error-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,pumpkin)))
      (t
       (:underline ,pumpkin))))
   `(slime-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,orange)))
      (t
       (:underline ,orange))))
   `(slime-style-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,sun-flower)))
      (t
       (:underline ,sun-flower))))
   `(slime-note-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,wisteria)))
      (t
       (:underline ,wisteria))))
   `(slime-highlight-face ((t (:inherit cadet))))
;;;;; term
   `(term-color-black ((t (:foreground ,midnight :background ,midnight))))
   `(term-color-red ((t (:foreground ,pumpkin :background ,pumpkin))))
   `(term-color-green ((t (:foreground ,nephritis :background ,nephritis))))
   `(term-color-yellow ((t (:foreground ,orange :background ,orange))))
   `(term-color-blue ((t (:foreground ,belize-hole :background ,belize-hole))))
   `(term-color-magenta ((t (:foreground ,wisteria :background ,wisteria))))
   `(term-color-cyan ((t (:foreground ,cadet :background ,cadet))))
   `(term-color-white ((t (:foreground ,silver :background ,silver))))
   '(term-default-fg-color ((t (:inherit term-color-white))))
   '(term-default-bg-color ((t (:inherit term-color-black))))
;;;;; web-mode
   `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
   `(web-mode-css-at-rule-face ((t (:foreground ,pumpkin ))))
   `(web-mode-css-prop-face ((t (:foreground ,pumpkin))))
   `(web-mode-css-pseudo-class-face ((t (:foreground ,orange :weight bold))))
   `(web-mode-css-rule-face ((t (:foreground ,belize-hole))))
   `(web-mode-doctype-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-folded-face ((t (:underline t))))
   `(web-mode-function-name-face ((t (:foreground ,midnight :weight bold))))
   `(web-mode-html-attr-name-face ((t (:foreground ,wisteria))))
   `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-html-tag-face ((t (:foreground ,cadet :weight bold))))
   `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
   `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
   `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
   `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
   `(web-mode-server-background-face ((t (:background ,silver))))
   `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
   `(web-mode-server-string-face ((t (:foreground ,pumpkin))))
   `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
   `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
   `(web-mode-whitespaces-face ((t (:background ,pumpkin))))
   `(web-mode-block-face ((t (:background "gray88"))))
   `(web-mode-current-element-highlight-face ((t (:inverse-video t))))
;;;;; whitespace-mode
   `(whitespace-space ((t (:background ,silver :foreground ,sun-flower))))
   `(whitespace-hspace ((t (:background ,silver :foreground ,sun-flower))))
   `(whitespace-tab ((t (:background ,orange))))
   `(whitespace-newline ((t (:foreground ,sun-flower))))
   `(whitespace-trailing ((t (:background ,pumpkin))))
   `(whitespace-line ((t (:background nil :foreground ,pumpkin))))
   `(whitespace-space-before-tab ((t (:background ,silver :foreground ,pumpkin))))
   `(whitespace-indentation ((t (:background ,silver :foreground ,sun-flower))))
   `(whitespace-empty ((t (:background ,orange))))
   `(whitespace-space-after-tab ((t (:background ,silver :foreground ,pumpkin))))
;;;;; which-func-mode
   `(which-func ((t (:foreground ,wisteria :background ,silver))))
;;;;; yascroll
   `(yascroll:thumb-text-area ((t (:background ,silver))))
   `(yascroll:thumb-fringe ((t (:background ,silver :foreground ,sun-flower))))
   ))

;;; Theme Variables
(flatui-dark/with-color-variables
 (custom-theme-set-variables
  'flat-dark
;;;;; ansi-color
  `(ansi-color-names-vector [,silver ,pumpkin ,emerald ,sun-flower ,french ,amethyst ,cadet ,midnight])
;;;;; fill-column-indicator
  `(fci-rule-color ,sun-flower)
;;;;; vc-annotate
  `(vc-annotate-color-map
    '(( 30. . ,pumpkin)
      ( 60. . ,pumpkin)
      ( 90. . ,orange)
      (120. . ,pumpkin)
      (150. . ,sun-flower)
      (180. . ,orange)
      (210. . ,emerald)
      (240. . ,nephritis)
      (270. . ,cadet)
      (300. . ,cadet)
      (330. . ,french)
      (360. . ,belize-hole)))
  `(vc-annotate-very-old-color ,belize-hole)
  `(vc-annotate-background ,silver)
  ))
;;; Footer
;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
		  (file-name-as-directory
		   (file-name-directory load-file-name))))
(provide-theme 'flat-dark)
;;; flat-dark-theme.el ends here
