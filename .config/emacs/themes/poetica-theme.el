;;; poetica-theme.el --- A light theme for the ancient writer -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: 2022-03-25
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(deftheme poetica "A dark theme for the ancient writer.")

(let ((bg      "#E7E0DA") ;; Main background color
      (fg      "#322F37") ;; Main foreground color
      ;;Highlight
      (hg      "#835549") ;; Main highlight color
      (bg-hg   "#CDC6C1") ;; Main background highlight color
      ;;Darker
      (bg-alt  "#DAD3CD") ;; Color for darker background
      (fg-alt  "#7C6F64") ;; Color for darker text
      ;;Colorful
      (orange  "#F89169")
      (green   "#689D6A")
      (blue    "#63A6A5")
      (verde   "#B9BAA2"))

(custom-theme-set-faces 'poetica

;; Text
`(default ((t (:foreground ,fg :background ,bg))))
`(fixed-pitch ((t (:inherit default))))
`(variable-pitch ((t (:foreground ,fg :background ,bg))))
`(bold ((t (:weight bold))))
`(italic ((t (:slant italic))))
`(bold-italic ((t (:inherit (bold italic)))))

;; Highlighting
`(region ((t (:foreground ,bg :background ,fg))))
`(cursor ((t (:foreground ,bg :background ,fg))))
`(match ((t (:foreground ,bg :background ,fg))))
`(show-paren-match ((t (:foreground ,hg :background ,fg))))
`(link ((t (:inherit font-lock-constant-face :underline t))))
`(org-link ((t (:inherit link))))
`(link-visited ((t (:inherit link :underline nil))))

;; Darker tones
`(solaire-default-face ((t (:background ,bg-alt))))
`(shadow ((t (:background ,bg-alt))))
`(hl-line ((t (:background ,bg-hg))))

;; syntax
;;; comments/docs
`(font-lock-doc-face                  ((t (:foreground ,fg-alt :italic t))))
`(font-lock-comment-delimiter-face    ((t (:inherit font-lock-doc-face))))
`(font-lock-comment-face              ((t (:inherit font-lock-doc-face))))
`(font-lock-builtin-face              ((t (:inherit font-lock-doc-face :foreground ,hg))))
;;; strings
`(font-lock-string-face               ((t (:foreground ,hg))))
;;; functions
`(font-lock-keyword-face              ((t (:foreground ,hg))))
`(font-lock-function-name-face        ((t (:inherit font-lock-keyword-face))))
;;; variables
`(font-lock-constant-face             ((t (:inherit font-lock-builtin-face))))
`(font-lock-variable-name-face        ((t (:inherit font-lock-constant-face))))
`(font-lock-type-face                 ((t (:inherit font-lock-constant-face))))
;;; others
`(font-lock-negation-char-face        ((t (:foreground ,hg))))
`(font-lock-preprocessor-face         ((t (:foreground ,hg))))
`(font-lock-preprocessor-char-face    ((t (:foreground ,hg))))
`(font-lock-regexp-grouping-backslash ((t (:foreground ,hg))))
`(font-lock-regexp-grouping-construct ((t (:foreground ,hg))))

;; Org-mode
;;; Headings
`(org-level-1 ((t (:height 1.0 :weight bold))))
`(org-level-1 ((t (:height 1.0 :weight bold))))
`(org-level-2 ((t (:height 1.0 :weight bold))))
`(org-level-3 ((t (:height 1.0 :weight bold))))
`(org-level-4 ((t (:height 1.0 :weight bold))))
`(org-level-5 ((t (:height 1.0 :weight bold))))
`(org-level-6 ((t (:height 1.0 :weight bold))))
`(org-level-7 ((t (:height 1.0 :weight bold))))
`(org-level-8 ((t (:height 1.0 :weight bold))))
`(outline-1 ((t (:inherit org-level-1))))
`(outline-2 ((t (:inherit org-level-1))))
`(outline-3 ((t (:inherit org-level-1))))
`(outline-4 ((t (:inherit org-level-1))))
`(outline-5 ((t (:inherit org-level-1))))
`(outline-6 ((t (:inherit org-level-1))))

;;; Properties
`(org-document-title ((t (:foreground ,fg))))
`(org-document-info ((t (:inherit org-document-title))))
`(org-document-info-keyword ((t (:inherit org-document-title))))
`(org-property-value ((t (:inherit org-document-title))))
`(org-special-keyword ((t (:inherit org-document-title))))
`(org-meta-line ((t (:inherit org-document-title))))
`(org-drawer ((t (:inherit org-document-title))))

`(org-hide ((t (:foreground ,bg))))
`(org-table ((t (:inherit fixed-pitch))))
`(org-code ((t (:inherit fixed-pitch))))
`(org-date ((t (:inherit org-link))))
`(org-verbatim ((t (:inherit fixed-pitch :foreground ,fg :background ,bg-hg))))
`(org-headline-done ((t (:foreground ,fg-alt :strike-through t))))

;;; Blocks
`(org-block             ((t (:background ,bg-alt :extend t))))
`(org-block-background  ((t (:inherit org-block))))
`(org-block-begin-line  ((t (:inherit org-block))))
`(org-block-end-line    ((t (:inherit org-block))))

;;; Org-ref
`(org-ref-cite-face      ((t (:inherit font-lock-constant-face))))
`(org-ref-cite-&-face    ((t (:inherit org-ref-cite-face))))
`(org-ref-acronym-face   ((t (:inherit org-ref-cite-face))))
`(org-ref-glossary-face  ((t (:inherit org-ref-cite-face))))
`(org-ref-label-face     ((t (:inherit org-ref-cite-face))))
`(org-ref-ref-face       ((t (:inherit org-ref-cite-face))))
`(font-lock-warning-face ((t (:inherit org-ref-cite-face))))

;; Minibuffer
`(minibuffer-prompt ((t (:inherit default))))

;; Completions
`(vertico-current ((t (:background ,bg-hg :distant-foreground nil :extend t))))
`(highlight ((t (:inherit vertico-current))))
`(completions-annotations ((t (:slant italic))))
;; orderless-match-face-0

`(corfu-current ((t (:background ,bg-hg))))
`(corfu-default ((t (:background ,bg-alt))))

;; Window
`(fringe ((t (:background nil))))
`(window-divider ((t (:foreground ,bg-hg))))
`(window-divider-first-pixel ((t (:inherit window-divider))))
`(window-divider-last-pixel ((t (:inherit window-divider))))

;; Diff
`(diff-added ((t (:foreground ,fg :background ,verde))))
`(diff-removed ((t (:foreground ,fg :background ,hg))))
`(diff-refine-removed ((t (:foreground ,fg :background ,fg))))
`(diff-changed ((t (:foreground ,fg))))
`(diff-context ((t (:foreground ,fg-alt))))

;; Magit
`(magit-diff-context ((t (:inherit diff-context))))
`(magit-diff-context-highlight ((t (:inherit diff-context))))
`(magit-diff-removed-highlight ((t (:inherit diff-removed))))
`(magit-diff-added-highlight ((t (:inherit diff-added))))
`(magit-diff-removed ((t (:inherit diff-removed))))
`(magit-diff-added ((t (:inherit diff-added))))
;;; Branch
`(magit-branch-remote ((t (:inherit font-lock-constant-face))))
`(magit-branch-local ((t (:inherit font-lock-constant-face))))
`(magit-section-highlight ((t (:inherit font-lock-constant-face))))
;;; Headings
`(magit-diff-file-heading ((t (:inherit default))))
`(magit-diff-file-heading-highlight ((t (:inherit magit-section-highlight))))
`(magit-diff-hunk-heading ((t (:background ,bg-hg))))
`(magit-diff-hunk-heading-highlight ((t (:inherit magit-diff-hunk-heading-highlight))))
`(magit-section-heading ((t (:inherit org-level-1))))
;;; Log
`(magit-log-date ((t (:inherit elfeed-search-date-face))))
`(magit-log-author ((t (:foreground ,fg-alt))))
`(magit-hash ((t (:inherit magit-log-author))))

;; Elfeed
`(elfeed-search-feed-face ((t (:foreground ,fg))))
`(elfeed-search-unread-title-face ((t (:inherit elfeed-search-feed-face :background nil))))
`(elfeed-search-tag-face ((t (:inherit font-lock-variable-name-face))))
`(elfeed-search-date-face ((t (:inherit font-lock-comment-face))))
`(elfeed-search-title-face ((t (:inherit font-lock-comment-face :slant normal))))
`(message-header-name ((t (:inherit org-document-title))))
`(message-header-subject ((t (:inherit org-document-title))))
`(message-header-to ((t (:inherit org-document-title))))
`(message-header-other ((t (:inherit org-document-info))))

;; Mode/header-line
`(mode-line ((t (:inherit variable-pitch :background ,bg-alt :height 99))))
`(mode-line-inactive ((t (:inherit mode-line))))
`(mode-line-buffer-id ((t (:inherit mode-line))))
`(header-line ((t (:inherit mode-line))))

`(hydra-face-red ((t (:foreground ,orange))))
`(hydra-face-blue ((t (:foreground ,blue))))

`(neo-dir-link-face ((t (:inherit font-lock-keyword-face))))
`(neo-file-link-face ((t (:inherit default))))
`(neo-root-dir-face ((t (:foreground ,bg-alt :height 0.1))))
`(neo-expand-btn-face ((t (:foreground ,bg-alt :height 0.1))))

`(centaur-tabs-active-bar-face ((t (:background ,hg))))
`(centaur-tabs-selected ((t (:background ,bg :foreground ,fg))))
`(centaur-tabs-selected-modified ((t (:background ,bg :foreground ,hg))))
`(centaur-tabs-unselected ((t (:background ,bg-alt :foreground ,fg-alt))))
`(centaur-tabs-unselected-modified ((t (:background ,bg-alt :foreground ,fg-alt))))
`(tab-line ((t (:background ,bg-alt :foreground ,fg-alt))))

`(term-color-red ((t (:foreground ,orange))))
`(term-color-green ((t (:foreground ,green))))
`(term-color-blue ((t (:foreground ,blue))))



))

(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory
                (file-name-directory load-file-name))))

(provide-theme 'poetica)
;;; poetica-theme.el ends here
